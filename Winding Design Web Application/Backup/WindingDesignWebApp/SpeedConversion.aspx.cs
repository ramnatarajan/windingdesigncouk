﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    public partial class SpeedConversion : PageBase
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateFields();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.SpeedConversion != null)
            {
                txtSCODOutputPower.Text = CurrentToolInstance.SpeedConversion.OutputPowerOriginal.HasValue ? CurrentToolInstance.SpeedConversion.OutputPowerOriginal.Value.ToString() : string.Empty;
                cboSCODSlotsList.SelectedValue = CurrentToolInstance.SpeedConversion.NoOfSlotsOriginal.HasValue ? CurrentToolInstance.SpeedConversion.NoOfSlotsOriginal.Value.ToString() : string.Empty;
                cboSCPolesList.SelectedValue = CurrentToolInstance.SpeedConversion.NoOfPolesOriginal.HasValue ? CurrentToolInstance.SpeedConversion.NoOfPolesOriginal.Value.ToString() : string.Empty;
                txtSCODCoreLength.Text = CurrentToolInstance.SpeedConversion.CoreLengthOriginal.HasValue ? CurrentToolInstance.SpeedConversion.CoreLengthOriginal.Value.ToString() : string.Empty;
                txtSCODCoreDiameter.Text = CurrentToolInstance.SpeedConversion.CoreDiameterOriginal.HasValue ? CurrentToolInstance.SpeedConversion.CoreDiameterOriginal.Value.ToString() : string.Empty;
                txtSCODNoOfCurrentPaths.Text = CurrentToolInstance.SpeedConversion.NoOfCurrentPathsOriginal.HasValue ? CurrentToolInstance.SpeedConversion.NoOfCurrentPathsOriginal.Value.ToString() : string.Empty;
                txtSCODTurnsSlots.Text = CurrentToolInstance.SpeedConversion.TurnsSlotOriginal.HasValue ? CurrentToolInstance.SpeedConversion.TurnsSlotOriginal.Value.ToString() : string.Empty;
                txtSCODOldSpan.Text = CurrentToolInstance.SpeedConversion.SpanOriginal.HasValue ? CurrentToolInstance.SpeedConversion.SpanOriginal.Value.ToString() : string.Empty;
                txtSCODWireDiameter.Text = CurrentToolInstance.SpeedConversion.WireDiameterOriginal.HasValue ? CurrentToolInstance.SpeedConversion.WireDiameterOriginal.Value.ToString() : string.Empty;
                txtSCODWireArea.Text = CurrentToolInstance.SpeedConversion.WireAreaOriginal.HasValue ? CurrentToolInstance.SpeedConversion.WireAreaOriginal.Value.ToString() : string.Empty;
                txtSCODMaxCoreRating.Text = CurrentToolInstance.SpeedConversion.MaxCoreRatingOriginal.HasValue ? CurrentToolInstance.SpeedConversion.MaxCoreRatingOriginal.Value.ToString() : string.Empty;
                txtSCODTurnsSlotFullSpan.Text = CurrentToolInstance.SpeedConversion.TurnsSlotFullSpanOriginal.HasValue ? CurrentToolInstance.SpeedConversion.TurnsSlotFullSpanOriginal.Value.ToString() : string.Empty;

                txtSCNDOutputPower.Text = CurrentToolInstance.SpeedConversion.OutputPowerNew.HasValue ? CurrentToolInstance.SpeedConversion.OutputPowerNew.Value.ToString() : string.Empty;
                cboSCNDNoOfPoles.SelectedValue = CurrentToolInstance.SpeedConversion.NoOfPolesNew.HasValue ? CurrentToolInstance.SpeedConversion.NoOfPolesNew.Value.ToString() : string.Empty;
                txtSCNDMaxCoreRating.Text = CurrentToolInstance.SpeedConversion.MaxCoreRatingNew.HasValue ? CurrentToolInstance.SpeedConversion.MaxCoreRatingNew.Value.ToString() : string.Empty;
                txtSCNDNoOfCurrentPaths.Text = CurrentToolInstance.SpeedConversion.NoOfCurrentPathsNew.HasValue ? CurrentToolInstance.SpeedConversion.NoOfCurrentPathsNew.Value.ToString() : string.Empty;
                txtSCNDTurnsSlots.Text = CurrentToolInstance.SpeedConversion.TurnsSlotNew.HasValue ? CurrentToolInstance.SpeedConversion.TurnsSlotNew.Value.ToString() : string.Empty;
                txtSCNDArea.Text = CurrentToolInstance.SpeedConversion.AreaNew.HasValue ? CurrentToolInstance.SpeedConversion.AreaNew.Value.ToString() : string.Empty;
                txtSCNDWireDiameter.Text = CurrentToolInstance.SpeedConversion.WireDiameterNew.HasValue ? CurrentToolInstance.SpeedConversion.WireDiameterNew.Value.ToString() : string.Empty;
                txtSCNDFullSpan.Text = CurrentToolInstance.SpeedConversion.FullSpanNew.HasValue ? CurrentToolInstance.SpeedConversion.FullSpanNew.Value.ToString() : string.Empty;

            }

            lblResultmessage.Visible = CurrentToolInstance != null && CurrentToolInstance.SpeedConversion != null && CurrentToolInstance.SpeedConversion.ResultMessages != null && CurrentToolInstance.SpeedConversion.ResultMessages.Count > 0 ? true : false;
            grdSpeedConvResults.DataSource = CurrentToolInstance != null && CurrentToolInstance.SpeedConversion != null && CurrentToolInstance.SpeedConversion.ResultMessages != null && CurrentToolInstance.SpeedConversion.ResultMessages.Count > 0 ? CurrentToolInstance.SpeedConversion.ResultMessages : null;
            grdSpeedConvResults.DataBind();

            if (!Page.IsPostBack)
            {
                AttachAlphaRestrictionToTextBoxes(Page.Controls);
            }

            SetMaxLengthAndToolTipToTextBoxes(Page.Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        private void CaptureFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.SpeedConversion != null)
            {
                CurrentToolInstance.SpeedConversion.JobIdentificationNumber = txtJobIdentificationNumber.Text.Trim();

                CurrentToolInstance.SpeedConversion.OutputPowerOriginal = GetDouble(txtSCODOutputPower.Text.Trim());
                CurrentToolInstance.SpeedConversion.NoOfSlotsOriginal = GetDouble(cboSCODSlotsList.SelectedValue.Trim());
                CurrentToolInstance.SpeedConversion.NoOfPolesOriginal = GetDouble(cboSCPolesList.SelectedValue.Trim());
                CurrentToolInstance.SpeedConversion.CoreLengthOriginal = GetDouble(txtSCODCoreLength.Text.Trim());
                CurrentToolInstance.SpeedConversion.CoreDiameterOriginal = GetDouble(txtSCODCoreDiameter.Text.Trim());
                CurrentToolInstance.SpeedConversion.NoOfCurrentPathsOriginal = GetDouble(txtSCODNoOfCurrentPaths.Text.Trim());
                CurrentToolInstance.SpeedConversion.TurnsSlotOriginal = GetDouble(txtSCODTurnsSlots.Text.Trim());
                CurrentToolInstance.SpeedConversion.SpanOriginal = GetDouble(txtSCODOldSpan.Text.Trim());
                CurrentToolInstance.SpeedConversion.WireDiameterOriginal = GetDouble(txtSCODWireDiameter.Text.Trim());
                CurrentToolInstance.SpeedConversion.OutputPowerNew = GetDouble(txtSCNDOutputPower.Text.Trim());
                CurrentToolInstance.SpeedConversion.NoOfPolesNew = GetDouble(cboSCNDNoOfPoles.SelectedValue.Trim());
                CurrentToolInstance.SpeedConversion.MaxCoreRatingNew = GetDouble(txtSCNDMaxCoreRating.Text.Trim());
                CurrentToolInstance.SpeedConversion.NoOfCurrentPathsNew = GetDouble(txtSCNDNoOfCurrentPaths.Text.Trim());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ClearFields(ControlCollection page)
        {
            foreach (Control c in page)
            {
                if (c.ID != null && c.GetType() == typeof(TextBox))
                {
                    ((TextBox)c).Text = string.Empty;
                }

                if (c.HasControls())
                {
                    ClearFields(c.Controls);
                }
            }

            grdSpeedConvResults.DataSource = null;
            grdSpeedConvResults.DataBind();
            lblResultmessage.Visible = false;
            CurrentToolInstance.PowerConversion = new BusinessLayer.PowerConversionInstance();
            cboSCPolesList.SelectedValue = string.Empty;
            cboSCODSlotsList.SelectedValue = string.Empty;
            cboSCNDNoOfPoles.SelectedValue = string.Empty;
        }

        #region Page events

        /// <summary>
        /// Handles the Click event of the btnSCWireCalc control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void btnSCWireCalc_Click(object sender, EventArgs e)
        {
            Response.Redirect("WireCalculator.aspx");
        }

        /// <summary>
        /// Handles the Click event of the btnSCSpan control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void btnSCSpan_Click(object sender, EventArgs e)
        {
            //TODO: Create a new Span Page
            Calculate();
        }

        /// <summary>
        /// Handles the Click event of the btnSCCalcAll control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void btnSCCalcAll_Click(object sender, EventArgs e)
        {
            Calculate();
        }

        /// <summary>
        /// Handles the Click event of the btnPCClearAll control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void btnPCClearAll_Click(object sender, EventArgs e)
        {
            ClearFields(Page.Controls);
        }

        /// <summary>
        /// Handles the Click event of the btnPrint control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Calculate();
            Session["SCInstance"] = CurrentToolInstance.SpeedConversion;
            ResponseHelper.Redirect("PrintCalcJob.aspx?CalcType=" + EnumHelper.GetStringValue(CalculationToolType.SpeedConversion), "_blank", "menubar=0,width=800,height=600");
        }

        /// <summary>
        /// 
        /// </summary>
        private void Calculate()
        {
            CaptureFields();

            CurrentToolInstance.SpeedConversion = CurrentToolInstance.SpeedConversion.Calculate(CurrentToolInstance.SpeedConversion);

            PopulateFields();
        }

        #endregion
    }
}