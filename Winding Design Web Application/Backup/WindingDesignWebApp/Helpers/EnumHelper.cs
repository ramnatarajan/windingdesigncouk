﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using WindingDesign.BusinessLayer;

namespace WindingDesign.WebApp
{
    public static class EnumHelper
    {
        /// <summary>
        /// Will get the string value for a given enums value, this will
        /// only work if you assign the StringValue attribute to
        /// the items in your enum.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetStringValue(this Enum value)
        {
            try
            {
                Type type = value.GetType();
                FieldInfo fieldInfo = type.GetField(value.ToString());
                StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                return attribs.Length > 0 ? attribs[0].StringValue : null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnumEntryText"></param>
        /// <returns></returns>
        public static WindingDesign.WebApp.PageBase.CalculationToolType GetToolTypeEnumValue(string EnumEntryText)
        {
            if (!string.IsNullOrWhiteSpace(EnumEntryText))
            {
                switch (EnumEntryText)
                {
                    case "WireCalculator":
                        return WindingDesign.WebApp.PageBase.CalculationToolType.WireCalculator;
                    case "VoltageConversion":
                        return WindingDesign.WebApp.PageBase.CalculationToolType.VoltageConversion;
                    case "PowerConversion":
                        return WindingDesign.WebApp.PageBase.CalculationToolType.PowerConversion;
                    case "CoilConversion":
                        return WindingDesign.WebApp.PageBase.CalculationToolType.CoilConversion;
                    case "SpeedConversion":
                        return WindingDesign.WebApp.PageBase.CalculationToolType.SpeedConversion;
                }
            }

            return PageBase.CalculationToolType.WireCalculator;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnumEntryText"></param>
        /// <returns></returns>
        public static BusinessLayer.VoltageConversionInstance.ConversionType GetVCEnumValue(string EnumEntryText)
        {
            switch (EnumEntryText)
            {
                case "Star to Star":
                    return BusinessLayer.VoltageConversionInstance.ConversionType.StarToStar;
                case "Star to Delta":
                    return BusinessLayer.VoltageConversionInstance.ConversionType.StarToDelta;
                case "Delta to Star":
                    return BusinessLayer.VoltageConversionInstance.ConversionType.DeltaToStar;
                case "Delta to Delta":
                    return BusinessLayer.VoltageConversionInstance.ConversionType.DeltaToDelta;
            }

            return BusinessLayer.VoltageConversionInstance.ConversionType.StarToStar;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnumEntryText"></param>
        /// <returns></returns>
        public static BusinessLayer.CoilConversionInstance.WindingType GetCCEnumValue(string EnumEntryText)
        {
            switch (EnumEntryText)
            {
                case "Concentric":
                    return BusinessLayer.CoilConversionInstance.WindingType.Concentric;
                case "Round":
                    return BusinessLayer.CoilConversionInstance.WindingType.Round;
            }

            return BusinessLayer.CoilConversionInstance.WindingType.Concentric;
        }
    }
}