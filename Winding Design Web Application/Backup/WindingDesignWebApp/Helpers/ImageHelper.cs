﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace WindingDesign.WebApp
{
    public class ImageHelper : Form
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mimeType"></param>
        /// <returns></returns>
        private ImageCodecInfo getEncoderInfo(string mimeType)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }

        /// <summary>
        /// Method that resizes the image and returns.
        /// </summary>
        /// <param name="img"></param>
        /// <param name="Height"></param>
        /// <param name="Width"></param>
        /// <returns></returns>
        public Image ImageResize(Image img, int Height, int Width)
        {
            Bitmap bmp = new Bitmap(Width, Height);
            bmp.SetResolution(100, 100);
            Graphics g = Graphics.FromImage((Image)bmp);
            g.CompositingQuality = CompositingQuality.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.DrawImage(img, 0, 0, Width, Height);
            g.Dispose();
            return bmp;
        }

        /// <summary>
        /// Method that creates the needy design image and its thumbnail
        /// </summary>
        /// <param name="ImageFile"></param>
        /// <param name="DesignID"></param>
        public void CreateWireDesignImages(HttpPostedFile ImageFile, int DesignID)
        {
            if (ImageFile != null)
            {
                string SourceImageServerPath = HttpContext.Current.Server.MapPath("~/Images/WireDesigns/Originals/" + DesignID.ToString() + ".JPG");
                ImageFile.SaveAs(SourceImageServerPath);
                System.Drawing.Imaging.Encoder jpegEnc = new System.Drawing.Imaging.Encoder(new Guid());
                EncoderParameter qualityParam = new EncoderParameter(jpegEnc, 100);
                EncoderParameters encoderParams = new EncoderParameters(1);
                encoderParams.Param[0] = qualityParam;
                ImageCodecInfo jpegCodec = getEncoderInfo("image/jpeg");

                Image SourceImageFile = System.Drawing.Image.FromFile(SourceImageServerPath);
                Image MainImage = ImageResize(SourceImageFile, 600, 840);
                Image ThumbnailImage = ImageResize(SourceImageFile, 100, 100);
                Image PrintImage = ImageResize(SourceImageFile, 1000, 1400);

                if (MainImage != null)
                { MainImage.Save(HttpContext.Current.Server.MapPath("~/Images/WireDesigns/" + DesignID.ToString() + ".JPG"), jpegCodec, encoderParams); }

                if (ThumbnailImage != null)
                { ThumbnailImage.Save(HttpContext.Current.Server.MapPath("~/Images/WireDesigns/Thumbnails/" + DesignID.ToString() + ".JPG"), jpegCodec, encoderParams); }

                if (PrintImage != null)
                { PrintImage.Save(HttpContext.Current.Server.MapPath("~/Images/WireDesigns/Prints/" + DesignID.ToString() + ".JPG"), jpegCodec, encoderParams); }
                
                SourceImageFile.Dispose();
                MainImage.Dispose();
                ThumbnailImage.Dispose();
                PrintImage.Dispose();

                SourceImageFile = null;
                MainImage = null;
                ThumbnailImage = null;
                PrintImage = null;

                if (File.Exists(SourceImageServerPath))
                {
                    File.Delete(SourceImageServerPath);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DesignID"></param>
        public void DeleteWireDesignImages(int DesignID)
        {
            try
            {
                string SourceImageServerPath = HttpContext.Current.Server.MapPath("~/Images/WireDesigns/Originals/" + DesignID.ToString() + ".JPG");
                string ThumbnailImageServerPath = HttpContext.Current.Server.MapPath("~/Images/WireDesigns/Thumbnails/" + DesignID.ToString() + ".JPG");
                string MainImageServerPath = HttpContext.Current.Server.MapPath("~/Images/WireDesigns/" + DesignID.ToString() + ".JPG");
                string PrintImageServerPath = HttpContext.Current.Server.MapPath("~/Images/WireDesigns/Prints/" + DesignID.ToString() + ".JPG");

                if (File.Exists(SourceImageServerPath))
                {
                    File.Delete(SourceImageServerPath);
                }

                if (File.Exists(ThumbnailImageServerPath))
                {
                    File.Delete(ThumbnailImageServerPath);
                }

                if (File.Exists(MainImageServerPath))
                {
                    File.Delete(MainImageServerPath);
                }

                if (File.Exists(PrintImageServerPath))
                {
                    File.Delete(PrintImageServerPath);
                }
            }
            catch { }
        }
    }
}
