﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WindingDesign.BusinessLayer;

namespace WindingDesign.WebApp
{
    public class MasterPageBase : System.Web.UI.MasterPage
    {
        /// <summary>
        /// 
        /// </summary>
        private ClientDetail currentClient;

        /// <summary>
        /// 
        /// </summary>
        private WindingDesignEntities currentEntityContext;

        /// <summary>
        /// 
        /// </summary>
        public WindingDesignEntities CurrentEntityContext
        {
            get
            {
                if (currentEntityContext == null)
                {
                    currentEntityContext = WindingDesignObjectFactory.GetObjectContext<WindingDesignEntities>();
                }

                return currentEntityContext;
            }
        }

        /// <summary>
        /// Gets the allowed pages without login.
        /// </summary>
        /// <value>
        /// The allowed pages without login.
        /// </value>
        public List<string> AllowedPagesWithoutLogin
        {
            get
            {
                return new List<string>
                {
                    "login",
                    "register",
                    "trainingvideos",
                    "aboutus",
                    "aboutmybook"
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ClientDetail CurrentClient
        {
            get
            {
                int cClientID = Session["CurrentClientID"] != null && Session["CurrentClientID"].ToString() != string.Empty ? Convert.ToInt32(Session["CurrentClientID"].ToString()) : 0;
                currentClient = null;

                if (cClientID != 0)
                {
                    BusinessLayer.Client cClient = (from usrs in CurrentEntityContext.Clients where usrs.ClientID == cClientID select usrs).FirstOrDefault();

                    if (cClient != null)
                    {
                        currentClient = new ClientDetail
                        {
                            ClientID = cClient.ClientID,
                            Title = cClient.Title,
                            FirstName = cClient.FirstName,
                            LastName = cClient.LastName,
                            EmailAddress = cClient.EmailAddress,
                            CompanyName = cClient.CompanyName,
                            VATNumber = cClient.VATNumber,
                            AddressLine1 = cClient.AddressLine1,
                            AddressLine2 = cClient.AddressLine2,
                            County = cClient.County,
                            PostCode = cClient.PostCode,
                            Country = cClient.Country,
                            TelephoneNumber = cClient.TelephoneNumber,
                            MobileNumber = cClient.MobileNumber,
                            Salt = cClient.Salt,
                            EncryptedPassword = cClient.EncryptedPassword,
                            RegisteredDate = cClient.RegisteredDate,
                            LastModifiedDate = cClient.LastModifiedDate,
                            LicenceExpiryDate = cClient.LicenceExpiryDate,
                            IsLocked = cClient.IsLocked,
                            IsAdmin = cClient.IsAdmin
                        };
                    }
                }

                return currentClient;
            }

            set
            {
                currentClient = value;
            }
        }

        /// <summary>
        /// Determines whether [is page allowed without login] [the specified current URL].
        /// </summary>
        /// <param name="CurrentURL">The current URL.</param>
        /// <returns>
        ///   <c>true</c> if [is page allowed without login] [the specified current URL]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsPageAllowedWithoutLogin(string CurrentURL)
        {
            bool ReturnValue = false;

            foreach (string aEntry in AllowedPagesWithoutLogin)
            {
                if (CurrentURL.Contains(aEntry) && !ReturnValue)
                {
                    ReturnValue = true;
                    break;
                }
            }

            return ReturnValue;
        }

        /// <summary>
        /// Gets the welcome text.
        /// </summary>
        /// <returns></returns>
        public string GetWelcomeText()
        {
            string ReturnText = string.Empty;

            try
            {
                if (CurrentClient != null)
                {
                    ReturnText = "Welcome " + CurrentClient.FirstName + " - ";
                }
                else
                {
                    ReturnText = "Welcome Guest - ";
                }
            }
            catch { }

            return ReturnText;
        }

        /// <summary>
        /// Gets the session literal text.
        /// </summary>
        /// <returns></returns>
        public string GetSessionLiteralText()
        {
            string ReturnText = string.Empty;

            try
            {
                if (CurrentClient != null)
                {
                    ReturnText = "(<a href='/Register.aspx' runat='server' class='WelcomeTexts'>Edit your Profile</a>&nbsp;/&nbsp;<a href='/LogOut.aspx' runat='server' class='WelcomeTexts'>Log Out</a>)" + GetLicenseExpiryLiteralText();
                }
                else
                {
                    ReturnText = "(<a href='/Register.aspx' runat='server' class='WelcomeTexts'>Register</a>&nbsp;/&nbsp;<a href='/LogIn.aspx' runat='server' class='WelcomeTexts'>Log In</a>)";
                }
            }
            catch { }

            return ReturnText;
        }

        /// <summary>
        /// Gets the license expiry literal text.
        /// </summary>
        /// <returns></returns>
        public string GetLicenseExpiryLiteralText()
        {
            string ReturnText = string.Empty;

            try
            {
                if (CurrentClient != null && CurrentClient.LicenceExpiryDate.HasValue && CurrentClient.LicenceExpiryDate.Value > DateTime.Now)
                {
                    ReturnText = " - License Expiry in " + (CurrentClient.LicenceExpiryDate.Value - DateTime.Now).Days + " day(s) <a href='/BuyLicense.aspx' runat='server' class='WelcomeTexts'>Buy License Extension</a>";
                }
                else if (CurrentClient != null && CurrentClient.LicenceExpiryDate.HasValue && CurrentClient.LicenceExpiryDate.Value < DateTime.Now)
                {
                    ReturnText = " - License Expired <a href='/BuyLicense.aspx' runat='server' class='WelcomeTexts'>Buy License Extension</a>";
                }
            }
            catch { }

            return ReturnText;
        }
    }
}