﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TrainingVideos.aspx.cs" Inherits="WindingDesign.WebApp.TrainingVideos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $('#HomeTabs').tabs();

        $('#TrainingVideos tbody tr').each(function () {
            var nTds = $('td', this);
            var DialogID = $(nTds[5]).text();
            nTds[1].setAttribute('title', DialogID);
            nTds[2].setAttribute('title', DialogID);
            nTds[6].setAttribute('title', "OpenEditVideo");
            $('#' + DialogID).dialog({
                autoOpen: false,
                modal: true,
                buttons: {},
                width: 640,
                resizable: false
            });
        });

        $('#TrainingVideos tbody tr td').click(function (e) {
            if (this.title != "OpenEditVideo") {
                $('#' + this.title).dialog('open');
                return false;
            }
        });

        var oTable = $('#TrainingVideos').dataTable({
            "bJQueryUI": true,
            "aoColumns": [{ "bVisible": false }, null, { "sWidth": "7em", "bSortable": false }, { "bVisible": false }, { "bVisible": false }, { "bVisible": false }, { "sWidth": "2em"}],
            "aaSorting": [],
            "oLanguage": { "sSearch": "Search " },
            "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, 'All']],
            "iDisplayLength": 5
        });
    });
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upVoltConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="text-align: left; height: 28px; width: 100%;">
                                                &nbsp;&nbsp;Training Videos
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table id="TrainingVideos" class="display" style="vertical-align: text-top;text-align:left;" cellspacing="0"
                        cellpadding="0" border="0">
                        <thead>
                            <tr>
                                <th>
                                </th>
                                <th>
                                    Description
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <%if (IsAdmin)
                                  { %>
                                <th>
                                </th>
                                <%}
                                  else
                                  { %>
                                  <th style="display:none;"></th>
                                    <%} %>
                            </tr>
                        </thead>
                        <tbody>
                            <% if (TrainingVideoSource != null && TrainingVideoSource.Count > 0)
                               {
                                   foreach (WindingDesign.WebApp.TrainingVideo vidEntry in TrainingVideoSource)
                                   {
                            %>
                            <tr>
                                <td style="vertical-align: text-top; margin-left: 10px;">
                                    <% =vidEntry.ShortDescription%>
                                </td>
                                <td style="vertical-align: text-top; margin-left: 10px;">
                                    <p style="font-size:12px;text-align:left;font-weight:bold;"><% =vidEntry.ShortDescription%></p>
                                    <p style="font-size:12px;text-align:justify;"><% =vidEntry.LongDescription%></p>
                                </td>
                                <td style="text-align:center;">
                                    <a id='dialoglink<% =vidEntry.TrainingVideoID %>' href="#">
                                        <object width="250" height="141"><param name="movie" value="<% =vidEntry.VideoEmbedPath %>?version=3&amp;hl=en_US&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="<% =vidEntry.VideoEmbedPath %>?version=3&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" width="250" height="141" allowscriptaccess="always" allowfullscreen="true">
                                            </embed>
                                        </object>
                                        <span id="dialog<% =vidEntry.TrainingVideoID %>" title="<% =vidEntry.ShortDescription %>">
                                        <p style="font-size:12px;text-align:justify;">&nbsp;&nbsp;&nbsp;<% =vidEntry.LongDescription%></p>
                                        <object width="640" height="360"><param name="movie" value="<% =vidEntry.VideoEmbedPath %>?version=3&amp;hl=en_US&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="<% =vidEntry.VideoEmbedPath %>?version=3&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true">
                                            </embed>
                                        </object>
                                            </span></a>
                                </td>
                                <td>
                                    <% =vidEntry.SearchStringText%>
                                </td>
                                <td>dialoglink<% =vidEntry.TrainingVideoID%></td>
                                <td>dialog<% =vidEntry.TrainingVideoID%></td>
                                <%if (IsAdmin)
                                  { %>
                                <td>
                                    <div style="text-align:center;"><a id="EditVideo" class="ui-state-default ui-corner-all" href="TrainingVideoEdit.aspx?TrainingVideoID=<% =vidEntry.TrainingVideoID %>">&nbsp;Edit&nbsp;</a></div>
                                </td>
                                <%}
                                  else
                                  { %>
                                  <td></td>
                                  <%} %>
                            </tr>
                            <%
                                }
                               }
                            %>
                        </tbody>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%if (IsAdmin)
              { %>
            <table>
                  <tr>
                      <td>
                         <br /><a id="A1" class="ui-state-default ui-corner-all" href="TrainingVideoEdit.aspx">&nbsp;Add Video&nbsp;</a>
                      </td>
                  </tr>
            </table>
            <%} %>
        </div>
    </div>
</asp:Content>
