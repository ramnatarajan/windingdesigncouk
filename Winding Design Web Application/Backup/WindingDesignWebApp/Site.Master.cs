﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SiteMaster : MasterPageBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentClient == null && !IsPageAllowedWithoutLogin(Page.AppRelativeVirtualPath.ToLower()))
            {
                Response.Redirect("Login.aspx");
            }

            lblWelcomUser.Text = GetWelcomeText();
            ltrlUpdateProfile.Text = GetSessionLiteralText();

            if (CurrentClient != null && CurrentClient.LicenceExpiryDate.HasValue && CurrentClient.LicenceExpiryDate < DateTime.Now && !Page.AppRelativeVirtualPath.ToLower().Contains("buylicense"))
            {
                Response.Redirect("BuyLicense.aspx?LExp=1");
            }
        }
    }
}
