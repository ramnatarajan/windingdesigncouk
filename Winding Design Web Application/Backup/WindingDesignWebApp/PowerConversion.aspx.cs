﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    public partial class PowerConverter : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateFields();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.PowerConversion != null)
            {
                txtJobIdentificationNumber.Text = CurrentToolInstance.PowerConversion.JobIdentificationNumber;

                txtPCODOutputPower.Text = CurrentToolInstance.PowerConversion.OutputPowerOriginal.HasValue ? CurrentToolInstance.PowerConversion.OutputPowerOriginal.Value.ToString() : string.Empty;
                cboPCPolesList.SelectedValue = CurrentToolInstance.PowerConversion.NoOfPolesOriginal.HasValue ? CurrentToolInstance.PowerConversion.NoOfPolesOriginal.Value.ToString() : string.Empty;
                txtPCODCoreLength.Text = CurrentToolInstance.PowerConversion.CoreLengthOriginal.HasValue ? CurrentToolInstance.PowerConversion.CoreLengthOriginal.Value.ToString() : string.Empty;
                txtPCODCoreDiameter.Text = CurrentToolInstance.PowerConversion.CoreDiameterOriginal.HasValue ? CurrentToolInstance.PowerConversion.CoreDiameterOriginal.Value.ToString() : string.Empty;
                txtPCODNoOfCurrentPaths.Text = CurrentToolInstance.PowerConversion.NoOfCurrentPathsOriginal.HasValue ? CurrentToolInstance.PowerConversion.NoOfCurrentPathsOriginal.Value.ToString() : string.Empty;
                txtPCODTurnsSlots.Text = CurrentToolInstance.PowerConversion.TurnsOriginal.HasValue ? CurrentToolInstance.PowerConversion.TurnsOriginal.Value.ToString() : string.Empty;
                txtPCODWireDiameter.Text = CurrentToolInstance.PowerConversion.WireDiameterOriginal.HasValue ? CurrentToolInstance.PowerConversion.WireDiameterOriginal.Value.ToString() : string.Empty;
                txtPCODMaxCoreRating.Text = CurrentToolInstance.PowerConversion.MaxCoreRatingOriginal.HasValue ? CurrentToolInstance.PowerConversion.MaxCoreRatingOriginal.Value.ToString() : string.Empty;
                txtPCODWireArea.Text = CurrentToolInstance.PowerConversion.WireAreaOriginal.HasValue ? CurrentToolInstance.PowerConversion.WireAreaOriginal.Value.ToString() : string.Empty;

                txtPCNDoutputPower.Text = CurrentToolInstance.PowerConversion.OutputPowerNew.HasValue ? CurrentToolInstance.PowerConversion.OutputPowerNew.Value.ToString() : string.Empty;
                txtPCNDNoOfCurrentPaths.Text = CurrentToolInstance.PowerConversion.NoOfCurrentPathsNew.HasValue ? CurrentToolInstance.PowerConversion.NoOfCurrentPathsNew.Value.ToString() : string.Empty;
                txtPCNDTurns.Text = CurrentToolInstance.PowerConversion.TurnsNew.HasValue ? CurrentToolInstance.PowerConversion.TurnsNew.Value.ToString() : string.Empty;
                txtPCNDWireDiameter.Text = CurrentToolInstance.PowerConversion.WireDiameterNew.HasValue ? CurrentToolInstance.PowerConversion.WireDiameterNew.Value.ToString() : string.Empty;
                txtPCNDNewArea.Text = CurrentToolInstance.PowerConversion.WireAreaNew.HasValue ? CurrentToolInstance.PowerConversion.WireAreaNew.Value.ToString() : string.Empty;
            }

            lblResultmessage.Visible = CurrentToolInstance != null && CurrentToolInstance.PowerConversion != null && CurrentToolInstance.PowerConversion.ResultMessages != null && CurrentToolInstance.PowerConversion.ResultMessages.Count > 0 ? true : false;
            grdPowerConvResults.DataSource = CurrentToolInstance != null && CurrentToolInstance.PowerConversion != null && CurrentToolInstance.PowerConversion.ResultMessages != null && CurrentToolInstance.PowerConversion.ResultMessages.Count > 0 ? CurrentToolInstance.PowerConversion.ResultMessages : null;
            grdPowerConvResults.DataBind();

            if (!Page.IsPostBack)
            {
                AttachAlphaRestrictionToTextBoxes(Page.Controls);
            }

            SetMaxLengthAndToolTipToTextBoxes(Page.Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        private void CaptureFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.PowerConversion != null)
            {
                CurrentToolInstance.PowerConversion.JobIdentificationNumber = txtJobIdentificationNumber.Text.Trim();

                CurrentToolInstance.PowerConversion.OutputPowerOriginal = GetDouble(txtPCODOutputPower.Text.Trim());
                CurrentToolInstance.PowerConversion.NoOfPolesOriginal = GetDouble(cboPCPolesList.SelectedValue.Trim());
                CurrentToolInstance.PowerConversion.CoreLengthOriginal = GetDouble(txtPCODCoreLength.Text.Trim());
                CurrentToolInstance.PowerConversion.CoreDiameterOriginal = GetDouble(txtPCODCoreDiameter.Text.Trim());
                CurrentToolInstance.PowerConversion.NoOfCurrentPathsOriginal = GetDouble(txtPCODNoOfCurrentPaths.Text.Trim());
                CurrentToolInstance.PowerConversion.TurnsOriginal = GetDouble(txtPCODTurnsSlots.Text.Trim());
                CurrentToolInstance.PowerConversion.WireDiameterOriginal = GetDouble(txtPCODWireDiameter.Text.Trim());

                CurrentToolInstance.PowerConversion.OutputPowerNew = GetDouble(txtPCNDoutputPower.Text.Trim());
                CurrentToolInstance.PowerConversion.NoOfCurrentPathsNew = GetDouble(txtPCNDNoOfCurrentPaths.Text.Trim());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ClearFields(ControlCollection page)
        {
            foreach (Control c in page)
            {
                if (c.ID != null && c.GetType() == typeof(TextBox))
                {
                    ((TextBox)c).Text = string.Empty;
                }

                if (c.HasControls())
                {
                    ClearFields(c.Controls);
                }
            }

            grdPowerConvResults.DataSource = null;
            grdPowerConvResults.DataBind();
            lblResultmessage.Visible = false;
            CurrentToolInstance.PowerConversion = new BusinessLayer.PowerConversionInstance();
            cboPCPolesList.SelectedValue = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPCWireCalc_Click(object sender, EventArgs e)
        {
            Response.Redirect("WireCalculator.aspx");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPCCalcAll_Click(object sender, EventArgs e)
        {
            Calculate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPCClearAll_Click(object sender, EventArgs e)
        {
            ClearFields(Page.Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Calculate();
            Session["PCInstance"] = CurrentToolInstance.PowerConversion;
            ResponseHelper.Redirect("PrintCalcJob.aspx?CalcType=" + EnumHelper.GetStringValue(CalculationToolType.PowerConversion), "_blank", "menubar=0,width=800,height=600");
        }

        /// <summary>
        /// 
        /// </summary>
        private void Calculate()
        {
            CaptureFields();

            CurrentToolInstance.PowerConversion = CurrentToolInstance.PowerConversion.Calculate(CurrentToolInstance.PowerConversion);

            PopulateFields();
        }
    }
}