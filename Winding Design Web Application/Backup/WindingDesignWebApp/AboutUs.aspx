﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AboutUs.aspx.cs" Inherits="WindingDesign.WebApp.AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <table class="AboutUsLayout">
                <tr>
                    <td style="vertical-align: text-top; text-align: right;">
                        <div class="AboutUsLeft">
                            <img alt="Tony Ball" src="Images/TonyBallLogo.png" height="200px" width="150px" />
                            <br />
                            <p class="AboutAuthorNameStyle">
                                Tony Ball
                            </p>
                        </div>
                    </td>
                    <td class="AboutUsRight">
                        <div class="AboutUsRight">
                            <h2>
                                Author Notes</h2>
                            <p>
                                I have been in the electric motor rewind industry for 45 years. I completed an indentured
                                apprenticeship as an armature winder and a 5 year City & Guilds day release Craft
                                Maintenance course. This was followed by a 2 year ONC course in Electrical Power
                                and Electronics which also included a fairly high level of mathematics. The electrical
                                power element was all about motors and generators.</p>
                            <p>
                                I have worked for three major rewind companies and rewound motors from as small
                                as my thumb to armatures 7 tonnes in weight. I worked in a range of other roles
                                including shop floor instruction, Safety and Training Management, Personnel, Works
                                Management and Quality Management but for the last 24 years I have run my own motor
                                rewind business. I employ two other engineers.</p>
                            <p>
                                I am sure that most small rewind companies would agree that there is not a lot of
                                technical information available to them so this is why I have put this web site
                                together. I also do not want the knowledge and experience that I have gained over
                                the years, lost when I retire from the industry.</p>
                            <p>
                                During my career with one particular company, I took every opportunity to learn
                                about 3ph winding design and was fortunate to spend time with two semi-retired engineers.
                                One was 67 years old when he retired and the other 74 years old. They were really
                                experienced in winding conversion and design. One learned what he knew from a course
                                which was put on by a motor manufacturing company and the other from old books.
                                The formulae that I have used on this web site originated from that motor manufacturing
                                company. I have been using these formulae for over 30 years and they have never
                                let me down once.</p>
                            <p>
                                I intend to put over twenty different elements into this program of which only a
                                small percentage of these are finished and on the site but this will be added to
                                over the coming two years. Also, if you have something that you wish to be added,
                                even if it is just a connection diagram, once checked, we will add it to the site.</p>
                            <p>
                                <h3>
                                    PLEASE NOTE:</h3>
                                The calculators on this web site are for designing and/or converting the windings
                                of small and medium 3ph, metric frame, IE1 (Eff2) electric motors. In many cases,
                                efficiency and power factor may change and this has not been taken into account.</p>
                            <p>
                                I have every confidence that they will work equally well on larger motors and motors
                                of different efficiency ratings however, I have not put this to the test.</p>
                            <p style="font-style:italic;text-align:right;">
                                Web Design by <a href="http://www.RamNatarajan.co.uk" target="_blank">Ram Natarajan</a>
                            </p>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
