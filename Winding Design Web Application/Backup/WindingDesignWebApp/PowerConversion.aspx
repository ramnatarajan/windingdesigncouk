﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="PowerConversion.aspx.cs" Inherits="WindingDesign.WebApp.PowerConverter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upPowerConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="text-align: left; width: 70%;">
                                                &nbsp;&nbsp;Converting Output Power Speed and Current Paths
                                            </td>
                                            <td style="text-align: right; width: 30%;">
                                                Job Number&nbsp;
                                                <asp:TextBox ID="txtJobIdentificationNumber" Width="70px" CssClass="SiteTextBoxes"
                                                    runat="server" MaxLength="10" />
                                                &nbsp;
                                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="CalcButtons" OnClick="btnPrint_Click"
                                                    ToolTip="Please allow popups to use print functionality" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTable">
                        <tr>
                            <td>
                                <div class="PCConvCalcLeft">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Original Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power in kW's
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODOutputPower" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No of Poles
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:DropDownList ID="cboPCPolesList" runat="server" Width="84px">
                                                                <asp:ListItem Selected="True"></asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>6</asp:ListItem>
                                                                <asp:ListItem>8</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Core Length (mm)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODCoreLength" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Core Diameter (mm)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODCoreDiameter" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No.of Current Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODNoOfCurrentPaths" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Turns/Slot
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODTurnsSlots" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Wire Diameter
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <a href='javascript:void();' class='tooltip'>
                                                                <asp:TextBox ID="txtPCODWireDiameter" Width="80px" CssClass="SiteTextBoxes tooltip"
                                                                    runat="server" />
                                                                <span>
                                                                    <div class="ToolTipDiv">
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;You need to convert your wire sizes to one diameter to proceed.
                                                                        Click Wire Calc to use wire calculator & return when finished.</div>
                                                                </span></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Wire Area
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODWireArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Est. Core Rating (kW's)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODMaxCoreRating" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="vertical-align: text-top;">
                                <div class="PCConvCalcMiddle">
                                    <div class="PCConvCalcMiddleTop">
                                        <table class="ConvMiddleTable">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Use the wire calculator to convert your original wires to one wire diameter
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnPCWireCalc" CssClass="CalcButtons" runat="server" Text="Wire Calc"
                                                        OnClick="btnPCWireCalc_Click" />
                                                    &nbsp;
                                                    <asp:Button ID="btnPCCalcAll" CssClass="CalcButtons" runat="server" Text="Calc All"
                                                        OnClick="btnPCCalcAll_Click" />
                                                    &nbsp;<asp:Button ID="btnPCClearAll" CssClass="CalcButtons" runat="server" Text="Clear All"
                                                        OnClick="btnPCClearAll_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="text-align: center; width: 100%;">
                                            <tr>
                                                <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                    <div class="WireCalcOldDataTableLabel">
                                                        <asp:Label runat="server" ID="lblResultmessage" Text="Results" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="grdPowerConvResults" runat="server" AutoGenerateColumns="False"
                                            BorderColor="Transparent" AllowPaging="False" ShowHeader="False">
                                            <Columns>
                                                <asp:TemplateField ShowHeader="False" ControlStyle-BorderWidth="0px">
                                                    <ItemTemplate>
                                                        <div class="ResultValidationMessage">
                                                            <asp:Label ID="lblMessage" runat="server" Text='<%# Container.DataItem %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="PCConvCalcRight">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td style="height: 10px;">
                                                <div style="width: 100%; text-align: center; font-weight: bold; font-size: 12px; margin-top: 3px;">
                                                    New Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power in kW's
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDoutputPower" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No.of Current Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDNoOfCurrentPaths" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Turns
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDTurns" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            New Area
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDNewArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Wire Diameter
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDWireDiameter" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
