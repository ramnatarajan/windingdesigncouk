﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="CoilConversion.aspx.cs" Inherits="WindingDesign.WebApp.CoilConversion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upVoltConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="text-align: left; width: 70%;">
                                                &nbsp;&nbsp;Converting Coil Pitches
                                            </td>
                                            <td style="text-align: right; width: 30%;">
                                                Job Number&nbsp;
                                                <asp:TextBox ID="txtJobIdentificationNumber" Width="70px" CssClass="SiteTextBoxes"
                                                    runat="server" MaxLength="10" />
                                                &nbsp;
                                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="CalcButtons" OnClick="btnPrint_Click"
                                                    ToolTip="Please allow popups to use print functionality" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="CoilConversionTable">
                        <tr>
                            <td style="width: 33%;">
                                <div class="CoilConversionLeft">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CoilConversionLeftTableTd1">
                                                            Number of Poles
                                                        </td>
                                                        <td class="CoilConversionLeftTableTd3">
                                                            <div class="CoilConversionMarginWrapper">
                                                                <asp:DropDownList ID="cboCCPolesList" runat="server" Width="94px">
                                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                                    <asp:ListItem>2</asp:ListItem>
                                                                    <asp:ListItem>4</asp:ListItem>
                                                                    <asp:ListItem>6</asp:ListItem>
                                                                    <asp:ListItem>8</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CoilConversionLeftTableTd1">
                                                            Number of Slots
                                                        </td>
                                                        <td class="CoilConversionLeftTableTd3">
                                                            <div class="CoilConversionMarginWrapper">
                                                                <asp:DropDownList ID="cboCCNoOfSlots" runat="server" Width="94px">
                                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                                    <asp:ListItem>12</asp:ListItem>
                                                                    <asp:ListItem>18</asp:ListItem>
                                                                    <asp:ListItem>24</asp:ListItem>
                                                                    <asp:ListItem>30</asp:ListItem>
                                                                    <asp:ListItem>36</asp:ListItem>
                                                                    <asp:ListItem>42</asp:ListItem>
                                                                    <asp:ListItem>48</asp:ListItem>
                                                                    <asp:ListItem>54</asp:ListItem>
                                                                    <asp:ListItem>60</asp:ListItem>
                                                                    <asp:ListItem>72</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CoilConversionLeftTableTd1">
                                                            Original Coils/Group
                                                        </td>
                                                        <td class="CoilConversionLeftTableTd3">
                                                            <div class="CoilConversionMarginWrapper">
                                                                <asp:TextBox ID="txtCCCoilGroupOriginal" Width="90px" CssClass="SiteTextBoxes" runat="server" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CoilConversionLeftTableTd1">
                                                            Original Coil Sides/Slot
                                                        </td>
                                                        <td class="CoilConversionLeftTableTd3">
                                                            <div class="CoilConversionMarginWrapper">
                                                                <asp:TextBox ID="txtCCCoilSidesSlotOriginal" Width="90px" CssClass="SiteTextBoxes"
                                                                    runat="server" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CoilConversionLeftTableTd1">
                                                            Full Span
                                                        </td>
                                                        <td class="CoilConversionLeftTableTd3">
                                                            <div class="CoilConversionMarginWrapper">
                                                                <asp:TextBox ID="txtCCFullSpan" Width="90px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                    runat="server" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CoilConversionLeftTableTd1">
                                                            Original Coil Type
                                                        </td>
                                                        <td class="CoilConversionLeftTableTd3">
                                                            <div class="CoilConversionMarginWrapper">
                                                                <asp:DropDownList ID="lstWindingOriginal" runat="server" Width="94px">
                                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                                    <asp:ListItem>Concentric</asp:ListItem>
                                                                    <asp:ListItem>Round</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="CoilConversionLeftTableTd1">
                                                            New Coil Type
                                                        </td>
                                                        <td class="CoilConversionLeftTableTd3">
                                                            <div class="CoilConversionMarginWrapper">
                                                                <asp:DropDownList ID="lstWindingNew" runat="server" Width="94px">
                                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                                    <asp:ListItem>Concentric</asp:ListItem>
                                                                    <asp:ListItem>Round</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="margin-left:50px;" class="failureNotification RequiredField">
                                                    <h3>
                                                        &nbsp;This page is under construction</h3>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnGoBack" CssClass="CalcButtons" runat="server"
                                                                Text="Go Back" OnClick="btnGoBack_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnCalcAll" CssClass="CalcButtons" runat="server" Text="Calc All"
                                                                OnClick="btnCalcAll_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnClearAll" CssClass="CalcButtons" runat="server" Text="Clear All"
                                                                OnClick="btnClearAll_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="CoilConvLeftBottom">
                                                    <table style="text-align: center; width: 100%;">
                                                        <tr>
                                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                                <div class="WireCalcOldDataTableLabel">
                                                                    <asp:Label runat="server" ID="lblResultmessage" Text="Results" /></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:GridView ID="grdCoilConvResults" runat="server" AutoGenerateColumns="False"
                                                        BorderColor="Transparent" AllowPaging="False" ShowHeader="False">
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="False" ControlStyle-BorderWidth="0px">
                                                                <ItemTemplate>
                                                                    <div class="ResultValidationMessage">
                                                                        <asp:Label ID="lblMessage" runat="server" Text='<%# Container.DataItem %>' />
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td>
                                <div class="CoilConversionRight">
                                    <table class="CoilConversionRight">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Original Coil Span and Turns</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;Span
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanOriginal1" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanOriginal2" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanOriginal3" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanOriginal4" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanOriginal5" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanOriginal6" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;Turns
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsOriginal1" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsOriginal2" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsOriginal3" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsOriginal4" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsOriginal5" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsOriginal6" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;CF
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFOriginal1" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFOriginal2" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFOriginal3" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFOriginal4" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFOriginal5" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFOriginal6" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td style="text-align: right;" colspan="2">
                                                            Effective Turns/Coil&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCEffectiveTurnsCoilOriginal" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <div class="CoilConversionInnerTable">
                                                    <table class="CoilConversionInnerTable">
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnAutoFill" CssClass="CalcButtons" runat="server" Text="Auto Fill"
                                                                    OnClick="btnAutoFill_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnManualFill" CssClass="CalcButtons" runat="server" Text="Manual Fill"
                                                                    OnClick="btnManualFill_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnClearSpansAndTurns" CssClass="CalcButtons" runat="server" Text="Clear"
                                                                    OnClick="btnClearSpansAndTurns_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    New Coil Span and Turns</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;Span
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanNew1" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanNew2" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanNew3" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanNew4" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanNew5" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilSpanNew6" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;Turns
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsNew1" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsNew2" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsNew3" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsNew4" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsNew5" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilTurnsNew6" Width="50px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;CF
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFNew1" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFNew2" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFNew3" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFNew4" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFNew5" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCCoilCFNew6" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td style="text-align: right;" colspan="3">
                                                            New Average Turns/Coil&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCCEffectiveTurnsCoilNew" Width="50px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
