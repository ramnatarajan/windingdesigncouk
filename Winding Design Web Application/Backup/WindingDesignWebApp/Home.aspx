﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Home.aspx.cs" Inherits="WindingDesign.WebApp.Home" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <ul>
            <li><a href="#HTab-1">Wire Calculator</a></li>
            <li><a href="#HTab-2">Converting Voltage</a></li>
            <li><a href="#HTab-3">Converting Output Power</a></li>
            <li><a href="#HTab-4">Converting Coil Pitches</a></li>
            <li><a href="#HTab-5">About</a></li>
        </ul>
        <div id="HTab-1" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upWireCalc" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="text-align: left; width: 30%;">
                                                &nbsp;
                                                <asp:Button ID="btnWCPrintJob" CssClass="CalcButtons" runat="server" Width="50px"
                                                    Text="Print" />
                                            </td>
                                            <td style="text-align: right; width: 70%;">
                                                Details of the job goes in here with date&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTable">
                        <tr>
                            <td>
                                <div class="WireCalcLeft">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Old Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                &nbsp;&nbsp;Enter number of wires & diameters in mms
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;&nbsp;No.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            Dia.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            Area
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires1" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia1" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea1" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires2" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia2" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea2" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires3" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia3" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea3" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires4" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia4" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea4" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires5" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia5" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea5" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires6" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia6" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea6" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires7" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia7" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea7" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires8" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia8" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea8" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires9" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia9" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea9" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires10" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia10" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea10" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                        </td>
                                                        <td style="text-align: right;">
                                                            Old Area =&nbsp;
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCOldArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="94%">
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <asp:Button ID="btnWCCalcOD" CssClass="CalcButtons" runat="server" Width="50px" Text="Calc" />
                                                            <asp:Button ID="btnWCClearOD" CssClass="CalcButtons" runat="server" Width="50px"
                                                                Text="Clear" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="vertical-align: text-top;">
                                <div class="WireCalcMiddle">
                                    <table class="WireCalcMiddleTable">
                                        <tr>
                                            <td class="WireCalcMiddleTableTdLeft">
                                                <asp:RadioButton ID="rdoCalcTypeMetric" CssClass="WireCalcMiddleTableRadio" runat="server"
                                                    Text="Metric" Checked="true" GroupName="CalcType" />&nbsp;
                                            </td>
                                            <td class="WireCalcMiddleTableTdRight">
                                                &nbsp;<asp:RadioButton ID="rdoCalcTypeImperial" CssClass="WireCalcMiddleTableRadio"
                                                    runat="server" Text="Imperial" GroupName="CalcType" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcMiddleTableTdLeft">
                                                <asp:Button ID="btnWCCalcAll" CssClass="CalcButtons" runat="server" Text="Calc All" />
                                            </td>
                                            <td class="WireCalcMiddleTableTdRight">
                                                &nbsp;&nbsp;<asp:Button ID="btnWCClearAll" CssClass="CalcButtons" runat="server"
                                                    Text="Clear All" />&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="text-align: center; width: 100%;">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Results</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="grdCalcResults" runat="server" AutoGenerateColumns="False" BorderColor="Transparent"
                                        AllowPaging="False" ShowHeader="False">
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False" ControlStyle-BorderWidth="0px">
                                                <ItemTemplate>
                                                    <div class="ResultValidationMessage">
                                                        <asp:Label ID="lblMessage" runat="server" Text='<%# Container.DataItem %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                            <td>
                                <div class="WireCalcRight">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    New Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                &nbsp;&nbsp;Enter number of wires & diameters in mms
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;&nbsp;No.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            Dia.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            Area
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires1" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia1" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea1" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires2" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia2" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea2" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires3" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia3" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea3" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires4" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia4" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea4" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires5" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia5" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea5" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires6" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia6" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea6" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires7" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia7" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea7" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires8" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia8" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea8" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires9" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia9" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea9" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires10" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia10" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea10" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                        </td>
                                                        <td style="text-align: right;">
                                                            New Area =&nbsp;
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNewArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="94%">
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <asp:Button ID="btnWCCalcND" CssClass="CalcButtons" runat="server" Text="Calc" Width="50px" />
                                                            <asp:Button ID="btnWCClearND" CssClass="CalcButtons" runat="server" Text="Clear"
                                                                Width="50px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTableBottom">
                        <tr>
                            <td>
                                <div class="WireCalcBottomLeft">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Estimator</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 60%;">
                                                <br />
                                                &nbsp;&nbsp;&nbsp;How many wires you want to work with
                                            </td>
                                            <td style="width: 10%; text-align: left;">
                                                <br />
                                                <asp:TextBox ID="txtWCEstNoOfWires" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="8" />
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <br />
                                                <asp:Button ID="btnWCCalcEst" CssClass="CalcButtons" runat="server" Text="Calc" Width="50px" />
                                                <asp:Button ID="btnWCClearEst" CssClass="CalcButtons" runat="server" Text="Clear"
                                                    Width="50px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%; height: 25px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td>
                                <div class="WireCalcBottomRight">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Strip And Square Section</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 24%;">
                                                &nbsp;&nbsp;&nbsp;&nbsp;Width<br />
                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtWCStripWidth" Width="50px" CssClass="SiteTextBoxes"
                                                    runat="server" MaxLength="8" />
                                            </td>
                                            <td style="width: 24%; text-align: left">
                                                Depth<br />
                                                <asp:TextBox ID="txtWCStripDepth" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="8" />
                                            </td>
                                            <td style="width: 24%; text-align: left">
                                                Area<br />
                                                <asp:TextBox ID="txtWCStripArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                    runat="server" />
                                            </td>
                                            <td style="width: 40%; text-align: left">
                                                <br />
                                                <asp:Button ID="btnWCCalcStrip" CssClass="CalcButtons" runat="server" Text="Calc"
                                                    Width="50px" />
                                                <asp:Button ID="btnWCClearStrip" CssClass="CalcButtons" runat="server" Text="Clear"
                                                    Width="50px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 90%; text-align: left; height: 25px;">
                                                &nbsp;&nbsp;<i>The radius of the corners have not been taken into account</i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="HTab-2" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upVoltConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="text-align: left; width: 30%;">
                                                &nbsp;<asp:Button ID="btnVCPrintJob" CssClass="CalcButtons" runat="server" Width="50px"
                                                    Text="Print" />
                                            </td>
                                            <td style="text-align: right; width: 70%;">
                                                Details of the job goes in here with date&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTable">
                        <tr>
                            <td>
                                <div class="ConvCalcLeft">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Original Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODSupplyVoltage" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Full Load Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODFullLoadCurrent" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Phase Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODPhaseVoltage" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Phase Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODPhaseCurrent" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Connection
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODConnection" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Frequency
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODSupplyFrequency" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power (kW's)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOutputPower" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Original Turns
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalTurns" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Original Wire Dia.
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalWireDia" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Original No. of Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalNoOfPaths" Width="80px" CssClass="SiteTextBoxes"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Original F.L Speed
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalFLSpeed" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Original Cooling
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalCooling" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="vertical-align: text-top;">
                                <div class="WireCalcMiddle">
                                    <div class="ConvMiddleTop">
                                        <table class="ConvMiddleTable">
                                            <tr>
                                                <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                    <div class="WireCalcOldDataTableLabel">
                                                        Conversion Type</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="ConvMiddleradiolist">
                                                        <asp:RadioButtonList ID="rdoListConvType" CssClass="ConvMiddleradiolist" runat="server">
                                                            <asp:ListItem Selected="true">Start to Star</asp:ListItem>
                                                            <asp:ListItem>Star to Delta</asp:ListItem>
                                                            <asp:ListItem>Delta to Star</asp:ListItem>
                                                            <asp:ListItem>Delta to Delta</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnVCCalcAll" CssClass="CalcButtons" runat="server" Text="Calc All" />
                                                    &nbsp;<asp:Button ID="btnVCClearAll" CssClass="CalcButtons" runat="server" Text="Clear All" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="ConvMiddleBottom">
                                        <table style="text-align: center; width: 100%;">
                                            <tr>
                                                <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                    <div class="WireCalcOldDataTableLabel">
                                                        Results</div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="grdVoltageConvResults" runat="server" AutoGenerateColumns="False"
                                            BorderColor="Transparent" AllowPaging="False" ShowHeader="False">
                                            <Columns>
                                                <asp:TemplateField ShowHeader="False" ControlStyle-BorderWidth="0px">
                                                    <ItemTemplate>
                                                        <div class="ResultValidationMessage">
                                                            <asp:Label ID="lblMessage" runat="server" Text='<%# Container.DataItem %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="ConvCalcRight">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    New Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDSupplyVoltage" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            New Load Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewLoadCurrent" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Phase Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDPhaseVoltage" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Phase Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDPhaseCurrent" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Connection
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDConnection" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Frequency
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDSupplyFrequency" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power (kW's)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDOutputPower" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            New Turns
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewTurns" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            New Wire Dia.
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewWireDia" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            New No. of Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewNoOfPaths" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            New F.L Speed
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewFLSpeed" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Change in Cooling
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDChangeInCooling" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="HTab-3" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upPowerConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="text-align: left; width: 30%;">
                                                &nbsp;<asp:Button ID="btnPCPrintJob" CssClass="CalcButtons" runat="server" Width="50px"
                                                    Text="Print" />
                                            </td>
                                            <td style="text-align: right; width: 70%;">
                                                Details of the job goes in here with date&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTable">
                        <tr>
                            <td>
                                <div class="PCConvCalcLeft">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Original Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power in kW's
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODOutputPower" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No of Poles
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:DropDownList ID="cboPCPolesList" runat="server" Width="84px">
                                                                <asp:ListItem Selected="True"></asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>6</asp:ListItem>
                                                                <asp:ListItem>8</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No. of Slots
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:DropDownList ID="DropDownList1" runat="server" Width="84px">
                                                                <asp:ListItem Selected="True"></asp:ListItem>
                                                                <asp:ListItem>12</asp:ListItem>
                                                                <asp:ListItem>18</asp:ListItem>
                                                                <asp:ListItem>24</asp:ListItem>
                                                                <asp:ListItem>30</asp:ListItem>
                                                                <asp:ListItem>36</asp:ListItem>
                                                                <asp:ListItem>42</asp:ListItem>
                                                                <asp:ListItem>48</asp:ListItem>
                                                                <asp:ListItem>54</asp:ListItem>
                                                                <asp:ListItem>60</asp:ListItem>
                                                                <asp:ListItem>72</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Core Length (mm)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODCoreLength" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Core Diameter (mm)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODCoreDiameter" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No.of Current Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODNoOfCurrentPaths" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Turns/Slot
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODTurnsSlots" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Wire Diameter
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODWireDiameter" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Slots/Pole/Phase
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODSlotsPole" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Chord Factor
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODChordFactor" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Max. Core Rating (kW's)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCODMaxCoreRating" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="vertical-align: text-top;">
                                <div class="PCConvCalcMiddle">
                                    <div class="PCConvCalcMiddleTop">
                                        <table class="ConvMiddleTable">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Use the wire calculator to convert your original wires to one wire diameter
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnPCCalcAll" CssClass="CalcButtons" runat="server" Text="Calc All" />
                                                    &nbsp;<asp:Button ID="btnPCClearAll" CssClass="CalcButtons" runat="server" Text="Clear All" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="PCConvCalcRight">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    New Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power in kW's
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDoutputPower" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No of Poles
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:DropDownList ID="cboPCPolesRightList" runat="server" Width="84px">
                                                                <asp:ListItem Selected="True"></asp:ListItem>
                                                                <asp:ListItem>2</asp:ListItem>
                                                                <asp:ListItem>4</asp:ListItem>
                                                                <asp:ListItem>6</asp:ListItem>
                                                                <asp:ListItem>8</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No.of Current Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDNoOfCurrentPaths" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Full Span (1 to)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDFullSpan" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Turns
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDTurns" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Wire Diameter
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDWireDiameter" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Coil Grouping
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDCoilGrouping" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Slots/Pole/Phase
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtPCNDSlotsPole" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            <div class="PCConvRightRadiolist">
                                                                <asp:RadioButtonList ID="rdoPCNDCalcType" CssClass="PCConvRightRadiolist" TextAlign="Left"
                                                                    runat="server">
                                                                    <asp:ListItem Selected="true">Double Layer</asp:ListItem>
                                                                    <asp:ListItem>Chain Basket</asp:ListItem>
                                                                    <asp:ListItem>Chain Group</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTableBottom">
                        <tr>
                            <td>
                                <div class="WireCalcBottomLeft">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Original Coil Span</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 5%;">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 to
                                            </td>
                                            <td style="width: 95%; text-align: left;">
                                                <asp:TextBox ID="txtPCOriginalCoilSpan1" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan2" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan3" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan4" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan5" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan6" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan7" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan8" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan9" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan10" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan11" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCOriginalCoilSpan12" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td>
                                <div class="WireCalcBottomRight">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    New coil Span</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 5%;">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 to
                                            </td>
                                            <td style="width: 95%; text-align: left;">
                                                <asp:TextBox ID="txtPCNewcoilSpan1" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan2" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan3" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan4" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan5" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan6" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan7" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan8" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan9" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan10" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan11" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                                <asp:TextBox ID="txtPCNewcoilSpan12" Width="20px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="2" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="HTab-4" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upCoilConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="text-align: left; width: 30%;">
                                                &nbsp;<asp:Button ID="btnCCPrintJob" CssClass="CalcButtons" runat="server" Width="50px"
                                                    Text="Print" />
                                            </td>
                                            <td style="text-align: right; width: 70%;">
                                                Details of the job goes in here with date&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="HTab-5" class="TabMinHeightDefaults">
            <table class="AboutUsLayout">
                <tr>
                    <td>
                        <div class="AboutUsLeft">
                            <img alt="Tony Ball" src="Images/TonyBallLogo.png" height="200px" width="150px" />
                        </div>
                    </td>
                    <td class="AboutUsRight">
                        <div class="AboutUsRight">
                            <h2>
                                WireCalc</h2>
                            <h3>
                                Engineering Publications Ltd</h3>
                            <p>
                                For designing and converting the windings of Small and Medium 3ph, metric frame,
                                IE1 (Eff2) electric motors.</p>
                            <p>
                                This program is for evaluation only. We would be very grateful for your comments.
                                You will receive a complimentary copy on completion.</p>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
