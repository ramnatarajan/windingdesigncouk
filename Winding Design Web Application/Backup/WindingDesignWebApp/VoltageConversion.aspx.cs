﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    /// <summary>
    /// 
    /// </summary>
    public partial class VoltageConversion : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public BusinessLayer.VoltageConversionInstance.ConversionType CurrentConversionType
        {
            get
            {
                switch (rdoListConvType.SelectedValue)
                {
                    case "Star to Star":
                        return BusinessLayer.VoltageConversionInstance.ConversionType.StarToStar;
                    case "Star to Delta":
                        return BusinessLayer.VoltageConversionInstance.ConversionType.StarToDelta;
                    case "Delta to Star":
                        return BusinessLayer.VoltageConversionInstance.ConversionType.DeltaToStar;
                    case "Delta to Delta":
                        return BusinessLayer.VoltageConversionInstance.ConversionType.DeltaToDelta;
                }

                return BusinessLayer.VoltageConversionInstance.ConversionType.StarToStar;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateFields();
            }
        }

        #region Page Button Events

        /// <summary>
        /// 
        /// </summary>
        private void ClearFields(ControlCollection page)
        {
            foreach (Control c in page)
            {
                if (c.ID != null && c.GetType() == typeof(TextBox))
                {
                    ((TextBox)c).Text = string.Empty;
                }

                if (c.HasControls())
                {
                    ClearFields(c.Controls);
                }
            }

            grdVoltageConvResults.DataSource = null;
            grdVoltageConvResults.DataBind();
            rdoListConvType.SelectedValue = "Star to Star";
            lblResultmessage.Visible = false;
            CurrentToolInstance.VoltageConversion = new BusinessLayer.VoltageConversionInstance();
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.VoltageConversion != null)
            {
                txtJobIdentificationNumber.Text = CurrentToolInstance.VoltageConversion.JobIdentificationNumber;

                txtVCODSupplyVoltage.Text = CurrentToolInstance.VoltageConversion.SupplyVoltageOriginal.HasValue ? CurrentToolInstance.VoltageConversion.SupplyVoltageOriginal.Value.ToString() : string.Empty;
                txtVCODFullLoadCurrent.Text = CurrentToolInstance.VoltageConversion.FullLoadCurrentOriginal.HasValue ? CurrentToolInstance.VoltageConversion.FullLoadCurrentOriginal.Value.ToString() : string.Empty;
                txtVCODPhaseVoltage.Text = CurrentToolInstance.VoltageConversion.PhaseVoltageOriginal.HasValue ? CurrentToolInstance.VoltageConversion.PhaseVoltageOriginal.Value.ToString() : string.Empty;
                txtVCODPhaseCurrent.Text = CurrentToolInstance.VoltageConversion.PhaseCurrentOriginal.HasValue ? CurrentToolInstance.VoltageConversion.PhaseCurrentOriginal.Value.ToString() : string.Empty;
                txtVCODConnection.Text = CurrentToolInstance.VoltageConversion.ConnectionOriginal;
                txtVCODSupplyFrequency.Text = CurrentToolInstance.VoltageConversion.SupplyFrequencyOriginal.HasValue ? CurrentToolInstance.VoltageConversion.SupplyFrequencyOriginal.Value.ToString() : string.Empty;
                txtVCODOutputPower.Text = CurrentToolInstance.VoltageConversion.OutputPowerOriginal.HasValue ? CurrentToolInstance.VoltageConversion.OutputPowerOriginal.Value.ToString() : string.Empty;
                txtVCODOriginalTurns.Text = CurrentToolInstance.VoltageConversion.TurnsOriginal.HasValue ? CurrentToolInstance.VoltageConversion.TurnsOriginal.Value.ToString() : string.Empty;
                txtVCODOriginalWireDia.Text = CurrentToolInstance.VoltageConversion.WireDiameterOriginal.HasValue ? CurrentToolInstance.VoltageConversion.WireDiameterOriginal.Value.ToString() : string.Empty;
                txtVCODOriginalNoOfPaths.Text = CurrentToolInstance.VoltageConversion.NoOfPathsOriginal.HasValue ? CurrentToolInstance.VoltageConversion.NoOfPathsOriginal.Value.ToString() : string.Empty;
                txtVCODOriginalFLSpeed.Text = CurrentToolInstance.VoltageConversion.FLSpeedOriginal.HasValue ? CurrentToolInstance.VoltageConversion.FLSpeedOriginal.Value.ToString() : string.Empty;
                txtVCODOriginalCooling.Text = CurrentToolInstance.VoltageConversion.CoolingOriginal.HasValue ? CurrentToolInstance.VoltageConversion.CoolingOriginal.Value.ToString() : string.Empty;

                txtVCNDSupplyVoltage.Text = CurrentToolInstance.VoltageConversion.SupplyVoltageNew.HasValue ? CurrentToolInstance.VoltageConversion.SupplyVoltageNew.Value.ToString() : string.Empty;
                txtVCNDNewLoadCurrent.Text = CurrentToolInstance.VoltageConversion.FullLoadCurrentNew.HasValue ? CurrentToolInstance.VoltageConversion.FullLoadCurrentNew.Value.ToString() : string.Empty;
                txtVCNDPhaseVoltage.Text = CurrentToolInstance.VoltageConversion.PhaseVoltageNew.HasValue ? CurrentToolInstance.VoltageConversion.PhaseVoltageNew.Value.ToString() : string.Empty;
                txtVCNDPhaseCurrent.Text = CurrentToolInstance.VoltageConversion.PhaseCurrentNew.HasValue ? CurrentToolInstance.VoltageConversion.PhaseCurrentNew.Value.ToString() : string.Empty;
                txtVCNDConnection.Text = CurrentToolInstance.VoltageConversion.ConnectionNew;
                txtVCNDSupplyFrequency.Text = CurrentToolInstance.VoltageConversion.SupplyFrequencyNew.HasValue ? CurrentToolInstance.VoltageConversion.SupplyFrequencyNew.Value.ToString() : string.Empty;
                txtVCNDOutputPower.Text = CurrentToolInstance.VoltageConversion.OutputPowerNew.HasValue ? CurrentToolInstance.VoltageConversion.OutputPowerNew.Value.ToString() : string.Empty;
                txtVCNDNewTurns.Text = CurrentToolInstance.VoltageConversion.TurnsNew.HasValue ? CurrentToolInstance.VoltageConversion.TurnsNew.Value.ToString() : string.Empty;
                txtVCNDNewWireDia.Text = CurrentToolInstance.VoltageConversion.WireDiameterNew.HasValue ? CurrentToolInstance.VoltageConversion.WireDiameterNew.Value.ToString() : string.Empty;
                txtVCNDNewNoOfPaths.Text = CurrentToolInstance.VoltageConversion.NoOfPathsNew.HasValue ? CurrentToolInstance.VoltageConversion.NoOfPathsNew.Value.ToString() : string.Empty;
                txtVCNDNewFLSpeed.Text = CurrentToolInstance.VoltageConversion.FLSpeedNew.HasValue ? CurrentToolInstance.VoltageConversion.FLSpeedNew.Value.ToString() : string.Empty;
                txtVCNDChangeInCooling.Text = CurrentToolInstance.VoltageConversion.CoolingNew.HasValue ? CurrentToolInstance.VoltageConversion.CoolingNew.Value.ToString() : string.Empty;
                rdoListConvType.SelectedValue = CurrentToolInstance.VoltageConversion.CurrentConversionType != 0 ? EnumHelper.GetStringValue(CurrentToolInstance.VoltageConversion.CurrentConversionType) : "Star to Star";
            }

            lblResultmessage.Visible = CurrentToolInstance != null && CurrentToolInstance.VoltageConversion != null && CurrentToolInstance.VoltageConversion.ResultMessages != null && CurrentToolInstance.VoltageConversion.ResultMessages.Count > 0 ? true : false;
            grdVoltageConvResults.DataSource = CurrentToolInstance != null && CurrentToolInstance.VoltageConversion != null && CurrentToolInstance.VoltageConversion.ResultMessages != null && CurrentToolInstance.VoltageConversion.ResultMessages.Count > 0 ? CurrentToolInstance.VoltageConversion.ResultMessages : null;
            grdVoltageConvResults.DataBind();

            if (!Page.IsPostBack)
            {
                AttachAlphaRestrictionToTextBoxes(Page.Controls);
            }

            SetMaxLengthAndToolTipToTextBoxes(Page.Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        private void CaptureFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.VoltageConversion != null)
            {
                CurrentToolInstance.VoltageConversion.JobIdentificationNumber = txtJobIdentificationNumber.Text.Trim();

                CurrentToolInstance.VoltageConversion.SupplyVoltageOriginal = GetDouble(txtVCODSupplyVoltage.Text.Trim());
                CurrentToolInstance.VoltageConversion.FullLoadCurrentOriginal = GetDouble(txtVCODFullLoadCurrent.Text.Trim());
                CurrentToolInstance.VoltageConversion.SupplyFrequencyOriginal = GetDouble(txtVCODSupplyFrequency.Text.Trim());
                CurrentToolInstance.VoltageConversion.OutputPowerOriginal = GetDouble(txtVCODOutputPower.Text.Trim());
                CurrentToolInstance.VoltageConversion.TurnsOriginal = GetDouble(txtVCODOriginalTurns.Text.Trim());
                CurrentToolInstance.VoltageConversion.WireDiameterOriginal = GetDouble(txtVCODOriginalWireDia.Text.Trim());
                CurrentToolInstance.VoltageConversion.NoOfPathsOriginal = GetDouble(txtVCODOriginalNoOfPaths.Text.Trim());
                CurrentToolInstance.VoltageConversion.FLSpeedOriginal = GetDouble(txtVCODOriginalFLSpeed.Text.Trim());

                CurrentToolInstance.VoltageConversion.SupplyVoltageNew = GetDouble(txtVCNDSupplyVoltage.Text.Trim());
                CurrentToolInstance.VoltageConversion.SupplyFrequencyNew = GetDouble(txtVCNDSupplyFrequency.Text.Trim());
                CurrentToolInstance.VoltageConversion.NoOfPathsNew = GetDouble(txtVCNDNewNoOfPaths.Text.Trim());

                CurrentToolInstance.VoltageConversion.CurrentConversionType = EnumHelper.GetVCEnumValue(rdoListConvType.SelectedValue);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnVCClearAll_Click(object sender, EventArgs e)
        {
            ClearFields(Page.Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnVCCalcAll_Click(object sender, EventArgs e)
        {
            Calculate();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Calculate()
        {
            CaptureFields();

            CurrentToolInstance.VoltageConversion = CurrentToolInstance.VoltageConversion.Calculate(CurrentToolInstance.VoltageConversion);

            if (CurrentToolInstance.VoltageConversion != null)
            {
                CurrentToolInstance.WireCalculator = new BusinessLayer.WireCalculatorInstance();
                CurrentToolInstance.SetWireDiameterToWireCalculator(CurrentToolInstance.VoltageConversion.WireDiameterNew);
            }

            PopulateFields();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdoListConvType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrentToolInstance != null && CurrentToolInstance.VoltageConversion != null)
            {
                CaptureFields();
                txtVCODConnection.Text = CurrentToolInstance.VoltageConversion.ConnectionOriginal;
                txtVCNDConnection.Text = CurrentToolInstance.VoltageConversion.ConnectionNew;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Calculate();
            Session["VCInstance"] = CurrentToolInstance.VoltageConversion;
            ResponseHelper.Redirect("PrintCalcJob.aspx?CalcType=" + EnumHelper.GetStringValue(CalculationToolType.VoltageConversion), "_blank", "menubar=0,width=800,height=600");
        }

        #endregion
    }
}