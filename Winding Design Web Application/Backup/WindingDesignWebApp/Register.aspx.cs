﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WindingDesign.BusinessLayer;

namespace WindingDesign.WebApp
{
    public partial class Register : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        private bool _isUpdate;

        /// <summary>
        /// 
        /// </summary>
        private string _currentCaptchaImageText;

        /// <summary>
        /// 
        /// </summary>
        public string CurrentCaptchaImageText
        {
            get
            {
                _currentCaptchaImageText = Session["CurrentCaptchaImageText"] != null ? Session["CurrentCaptchaImageText"].ToString() : string.Empty;
                return _currentCaptchaImageText;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentTitle
        {
            get
            {
                if (CurrentClient != null)
                {
                    return "Edit Profile";
                }
                else
                {
                    return "Create New Account";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool IsUpdate
        {
            get
            {
                if (CurrentClient != null)
                { _isUpdate = true; }
                else
                { _isUpdate = false; }
                return _isUpdate;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lstCountries.DataSource = CountriesList;
                lstCountries.DataBind();
                PopulateFields();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void PopulateFields()
        {
            lblTitle.Text = CurrentTitle;

            if (CurrentClient != null)
            {
                lstTitle.SelectedValue = CurrentClient.Title;
                txtFirstName.Text = CurrentClient.FirstName;
                txtLastName.Text = CurrentClient.LastName;
                txtCompanyName.Text = CurrentClient.CompanyName;
                txtVATNumber.Text = CurrentClient.VATNumber;
                txtEmail.Text = CurrentClient.EmailAddress;
                txtEmail.ReadOnly = true;
                txtAddressLine1.Text = CurrentClient.AddressLine1;
                txtAddressLine2.Text = CurrentClient.AddressLine2;
                txtCounty.Text = CurrentClient.County;
                txtPostCode.Text = CurrentClient.PostCode;
                lstCountries.SelectedValue = CurrentClient.Country;
                txtTelephone.Text = CurrentClient.TelephoneNumber;
                txtMobile.Text = CurrentClient.MobileNumber;
                btnCreateUserButton.Text = "Save";
            }
            else
            {
                txtEmail.ReadOnly = false;
                btnCreateUserButton.Text = "Create Account";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreateUserButton_Click(object sender, EventArgs e)
        {
            if (txtCaptcha.Text.Trim() == CurrentCaptchaImageText)
            {
                if ((!IsUpdate && BusinessLayer.DBHelper.ReturnClientByEmailAddress(txtEmail.Text.Trim()) == null) || IsUpdate)
                {
                    int cClientID = CurrentClient != null ? CurrentClient.ClientID : 0;

                    AddEditClient(cClientID, lstTitle.SelectedValue, txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtEmail.Text.Trim(), txtCompanyName.Text.Trim(), txtVATNumber.Text.Trim(), txtAddressLine1.Text.Trim(), txtAddressLine2.Text.Trim(), txtCounty.Text.Trim(), lstCountries.SelectedValue, txtPostCode.Text.Trim(), txtTelephone.Text.Trim(), txtMobile.Text.Trim(), txtPassword.Text.Trim());

                    string EmailMessage = string.Empty;

                    if (IsUpdate)
                    {
                        EmailMessage = "Thank you for updating your profile with windingdesign.co.uk.";
                    }
                    else
                    {
                        EmailMessage = "Thank you for registering with www.windingdesign.co.uk";
                        SendEmail("Winding Design" + Environment.NewLine, "Tony", "admin@windingdesign.co.uk", "solentrewinds@aol.com", "New user registered in your site WindingDesign.co.uk", "New user registered tou your site. User Name: " + lstTitle.SelectedValue + " " + txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim() + ", Email: " + txtEmail.Text.Trim());
                    }

                    SendEmail("Winding Design" + Environment.NewLine, lstTitle.SelectedValue + " " + txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim(), "admin@windingdesign.co.uk", txtEmail.Text.Trim(), "Message from WindingDesign.co.uk", EmailMessage);

                    Response.Redirect("WireCalculator.aspx");
                }
                else
                {
                    ErrorMessage.Text = "Sorry, the email already exists. Use another one or if you forgot your password go to password reminder screen and your password will be emailed to you.";
                }
            }
            else
            {
                ErrorMessage.Text = "The words you typed in from the image is not quite right. Please try again.";
                txtCaptcha.Text = string.Empty;
            }
        }        
    }
}
