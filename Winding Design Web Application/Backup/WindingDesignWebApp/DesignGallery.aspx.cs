﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    public partial class DesignGallery : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsAdmin
        {
            get
            {
                if (CurrentClient.IsAdmin)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lblTitle.Text = CurrentDesignType.Description;
            }
        }
    }
}