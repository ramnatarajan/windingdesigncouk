﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WindingDesign.WebApp
{
    public class TrainingVideo
    {
        public int TrainingVideoID { get; set; }

        public string ShortDescription { get; set; }

        public string LongDescription { get; set; }

        public string VideoEmbedPath { get; set; }

        public string SearchStringText
        {
            get
            {
                return TrainingVideoID.ToString() + "|" + ShortDescription + "|" + LongDescription + "|" + VideoEmbedPath;
            }
        }
    }
}