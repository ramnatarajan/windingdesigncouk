﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WindingDesign.WebApp
{
    public class WireDesign
    {
        public int DesignID { get; set; }

        public string ShortDescription { get; set; }

        public string LongDescription { get; set; }

        public string ThumbnailPath { get; set; }

        public string ImagePath { get; set; }

        public string OriginalImagePath { get; set; }

        public string SearchStringText
        {
            get
            {
                return DesignID.ToString() + "|" + ShortDescription + "|" + LongDescription + "|";
            }
        }
    }
}