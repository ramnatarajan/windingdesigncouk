﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WindingDesign.WebApp
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientDetail
    {
        public int ClientID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string CompanyName { get; set; }
        public string VATNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string TelephoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Salt { get; set; }
        public string EncryptedPassword { get; set; }
        public DateTime RegisteredDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public DateTime? LicenceExpiryDate { get; set; }
        public bool IsLocked { get; set; }
        public bool IsAdmin { get; set; }
    }
}