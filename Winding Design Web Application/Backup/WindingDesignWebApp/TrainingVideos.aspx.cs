﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    public partial class TrainingVideos : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsAdmin
        {
            get
            {
                if (CurrentClient != null && CurrentClient.IsAdmin)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}