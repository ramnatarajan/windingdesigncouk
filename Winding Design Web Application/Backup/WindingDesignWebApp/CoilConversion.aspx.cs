﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    public partial class CoilConversion : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateFields();
            }
        }

        #region Page Button Events

        /// <summary>
        /// 
        /// </summary>
        public enum FieldsClearType
        {
            ClearAll = 0,
            ClearSpanSide = 1
        }

        /// <summary>
        /// Clears the fields.
        /// </summary>
        /// <param name="cType">Type of the c.</param>
        private void ClearFields(FieldsClearType cType)
        {
            switch (cType)
            {
                case FieldsClearType.ClearSpanSide:
                    txtCCCoilSpanOriginal1.Text = string.Empty;
                    txtCCCoilSpanOriginal2.Text = string.Empty;
                    txtCCCoilSpanOriginal3.Text = string.Empty;
                    txtCCCoilSpanOriginal4.Text = string.Empty;
                    txtCCCoilSpanOriginal5.Text = string.Empty;
                    txtCCCoilSpanOriginal6.Text = string.Empty;

                    txtCCCoilTurnsOriginal1.Text = string.Empty;
                    txtCCCoilTurnsOriginal2.Text = string.Empty;
                    txtCCCoilTurnsOriginal3.Text = string.Empty;
                    txtCCCoilTurnsOriginal4.Text = string.Empty;
                    txtCCCoilTurnsOriginal5.Text = string.Empty;
                    txtCCCoilTurnsOriginal6.Text = string.Empty;

                    txtCCCoilCFOriginal1.Text = string.Empty;
                    txtCCCoilCFOriginal2.Text = string.Empty;
                    txtCCCoilCFOriginal3.Text = string.Empty;
                    txtCCCoilCFOriginal4.Text = string.Empty;
                    txtCCCoilCFOriginal5.Text = string.Empty;
                    txtCCCoilCFOriginal6.Text = string.Empty;

                    txtCCEffectiveTurnsCoilOriginal.Text = string.Empty;

                    txtCCCoilSpanNew1.Text = string.Empty;
                    txtCCCoilSpanNew2.Text = string.Empty;
                    txtCCCoilSpanNew3.Text = string.Empty;
                    txtCCCoilSpanNew4.Text = string.Empty;
                    txtCCCoilSpanNew5.Text = string.Empty;
                    txtCCCoilSpanNew6.Text = string.Empty;

                    txtCCCoilTurnsNew1.Text = string.Empty;
                    txtCCCoilTurnsNew2.Text = string.Empty;
                    txtCCCoilTurnsNew3.Text = string.Empty;
                    txtCCCoilTurnsNew4.Text = string.Empty;
                    txtCCCoilTurnsNew5.Text = string.Empty;
                    txtCCCoilTurnsNew6.Text = string.Empty;

                    txtCCCoilCFNew1.Text = string.Empty;
                    txtCCCoilCFNew2.Text = string.Empty;
                    txtCCCoilCFNew3.Text = string.Empty;
                    txtCCCoilCFNew4.Text = string.Empty;
                    txtCCCoilCFNew5.Text = string.Empty;
                    txtCCCoilCFNew6.Text = string.Empty;

                    txtCCEffectiveTurnsCoilNew.Text = string.Empty;
                    break;
                case FieldsClearType.ClearAll:
                    ClearFields(FieldsClearType.ClearSpanSide);
                    txtCCCoilGroupOriginal.Text = string.Empty;
                    txtCCCoilSidesSlotOriginal.Text = string.Empty;
                    txtCCFullSpan.Text = string.Empty;
                    lstWindingOriginal.SelectedValue = string.Empty;
                    lstWindingNew.SelectedValue = string.Empty;
                    cboCCPolesList.SelectedValue = string.Empty;
                    cboCCNoOfSlots.SelectedValue = string.Empty;
                    txtJobIdentificationNumber.Text = string.Empty;
                    CurrentToolInstance.CoilConversion = new BusinessLayer.CoilConversionInstance();
                    break;
            }

            lblResultmessage.Visible = false;
            grdCoilConvResults.DataSource = null;
            grdCoilConvResults.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.CoilConversion != null)
            {
                txtJobIdentificationNumber.Text = CurrentToolInstance.CoilConversion.JobIdentificationNumber;

                cboCCPolesList.SelectedValue = CurrentToolInstance.CoilConversion.NoOfPoles.HasValue ? CurrentToolInstance.CoilConversion.NoOfPoles.Value.ToString() : string.Empty;
                cboCCNoOfSlots.SelectedValue = CurrentToolInstance.CoilConversion.NoOfSlots.HasValue ? CurrentToolInstance.CoilConversion.NoOfSlots.Value.ToString() : string.Empty;
                txtCCCoilGroupOriginal.Text = CurrentToolInstance.CoilConversion.CoilGroupOriginal.HasValue ? CurrentToolInstance.CoilConversion.CoilGroupOriginal.Value.ToString() : string.Empty;
                txtCCCoilSidesSlotOriginal.Text = CurrentToolInstance.CoilConversion.CoilSidesSlotOriginal.HasValue ? CurrentToolInstance.CoilConversion.CoilSidesSlotOriginal.Value.ToString() : string.Empty;
                txtCCFullSpan.Text = CurrentToolInstance.CoilConversion.FullSpan.HasValue ? CurrentToolInstance.CoilConversion.FullSpan.Value.ToString() : string.Empty;
                lstWindingOriginal.SelectedValue = EnumHelper.GetStringValue(CurrentToolInstance.CoilConversion.WindingOriginal);
                lstWindingNew.SelectedValue = EnumHelper.GetStringValue(CurrentToolInstance.CoilConversion.WindingNew);

                txtCCCoilSpanOriginal1.Text = CurrentToolInstance.CoilConversion.CoilSpanOriginal1.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanOriginal1.Value.ToString() : string.Empty;
                txtCCCoilSpanOriginal2.Text = CurrentToolInstance.CoilConversion.CoilSpanOriginal2.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanOriginal2.Value.ToString() : string.Empty;
                txtCCCoilSpanOriginal3.Text = CurrentToolInstance.CoilConversion.CoilSpanOriginal3.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanOriginal3.Value.ToString() : string.Empty;
                txtCCCoilSpanOriginal4.Text = CurrentToolInstance.CoilConversion.CoilSpanOriginal4.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanOriginal4.Value.ToString() : string.Empty;
                txtCCCoilSpanOriginal5.Text = CurrentToolInstance.CoilConversion.CoilSpanOriginal5.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanOriginal5.Value.ToString() : string.Empty;
                txtCCCoilSpanOriginal6.Text = CurrentToolInstance.CoilConversion.CoilSpanOriginal6.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanOriginal6.Value.ToString() : string.Empty;

                txtCCCoilTurnsOriginal1.Text = CurrentToolInstance.CoilConversion.CoilTurnsOriginal1.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsOriginal1.Value.ToString() : string.Empty;
                txtCCCoilTurnsOriginal2.Text = CurrentToolInstance.CoilConversion.CoilTurnsOriginal2.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsOriginal2.Value.ToString() : string.Empty;
                txtCCCoilTurnsOriginal3.Text = CurrentToolInstance.CoilConversion.CoilTurnsOriginal3.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsOriginal3.Value.ToString() : string.Empty;
                txtCCCoilTurnsOriginal4.Text = CurrentToolInstance.CoilConversion.CoilTurnsOriginal4.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsOriginal4.Value.ToString() : string.Empty;
                txtCCCoilTurnsOriginal5.Text = CurrentToolInstance.CoilConversion.CoilTurnsOriginal5.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsOriginal5.Value.ToString() : string.Empty;
                txtCCCoilTurnsOriginal6.Text = CurrentToolInstance.CoilConversion.CoilTurnsOriginal6.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsOriginal6.Value.ToString() : string.Empty;

                txtCCCoilCFOriginal1.Text = CurrentToolInstance.CoilConversion.CoilCFOriginal1.HasValue ? CurrentToolInstance.CoilConversion.CoilCFOriginal1.Value.ToString() : string.Empty;
                txtCCCoilCFOriginal2.Text = CurrentToolInstance.CoilConversion.CoilCFOriginal2.HasValue ? CurrentToolInstance.CoilConversion.CoilCFOriginal2.Value.ToString() : string.Empty;
                txtCCCoilCFOriginal3.Text = CurrentToolInstance.CoilConversion.CoilCFOriginal3.HasValue ? CurrentToolInstance.CoilConversion.CoilCFOriginal3.Value.ToString() : string.Empty;
                txtCCCoilCFOriginal4.Text = CurrentToolInstance.CoilConversion.CoilCFOriginal4.HasValue ? CurrentToolInstance.CoilConversion.CoilCFOriginal4.Value.ToString() : string.Empty;
                txtCCCoilCFOriginal5.Text = CurrentToolInstance.CoilConversion.CoilCFOriginal5.HasValue ? CurrentToolInstance.CoilConversion.CoilCFOriginal5.Value.ToString() : string.Empty;
                txtCCCoilCFOriginal6.Text = CurrentToolInstance.CoilConversion.CoilCFOriginal6.HasValue ? CurrentToolInstance.CoilConversion.CoilCFOriginal6.Value.ToString() : string.Empty;

                txtCCEffectiveTurnsCoilOriginal.Text = CurrentToolInstance.CoilConversion.EffectiveTurnsCoilOriginal.HasValue ? CurrentToolInstance.CoilConversion.EffectiveTurnsCoilOriginal.Value.ToString() : string.Empty;

                txtCCCoilSpanNew1.Text = CurrentToolInstance.CoilConversion.CoilSpanNew1.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanNew1.Value.ToString() : string.Empty;
                txtCCCoilSpanNew2.Text = CurrentToolInstance.CoilConversion.CoilSpanNew2.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanNew2.Value.ToString() : string.Empty;
                txtCCCoilSpanNew3.Text = CurrentToolInstance.CoilConversion.CoilSpanNew3.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanNew3.Value.ToString() : string.Empty;
                txtCCCoilSpanNew4.Text = CurrentToolInstance.CoilConversion.CoilSpanNew4.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanNew4.Value.ToString() : string.Empty;
                txtCCCoilSpanNew5.Text = CurrentToolInstance.CoilConversion.CoilSpanNew5.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanNew5.Value.ToString() : string.Empty;
                txtCCCoilSpanNew6.Text = CurrentToolInstance.CoilConversion.CoilSpanNew6.HasValue ? CurrentToolInstance.CoilConversion.CoilSpanNew6.Value.ToString() : string.Empty;

                txtCCCoilTurnsNew1.Text = CurrentToolInstance.CoilConversion.CoilTurnsNew1.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsNew1.Value.ToString() : string.Empty;
                txtCCCoilTurnsNew2.Text = CurrentToolInstance.CoilConversion.CoilTurnsNew2.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsNew2.Value.ToString() : string.Empty;
                txtCCCoilTurnsNew3.Text = CurrentToolInstance.CoilConversion.CoilTurnsNew3.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsNew3.Value.ToString() : string.Empty;
                txtCCCoilTurnsNew4.Text = CurrentToolInstance.CoilConversion.CoilTurnsNew4.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsNew4.Value.ToString() : string.Empty;
                txtCCCoilTurnsNew5.Text = CurrentToolInstance.CoilConversion.CoilTurnsNew5.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsNew5.Value.ToString() : string.Empty;
                txtCCCoilTurnsNew6.Text = CurrentToolInstance.CoilConversion.CoilTurnsNew6.HasValue ? CurrentToolInstance.CoilConversion.CoilTurnsNew6.Value.ToString() : string.Empty;

                txtCCCoilCFNew1.Text = CurrentToolInstance.CoilConversion.CoilCFNew1.HasValue ? CurrentToolInstance.CoilConversion.CoilCFNew1.Value.ToString() : string.Empty;
                txtCCCoilCFNew2.Text = CurrentToolInstance.CoilConversion.CoilCFNew2.HasValue ? CurrentToolInstance.CoilConversion.CoilCFNew2.Value.ToString() : string.Empty;
                txtCCCoilCFNew3.Text = CurrentToolInstance.CoilConversion.CoilCFNew3.HasValue ? CurrentToolInstance.CoilConversion.CoilCFNew3.Value.ToString() : string.Empty;
                txtCCCoilCFNew4.Text = CurrentToolInstance.CoilConversion.CoilCFNew4.HasValue ? CurrentToolInstance.CoilConversion.CoilCFNew4.Value.ToString() : string.Empty;
                txtCCCoilCFNew5.Text = CurrentToolInstance.CoilConversion.CoilCFNew5.HasValue ? CurrentToolInstance.CoilConversion.CoilCFNew5.Value.ToString() : string.Empty;
                txtCCCoilCFNew6.Text = CurrentToolInstance.CoilConversion.CoilCFNew6.HasValue ? CurrentToolInstance.CoilConversion.CoilCFNew6.Value.ToString() : string.Empty;

                txtCCEffectiveTurnsCoilNew.Text = CurrentToolInstance.CoilConversion.EffectiveTurnsCoilNew.HasValue ? CurrentToolInstance.CoilConversion.EffectiveTurnsCoilNew.Value.ToString() : string.Empty;
            }

            lblResultmessage.Visible = CurrentToolInstance != null && CurrentToolInstance.CoilConversion != null && CurrentToolInstance.CoilConversion.ResultMessages != null && CurrentToolInstance.CoilConversion.ResultMessages.Count > 0 ? true : false;
            grdCoilConvResults.DataSource = CurrentToolInstance != null && CurrentToolInstance.CoilConversion != null && CurrentToolInstance.CoilConversion.ResultMessages != null && CurrentToolInstance.CoilConversion.ResultMessages.Count > 0 ? CurrentToolInstance.CoilConversion.ResultMessages : null;
            grdCoilConvResults.DataBind();

            if (!Page.IsPostBack)
            {
                AttachAlphaRestrictionToTextBoxes(Page.Controls);
            }

            SetMaxLengthAndToolTipToTextBoxes(Page.Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        private void CaptureFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.CoilConversion != null)
            {
                CurrentToolInstance.CoilConversion.JobIdentificationNumber = txtJobIdentificationNumber.Text.Trim();

                CurrentToolInstance.CoilConversion.NoOfPoles = GetDouble(cboCCPolesList.SelectedValue);
                CurrentToolInstance.CoilConversion.NoOfSlots = GetDouble(cboCCNoOfSlots.SelectedValue);
                CurrentToolInstance.CoilConversion.CoilGroupOriginal = GetDouble(txtCCCoilGroupOriginal.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSidesSlotOriginal = GetDouble(txtCCCoilSidesSlotOriginal.Text.Trim());
                CurrentToolInstance.CoilConversion.WindingOriginal = EnumHelper.GetCCEnumValue(lstWindingOriginal.SelectedValue);
                CurrentToolInstance.CoilConversion.WindingNew = EnumHelper.GetCCEnumValue(lstWindingNew.SelectedValue);
                CurrentToolInstance.CoilConversion.FullSpan = GetDouble(txtCCFullSpan.Text.Trim());

                CurrentToolInstance.CoilConversion.CoilSpanOriginal1 = GetDouble(txtCCCoilSpanOriginal1.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanOriginal2 = GetDouble(txtCCCoilSpanOriginal2.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanOriginal3 = GetDouble(txtCCCoilSpanOriginal3.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanOriginal4 = GetDouble(txtCCCoilSpanOriginal4.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanOriginal5 = GetDouble(txtCCCoilSpanOriginal5.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanOriginal6 = GetDouble(txtCCCoilSpanOriginal6.Text.Trim());

                CurrentToolInstance.CoilConversion.CoilTurnsOriginal1 = GetDouble(txtCCCoilTurnsOriginal1.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsOriginal2 = GetDouble(txtCCCoilTurnsOriginal2.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsOriginal3 = GetDouble(txtCCCoilTurnsOriginal3.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsOriginal4 = GetDouble(txtCCCoilTurnsOriginal4.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsOriginal5 = GetDouble(txtCCCoilTurnsOriginal5.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsOriginal6 = GetDouble(txtCCCoilTurnsOriginal6.Text.Trim());

                CurrentToolInstance.CoilConversion.CoilCFOriginal1 = GetDouble(txtCCCoilCFOriginal1.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFOriginal2 = GetDouble(txtCCCoilCFOriginal2.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFOriginal3 = GetDouble(txtCCCoilCFOriginal3.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFOriginal4 = GetDouble(txtCCCoilCFOriginal4.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFOriginal5 = GetDouble(txtCCCoilCFOriginal5.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFOriginal6 = GetDouble(txtCCCoilCFOriginal6.Text.Trim());

                CurrentToolInstance.CoilConversion.EffectiveTurnsCoilOriginal = GetDouble(txtCCEffectiveTurnsCoilOriginal.Text.Trim());

                CurrentToolInstance.CoilConversion.CoilSpanNew1 = GetDouble(txtCCCoilSpanNew1.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanNew2 = GetDouble(txtCCCoilSpanNew2.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanNew3 = GetDouble(txtCCCoilSpanNew3.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanNew4 = GetDouble(txtCCCoilSpanNew4.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanNew5 = GetDouble(txtCCCoilSpanNew5.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilSpanNew6 = GetDouble(txtCCCoilSpanNew6.Text.Trim());

                CurrentToolInstance.CoilConversion.CoilTurnsNew1 = GetDouble(txtCCCoilTurnsNew1.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsNew2 = GetDouble(txtCCCoilTurnsNew2.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsNew3 = GetDouble(txtCCCoilTurnsNew3.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsNew4 = GetDouble(txtCCCoilTurnsNew4.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsNew5 = GetDouble(txtCCCoilTurnsNew5.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilTurnsNew6 = GetDouble(txtCCCoilTurnsNew6.Text.Trim());

                CurrentToolInstance.CoilConversion.CoilCFNew1 = GetDouble(txtCCCoilCFNew1.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFNew2 = GetDouble(txtCCCoilCFNew2.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFNew3 = GetDouble(txtCCCoilCFNew3.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFNew4 = GetDouble(txtCCCoilCFNew4.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFNew5 = GetDouble(txtCCCoilCFNew5.Text.Trim());
                CurrentToolInstance.CoilConversion.CoilCFNew6 = GetDouble(txtCCCoilCFNew6.Text.Trim());

                CurrentToolInstance.CoilConversion.EffectiveTurnsCoilNew = GetDouble(txtCCEffectiveTurnsCoilNew.Text.Trim());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Calculate()
        {
            CaptureFields();

            CurrentToolInstance.CoilConversion = CurrentToolInstance.CoilConversion.Calculate(CurrentToolInstance.CoilConversion);

            PopulateFields();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Calculate();
            Session["CCInstance"] = CurrentToolInstance.CoilConversion;
            ResponseHelper.Redirect("PrintCalcJob.aspx?CalcType=" + EnumHelper.GetStringValue(CalculationToolType.CoilConversion), "_blank", "menubar=0,width=800,height=600");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGoBack_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCalcAll_Click(object sender, EventArgs e)
        {
            Calculate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClearAll_Click(object sender, EventArgs e)
        {
            ClearFields(FieldsClearType.ClearAll);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAutoFill_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnManualFill_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClearSpansAndTurns_Click(object sender, EventArgs e)
        {
            ClearFields(FieldsClearType.ClearSpanSide);
        }

        #endregion
    }
}