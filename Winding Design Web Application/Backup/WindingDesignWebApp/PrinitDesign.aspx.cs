﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    public partial class PrinitDesign : System.Web.UI.Page
    {
        /// <summary>
        /// 
        /// </summary>
        private string currentWireDesignURL;

        /// <summary>
        /// 
        /// </summary>
        public string CurrentWireDesignURL
        {
            get
            {
                currentWireDesignURL = Request.QueryString["DesignURL"] != null && Request.QueryString["DesignURL"].ToString() != "" ? Request.QueryString["DesignURL"].ToString() : string.Empty;
                return currentWireDesignURL;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                imgDesign.Src = CurrentWireDesignURL;
            }
        }
    }
}