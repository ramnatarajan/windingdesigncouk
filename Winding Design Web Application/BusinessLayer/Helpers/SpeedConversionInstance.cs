﻿// -----------------------------------------------------------------------
// <copyright file="SpeedConversionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace WindingDesign.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SpeedConversionInstance
    {
        #region Calculation Region

        /// <summary>
        /// 
        /// </summary>
        public List<string> ResultMessages { get; set; }

        /// <summary>
        /// Gets or sets the job identification number.
        /// </summary>
        /// <value>
        /// The job identification number.
        /// </value>
        public string JobIdentificationNumber { get; set; }

        #endregion

        #region Original Data

        public double? OutputPowerOriginal { get; set; }
        public double? NoOfSlotsOriginal { get; set; }
        public double? NoOfPolesOriginal { get; set; }
        public double? CoreLengthOriginal { get; set; }
        public double? CoreDiameterOriginal { get; set; }
        public double? NoOfCurrentPathsOriginal { get; set; }
        public double? TurnsSlotOriginal { get; set; }
        public double? SpanOriginal { get; set; }
        public double? WireDiameterOriginal { get; set; }
        public double? WireAreaOriginal { get; set; }
        public double? MaxCoreRatingOriginal { get; set; }
        public double? TurnsSlotFullSpanOriginal { get; set; }

        #endregion

        #region New Data

        public double? OutputPowerNew { get; set; }
        public double? NoOfPolesNew { get; set; }
        public double? MaxCoreRatingNew { get; set; }
        public double? NoOfCurrentPathsNew { get; set; }
        public double? TurnsSlotNew { get; set; }
        public double? AreaNew { get; set; }
        public double? WireDiameterNew { get; set; }
        public double? FullSpanNew { get; set; }

        #endregion

        #region Business Logic
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cType"></param>
        /// <param name="cInstance"></param>
        /// <returns></returns>
        public SpeedConversionInstance Calculate(SpeedConversionInstance cInstance)
        {
            this.SetCurrentInstance(cInstance);
            ResultMessages = new List<string>();

            if (this.OutputPowerOriginal.HasValue && this.NoOfPolesOriginal.HasValue
                && this.NoOfPolesNew.HasValue && this.SpanOriginal.HasValue
                && this.CoreLengthOriginal.HasValue && this.CoreDiameterOriginal.HasValue
                && this.NoOfCurrentPathsOriginal.HasValue && this.WireDiameterOriginal.HasValue
                && this.OutputPowerNew.HasValue && this.NoOfCurrentPathsNew.HasValue
                && this.MaxCoreRatingNew.HasValue && this.TurnsSlotOriginal.HasValue
                && this.NoOfSlotsOriginal.HasValue)
            {
                double Volume = Math.Pow(((this.CoreDiameterOriginal.HasValue ? this.CoreDiameterOriginal.Value : 0) / 25.4), 2) * ((this.CoreLengthOriginal.HasValue ? this.CoreLengthOriginal.Value : 0) / 25.4);
                this.MaxCoreRatingOriginal = Math.Round(Volume / GetVolumeByPoles(this.NoOfPolesOriginal.HasValue ? (int)this.NoOfPolesOriginal.Value : 0), 4);
                this.TurnsSlotFullSpanOriginal = Math.Round(this.TurnsSlotOriginal.Value * (Math.Sin(90 * (this.SpanOriginal.Value - 1)) / (this.NoOfSlotsOriginal.Value / this.NoOfPolesOriginal.Value)), 4);
                this.TurnsSlotNew = Math.Round(this.TurnsSlotFullSpanOriginal.Value * Math.Pow((this.NoOfPolesNew.Value / this.NoOfPolesOriginal.Value), 0.5) * Math.Pow((this.OutputPowerOriginal.Value / this.OutputPowerNew.Value), 0.5), 4);
                this.FullSpanNew = Math.Round((this.NoOfSlotsOriginal.Value / this.NoOfPolesOriginal.Value) + 1, 4);
                this.WireAreaOriginal = Math.Round(Math.Pow((this.WireDiameterOriginal.Value / 2), 2) * Math.PI, 4);
                this.AreaNew = Math.Round(this.WireAreaOriginal.Value * (this.OutputPowerNew.Value / this.OutputPowerOriginal.Value), 4);
                this.WireDiameterNew = Math.Round(Math.Pow((this.AreaNew.Value / Math.PI), 0.5) * 2, 4);

                ResultMessages.Add("Original Wire Area: " + this.WireAreaOriginal.Value.ToString());
                ResultMessages.Add("Original Max. Core Rating (kW's): " + this.MaxCoreRatingOriginal.Value.ToString());
                ResultMessages.Add("Turns/Slot at Full Span: " + this.TurnsSlotFullSpanOriginal.Value.ToString());
                ResultMessages.Add("New Turns/Slot: " + this.TurnsSlotNew.Value.ToString());
                ResultMessages.Add("New Area: " + this.AreaNew.Value.ToString());
                ResultMessages.Add("New Wire Diameter: " + this.WireDiameterNew.Value.ToString());
                ResultMessages.Add("New Full Span 1 - : " + this.FullSpanNew.Value.ToString());
            }
            else
            {
                ResultMessages.Add("Enter the data in the boxes (Left and Right) and click calc!");
            }

            return this;
        }

        /// <summary>
        /// Gets the volume by poles.
        /// </summary>
        /// <param name="noPoles">The no poles.</param>
        /// <returns></returns>
        public double GetVolumeByPoles(int noPoles)
        {
            switch (noPoles)
            {
                case 2:
                    return 12.9;
                case 4:
                    return 15.6;
                case 6:
                    return 27.5;
                case 8:
                    return 38.4;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cInstance"></param>
        private void SetCurrentInstance(SpeedConversionInstance cInstance)
        {
            if (cInstance != null)
            {
                this.OutputPowerOriginal = cInstance.OutputPowerOriginal;
                this.NoOfSlotsOriginal = cInstance.NoOfSlotsOriginal;
                this.NoOfPolesOriginal = cInstance.NoOfPolesOriginal;
                this.CoreLengthOriginal = cInstance.CoreLengthOriginal;
                this.CoreDiameterOriginal = cInstance.CoreDiameterOriginal;
                this.NoOfCurrentPathsOriginal = cInstance.NoOfCurrentPathsOriginal;
                this.TurnsSlotOriginal = cInstance.TurnsSlotOriginal;
                this.SpanOriginal = cInstance.SpanOriginal;
                this.WireDiameterOriginal = cInstance.WireDiameterOriginal;
                this.WireAreaOriginal = cInstance.WireAreaOriginal;
                this.MaxCoreRatingOriginal = cInstance.MaxCoreRatingOriginal;
                this.TurnsSlotFullSpanOriginal = cInstance.TurnsSlotFullSpanOriginal;
                this.OutputPowerNew = cInstance.OutputPowerNew;
                this.NoOfPolesNew = cInstance.NoOfPolesNew;
                this.MaxCoreRatingNew = cInstance.MaxCoreRatingNew;
                this.NoOfCurrentPathsNew = cInstance.NoOfCurrentPathsNew;
                this.TurnsSlotNew = cInstance.TurnsSlotNew;
                this.AreaNew = cInstance.AreaNew;
                this.WireDiameterNew = cInstance.WireDiameterNew;
                this.FullSpanNew = cInstance.FullSpanNew;
            }
        }

        #endregion
    }
}