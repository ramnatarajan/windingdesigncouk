﻿// -----------------------------------------------------------------------
// <copyright file="CoilConversionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace WindingDesign.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CoilConversionInstance
    {
        #region Calculation Region

        /// <summary>
        /// 
        /// </summary>
        public enum WindingType
        {
            [StringValue("Concentric")]
            Concentric = 1,

            [StringValue("Round")]
            Round = 2
        }

        /// <summary>
        /// 
        /// </summary>
        public WindingType WindingOriginal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public WindingType WindingNew { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> ResultMessages { get; set; }

        /// <summary>
        /// Gets or sets the job identification number.
        /// </summary>
        /// <value>
        /// The job identification number.
        /// </value>
        public string JobIdentificationNumber { get; set; }

        #endregion

        #region Data Fields

        public double? NoOfPoles { get; set; }
        public double? NoOfSlots { get; set; }
        public double? CoilGroupOriginal { get; set; }
        public double? CoilSidesSlotOriginal { get; set; }
        public double? FullSpan { get; set; }

        public double? CoilSpanOriginal1 { get; set; }
        public double? CoilSpanOriginal2 { get; set; }
        public double? CoilSpanOriginal3 { get; set; }
        public double? CoilSpanOriginal4 { get; set; }
        public double? CoilSpanOriginal5 { get; set; }
        public double? CoilSpanOriginal6 { get; set; }

        public double? CoilTurnsOriginal1 { get; set; }
        public double? CoilTurnsOriginal2 { get; set; }
        public double? CoilTurnsOriginal3 { get; set; }
        public double? CoilTurnsOriginal4 { get; set; }
        public double? CoilTurnsOriginal5 { get; set; }
        public double? CoilTurnsOriginal6 { get; set; }

        public double? CoilCFOriginal1 { get; set; }
        public double? CoilCFOriginal2 { get; set; }
        public double? CoilCFOriginal3 { get; set; }
        public double? CoilCFOriginal4 { get; set; }
        public double? CoilCFOriginal5 { get; set; }
        public double? CoilCFOriginal6 { get; set; }

        public double? EffectiveTurnsCoilOriginal { get; set; }

        public double? CoilSpanNew1 { get; set; }
        public double? CoilSpanNew2 { get; set; }
        public double? CoilSpanNew3 { get; set; }
        public double? CoilSpanNew4 { get; set; }
        public double? CoilSpanNew5 { get; set; }
        public double? CoilSpanNew6 { get; set; }

        public double? CoilTurnsNew1 { get; set; }
        public double? CoilTurnsNew2 { get; set; }
        public double? CoilTurnsNew3 { get; set; }
        public double? CoilTurnsNew4 { get; set; }
        public double? CoilTurnsNew5 { get; set; }
        public double? CoilTurnsNew6 { get; set; }

        public double? CoilCFNew1 { get; set; }
        public double? CoilCFNew2 { get; set; }
        public double? CoilCFNew3 { get; set; }
        public double? CoilCFNew4 { get; set; }
        public double? CoilCFNew5 { get; set; }
        public double? CoilCFNew6 { get; set; }

        public double? EffectiveTurnsCoilNew { get; set; }

        #endregion

        #region Business Logic

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cType"></param>
        /// <param name="cInstance"></param>
        /// <returns></returns>
        public CoilConversionInstance Calculate(CoilConversionInstance cInstance)
        {
            this.SetCurrentInstance(cInstance);
            ResultMessages = new List<string> { "This page is under construction", "This page is under construction" };


            // TODO:

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cInstance"></param>
        private void SetCurrentInstance(CoilConversionInstance cInstance)
        {
            if (cInstance != null)
            {
                this.WindingOriginal = cInstance.WindingOriginal;
                this.WindingNew = cInstance.WindingNew;

                this.NoOfPoles = cInstance.NoOfPoles;
                this.NoOfSlots = cInstance.NoOfSlots;
                this.CoilGroupOriginal = cInstance.CoilGroupOriginal;
                this.CoilSidesSlotOriginal = cInstance.CoilSidesSlotOriginal;
                this.FullSpan = cInstance.FullSpan;

                this.CoilSpanOriginal1 = cInstance.CoilSpanOriginal1;
                this.CoilSpanOriginal2 = cInstance.CoilSpanOriginal2;
                this.CoilSpanOriginal3 = cInstance.CoilSpanOriginal3;
                this.CoilSpanOriginal4 = cInstance.CoilSpanOriginal4;
                this.CoilSpanOriginal5 = cInstance.CoilSpanOriginal5;
                this.CoilSpanOriginal6 = cInstance.CoilSpanOriginal6;

                this.CoilTurnsOriginal1 = cInstance.CoilTurnsOriginal1;
                this.CoilTurnsOriginal2 = cInstance.CoilTurnsOriginal2;
                this.CoilTurnsOriginal3 = cInstance.CoilTurnsOriginal3;
                this.CoilTurnsOriginal4 = cInstance.CoilTurnsOriginal4;
                this.CoilTurnsOriginal5 = cInstance.CoilTurnsOriginal5;
                this.CoilTurnsOriginal6 = cInstance.CoilTurnsOriginal6;

                this.CoilCFOriginal1 = cInstance.CoilCFOriginal1;
                this.CoilCFOriginal2 = cInstance.CoilCFOriginal2;
                this.CoilCFOriginal3 = cInstance.CoilCFOriginal3;
                this.CoilCFOriginal4 = cInstance.CoilCFOriginal4;
                this.CoilCFOriginal5 = cInstance.CoilCFOriginal5;
                this.CoilCFOriginal6 = cInstance.CoilCFOriginal6;

                this.EffectiveTurnsCoilOriginal = cInstance.EffectiveTurnsCoilOriginal;

                this.CoilSpanNew1 = cInstance.CoilSpanNew1;
                this.CoilSpanNew2 = cInstance.CoilSpanNew2;
                this.CoilSpanNew3 = cInstance.CoilSpanNew3;
                this.CoilSpanNew4 = cInstance.CoilSpanNew4;
                this.CoilSpanNew5 = cInstance.CoilSpanNew5;
                this.CoilSpanNew6 = cInstance.CoilSpanNew6;

                this.CoilTurnsNew1 = cInstance.CoilTurnsNew1;
                this.CoilTurnsNew2 = cInstance.CoilTurnsNew2;
                this.CoilTurnsNew3 = cInstance.CoilTurnsNew3;
                this.CoilTurnsNew4 = cInstance.CoilTurnsNew4;
                this.CoilTurnsNew5 = cInstance.CoilTurnsNew5;
                this.CoilTurnsNew6 = cInstance.CoilTurnsNew6;

                this.CoilCFNew1 = cInstance.CoilCFNew1;
                this.CoilCFNew2 = cInstance.CoilCFNew2;
                this.CoilCFNew3 = cInstance.CoilCFNew3;
                this.CoilCFNew4 = cInstance.CoilCFNew4;
                this.CoilCFNew5 = cInstance.CoilCFNew5;
                this.CoilCFNew6 = cInstance.CoilCFNew6;

                this.EffectiveTurnsCoilNew = cInstance.EffectiveTurnsCoilNew;
            }
        }

        #endregion
    }
}
