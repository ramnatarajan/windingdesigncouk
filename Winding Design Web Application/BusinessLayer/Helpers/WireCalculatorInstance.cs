﻿// -----------------------------------------------------------------------
// <copyright file="WireCalculatorInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace WindingDesign.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class WireCalculatorInstance
    {
        #region Calculation Region

        /// <summary>
        /// 
        /// </summary>
        public enum CalculatorMode
        {
            Metric = 1,
            Imperial = 2
        }

        /// <summary>
        /// 
        /// </summary>
        public CalculatorMode CurrentCalculationMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentCalculationModeString
        {
            get
            {
                switch (CurrentCalculationMode)
                {
                    case CalculatorMode.Imperial:
                        return "inches";
                    case CalculatorMode.Metric:
                        return "mm's";
                }

                return "mm's";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<string> ResultMessages { get; set; }

        /// <summary>
        /// Gets or sets the job identification number.
        /// </summary>
        /// <value>
        /// The job identification number.
        /// </value>
        public string JobIdentificationNumber { get; set; }

        #endregion

        #region Old Data

        public double? NoOfWiresOld1 { get; set; }
        public double? NoOfWiresOld2 { get; set; }
        public double? NoOfWiresOld3 { get; set; }
        public double? NoOfWiresOld4 { get; set; }
        public double? NoOfWiresOld5 { get; set; }
        public double? NoOfWiresOld6 { get; set; }
        public double? NoOfWiresOld7 { get; set; }
        public double? NoOfWiresOld8 { get; set; }
        public double? NoOfWiresOld9 { get; set; }
        public double? NoOfWiresOld10 { get; set; }

        public double? WireDiameterOld1 { get; set; }
        public double? WireDiameterOld2 { get; set; }
        public double? WireDiameterOld3 { get; set; }
        public double? WireDiameterOld4 { get; set; }
        public double? WireDiameterOld5 { get; set; }
        public double? WireDiameterOld6 { get; set; }
        public double? WireDiameterOld7 { get; set; }
        public double? WireDiameterOld8 { get; set; }
        public double? WireDiameterOld9 { get; set; }
        public double? WireDiameterOld10 { get; set; }

        public double? WireAreaOld1 { get; set; }
        public double? WireAreaOld2 { get; set; }
        public double? WireAreaOld3 { get; set; }
        public double? WireAreaOld4 { get; set; }
        public double? WireAreaOld5 { get; set; }
        public double? WireAreaOld6 { get; set; }
        public double? WireAreaOld7 { get; set; }
        public double? WireAreaOld8 { get; set; }
        public double? WireAreaOld9 { get; set; }
        public double? WireAreaOld10 { get; set; }

        public double? OldArea { get; set; }

        #endregion

        #region New Data

        public double? NoOfWiresNew1 { get; set; }
        public double? NoOfWiresNew2 { get; set; }
        public double? NoOfWiresNew3 { get; set; }
        public double? NoOfWiresNew4 { get; set; }
        public double? NoOfWiresNew5 { get; set; }
        public double? NoOfWiresNew6 { get; set; }
        public double? NoOfWiresNew7 { get; set; }
        public double? NoOfWiresNew8 { get; set; }
        public double? NoOfWiresNew9 { get; set; }
        public double? NoOfWiresNew10 { get; set; }

        public double? WireDiameterNew1 { get; set; }
        public double? WireDiameterNew2 { get; set; }
        public double? WireDiameterNew3 { get; set; }
        public double? WireDiameterNew4 { get; set; }
        public double? WireDiameterNew5 { get; set; }
        public double? WireDiameterNew6 { get; set; }
        public double? WireDiameterNew7 { get; set; }
        public double? WireDiameterNew8 { get; set; }
        public double? WireDiameterNew9 { get; set; }
        public double? WireDiameterNew10 { get; set; }

        public double? WireAreaNew1 { get; set; }
        public double? WireAreaNew2 { get; set; }
        public double? WireAreaNew3 { get; set; }
        public double? WireAreaNew4 { get; set; }
        public double? WireAreaNew5 { get; set; }
        public double? WireAreaNew6 { get; set; }
        public double? WireAreaNew7 { get; set; }
        public double? WireAreaNew8 { get; set; }
        public double? WireAreaNew9 { get; set; }
        public double? WireAreaNew10 { get; set; }

        public double? NewArea { get; set; }

        #endregion

        #region Estimator

        public double? EstimatorSectionNoOfWires { get; set; }

        #endregion

        #region Strip And Square Section

        public double? StripSectionWidth { get; set; }
        public double? StripSectionDepth { get; set; }
        public double? StripSectionArea { get; set; }

        #endregion

        #region business logic

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cZone"></param>
        /// <param name="cType"></param>
        /// <param name="cInstance"></param>
        /// <returns></returns>
        public WireCalculatorInstance Calculate(WindingDesign.BusinessLayer.DBHelper.WCCalculationZone cZone, CalculatorMode cType, WireCalculatorInstance cInstance)
        {
            this.CurrentCalculationMode = cType;
            this.SetCurrentInstance(cInstance);
            ResultMessages = new List<string>();

            switch (cZone)
            {
                case DBHelper.WCCalculationZone.CalculateAll:
                    if (NoOfWiresOld1.HasValue && WireDiameterOld1.HasValue)
                        WireAreaOld1 = Math.Round(((Math.Pow((WireDiameterOld1.Value / 2), 2)) * NoOfWiresOld1.Value * Math.PI), 10);
                    if (NoOfWiresOld2.HasValue && WireDiameterOld2.HasValue)
                        WireAreaOld2 = Math.Round(((Math.Pow((WireDiameterOld2.Value / 2), 2)) * NoOfWiresOld2.Value * Math.PI), 10);
                    if (NoOfWiresOld3.HasValue && WireDiameterOld3.HasValue)
                        WireAreaOld3 = Math.Round(((Math.Pow((WireDiameterOld3.Value / 2), 2)) * NoOfWiresOld3.Value * Math.PI), 10);
                    if (NoOfWiresOld4.HasValue && WireDiameterOld4.HasValue)
                        WireAreaOld4 = Math.Round(((Math.Pow((WireDiameterOld4.Value / 2), 2)) * NoOfWiresOld4.Value * Math.PI), 10);
                    if (NoOfWiresOld5.HasValue && WireDiameterOld5.HasValue)
                        WireAreaOld5 = Math.Round(((Math.Pow((WireDiameterOld5.Value / 2), 2)) * NoOfWiresOld5.Value * Math.PI), 10);
                    if (NoOfWiresOld6.HasValue && WireDiameterOld6.HasValue)
                        WireAreaOld6 = Math.Round(((Math.Pow((WireDiameterOld6.Value / 2), 2)) * NoOfWiresOld6.Value * Math.PI), 10);
                    if (NoOfWiresOld7.HasValue && WireDiameterOld7.HasValue)
                        WireAreaOld7 = Math.Round(((Math.Pow((WireDiameterOld7.Value / 2), 2)) * NoOfWiresOld7.Value * Math.PI), 10);
                    if (NoOfWiresOld8.HasValue && WireDiameterOld8.HasValue)
                        WireAreaOld8 = Math.Round(((Math.Pow((WireDiameterOld8.Value / 2), 2)) * NoOfWiresOld8.Value * Math.PI), 10);
                    if (NoOfWiresOld9.HasValue && WireDiameterOld9.HasValue)
                        WireAreaOld9 = Math.Round(((Math.Pow((WireDiameterOld9.Value / 2), 2)) * NoOfWiresOld9.Value * Math.PI), 10);
                    if (NoOfWiresOld10.HasValue && WireDiameterOld10.HasValue)
                        WireAreaOld10 = Math.Round(((Math.Pow((WireDiameterOld10.Value / 2), 2)) * NoOfWiresOld10.Value * Math.PI), 10);

                    if (StripSectionWidth.HasValue && StripSectionDepth.HasValue)
                    {
                        this.NoOfWiresOld1 = 1;
                        this.StripSectionArea = Math.Round((this.StripSectionWidth.Value * this.StripSectionDepth.Value), 10);
                        this.WireDiameterOld1 = Math.Round((Math.Pow((this.StripSectionArea.Value / Math.PI), 0.5) * 2), 5);
                        this.WireAreaOld1 = Math.Round((this.StripSectionWidth.Value * this.StripSectionDepth.Value), 10);
                    }

                    OldArea = (WireAreaOld1.HasValue ? WireAreaOld1.Value : 0) +
                        (WireAreaOld2.HasValue ? WireAreaOld2.Value : 0) +
                        (WireAreaOld3.HasValue ? WireAreaOld3.Value : 0) +
                        (WireAreaOld4.HasValue ? WireAreaOld4.Value : 0) +
                        (WireAreaOld5.HasValue ? WireAreaOld5.Value : 0) +
                        (WireAreaOld6.HasValue ? WireAreaOld6.Value : 0) +
                        (WireAreaOld7.HasValue ? WireAreaOld7.Value : 0) +
                        (WireAreaOld8.HasValue ? WireAreaOld8.Value : 0) +
                        (WireAreaOld9.HasValue ? WireAreaOld9.Value : 0) +
                        (WireAreaOld10.HasValue ? WireAreaOld10.Value : 0);

                    if (OldArea.HasValue && OldArea.Value == 0)
                    {
                        OldArea = null;
                        ResultMessages.Add("Enter the data in the boxes (Left) and click calc!");
                    }
                    
                    if (NoOfWiresNew1.HasValue && WireDiameterNew1.HasValue)
                        WireAreaNew1 = Math.Round(((Math.Pow((WireDiameterNew1.Value / 2), 2)) * NoOfWiresNew1.Value * Math.PI), 10);
                    if (NoOfWiresNew2.HasValue && WireDiameterNew2.HasValue)
                        WireAreaNew2 = Math.Round(((Math.Pow((WireDiameterNew2.Value / 2), 2)) * NoOfWiresNew2.Value * Math.PI), 10);
                    if (NoOfWiresNew3.HasValue && WireDiameterNew3.HasValue)
                        WireAreaNew3 = Math.Round(((Math.Pow((WireDiameterNew3.Value / 2), 2)) * NoOfWiresNew3.Value * Math.PI), 10);
                    if (NoOfWiresNew4.HasValue && WireDiameterNew4.HasValue)
                        WireAreaNew4 = Math.Round(((Math.Pow((WireDiameterNew4.Value / 2), 2)) * NoOfWiresNew4.Value * Math.PI), 10);
                    if (NoOfWiresNew5.HasValue && WireDiameterNew5.HasValue)
                        WireAreaNew5 = Math.Round(((Math.Pow((WireDiameterNew5.Value / 2), 2)) * NoOfWiresNew5.Value * Math.PI), 10);
                    if (NoOfWiresNew6.HasValue && WireDiameterNew6.HasValue)
                        WireAreaNew6 = Math.Round(((Math.Pow((WireDiameterNew6.Value / 2), 2)) * NoOfWiresNew6.Value * Math.PI), 10);
                    if (NoOfWiresNew7.HasValue && WireDiameterNew7.HasValue)
                        WireAreaNew7 = Math.Round(((Math.Pow((WireDiameterNew7.Value / 2), 2)) * NoOfWiresNew7.Value * Math.PI), 10);
                    if (NoOfWiresNew8.HasValue && WireDiameterNew8.HasValue)
                        WireAreaNew8 = Math.Round(((Math.Pow((WireDiameterNew8.Value / 2), 2)) * NoOfWiresNew8.Value * Math.PI), 10);
                    if (NoOfWiresNew9.HasValue && WireDiameterNew9.HasValue)
                        WireAreaNew9 = Math.Round(((Math.Pow((WireDiameterNew9.Value / 2), 2)) * NoOfWiresNew9.Value * Math.PI), 10);
                    if (NoOfWiresNew10.HasValue && WireDiameterNew10.HasValue)
                        WireAreaNew10 = Math.Round(((Math.Pow((WireDiameterNew10.Value / 2), 2)) * NoOfWiresNew10.Value * Math.PI), 10);

                    NewArea = (WireAreaNew1.HasValue ? WireAreaNew1.Value : 0) +
                        (WireAreaNew2.HasValue ? WireAreaNew2.Value : 0) +
                        (WireAreaNew3.HasValue ? WireAreaNew3.Value : 0) +
                        (WireAreaNew4.HasValue ? WireAreaNew4.Value : 0) +
                        (WireAreaNew5.HasValue ? WireAreaNew5.Value : 0) +
                        (WireAreaNew6.HasValue ? WireAreaNew6.Value : 0) +
                        (WireAreaNew7.HasValue ? WireAreaNew7.Value : 0) +
                        (WireAreaNew8.HasValue ? WireAreaNew8.Value : 0) +
                        (WireAreaNew9.HasValue ? WireAreaNew9.Value : 0) +
                        (WireAreaNew10.HasValue ? WireAreaNew10.Value : 0);

                    if (NewArea.HasValue && NewArea.Value == 0)
                    {
                        NewArea = null;
                        ResultMessages.Add("Enter the data in the boxes (Right) and click calc!");
                    }

                    if (this.OldArea.HasValue && this.NewArea.HasValue)
                    {
                        if (this.OldArea.Value == this.NewArea.Value)
                        {
                            ResultMessages.Add("Your new area is the same as your old area");
                        }
                        else if (this.OldArea.Value > this.NewArea.Value)
                        {
                            ResultMessages.Add("Your new area is " + Math.Round((((this.OldArea.Value - this.NewArea.Value) / this.OldArea.Value) * 100), 7).ToString() + "% smaller than your old area");
                        }
                        else if (this.OldArea.Value < this.NewArea.Value)
                        {
                            ResultMessages.Add("Your new area is " + Math.Round((((this.NewArea.Value - this.OldArea.Value) / this.OldArea.Value) * 100), 7).ToString() + "% bigger than your old area");
                        }

                        if (this.OldArea.Value != this.NewArea.Value)
                        {
                            double AreaDifference = (this.OldArea.Value - this.NewArea.Value) < 0 ? (this.NewArea.Value - this.OldArea.Value) : (this.OldArea.Value - this.NewArea.Value);
                            ResultMessages.Add("The area difference is " + Math.Round(AreaDifference, 10).ToString() + " " + CurrentCalculationModeString);
                            ResultMessages.Add("This is equal to a diameter of " + Math.Round((Math.Pow((AreaDifference / Math.PI), 0.5) * 2), 10).ToString() + " " + CurrentCalculationModeString);
                        }
                    }
                    break;
                case DBHelper.WCCalculationZone.Estimator:

                    if (EstimatorSectionNoOfWires.HasValue && EstimatorSectionNoOfWires.Value != 0)
                    {
                        Calculate(DBHelper.WCCalculationZone.OldData, cType, cInstance);
                        double cEstimate = 0;
                        if (OldArea.HasValue)
                        {
                            cEstimate = Math.Round((Math.Pow((OldArea.Value / EstimatorSectionNoOfWires.Value / Math.PI), 0.5) * 2), 7);
                            ResultMessages.Add("Try " + EstimatorSectionNoOfWires + " wires around " + cEstimate + " " + CurrentCalculationModeString + " diameter.");
                        }
                    }
                    else
                    {
                        ResultMessages.Add("Enter the no. of wires in the Estimator box (Below)!");
                    }

                    break;
                case DBHelper.WCCalculationZone.OldData:
                    if (NoOfWiresOld1.HasValue && WireDiameterOld1.HasValue)
                        WireAreaOld1 = Math.Round(((Math.Pow((WireDiameterOld1.Value / 2), 2)) * NoOfWiresOld1.Value * Math.PI), 10);
                    if (NoOfWiresOld2.HasValue && WireDiameterOld2.HasValue)
                        WireAreaOld2 = Math.Round(((Math.Pow((WireDiameterOld2.Value / 2), 2)) * NoOfWiresOld2.Value * Math.PI), 10);
                    if (NoOfWiresOld3.HasValue && WireDiameterOld3.HasValue)
                        WireAreaOld3 = Math.Round(((Math.Pow((WireDiameterOld3.Value / 2), 2)) * NoOfWiresOld3.Value * Math.PI), 10);
                    if (NoOfWiresOld4.HasValue && WireDiameterOld4.HasValue)
                        WireAreaOld4 = Math.Round(((Math.Pow((WireDiameterOld4.Value / 2), 2)) * NoOfWiresOld4.Value * Math.PI), 10);
                    if (NoOfWiresOld5.HasValue && WireDiameterOld5.HasValue)
                        WireAreaOld5 = Math.Round(((Math.Pow((WireDiameterOld5.Value / 2), 2)) * NoOfWiresOld5.Value * Math.PI), 10);
                    if (NoOfWiresOld6.HasValue && WireDiameterOld6.HasValue)
                        WireAreaOld6 = Math.Round(((Math.Pow((WireDiameterOld6.Value / 2), 2)) * NoOfWiresOld6.Value * Math.PI), 10);
                    if (NoOfWiresOld7.HasValue && WireDiameterOld7.HasValue)
                        WireAreaOld7 = Math.Round(((Math.Pow((WireDiameterOld7.Value / 2), 2)) * NoOfWiresOld7.Value * Math.PI), 10);
                    if (NoOfWiresOld8.HasValue && WireDiameterOld8.HasValue)
                        WireAreaOld8 = Math.Round(((Math.Pow((WireDiameterOld8.Value / 2), 2)) * NoOfWiresOld8.Value * Math.PI), 10);
                    if (NoOfWiresOld9.HasValue && WireDiameterOld9.HasValue)
                        WireAreaOld9 = Math.Round(((Math.Pow((WireDiameterOld9.Value / 2), 2)) * NoOfWiresOld9.Value * Math.PI), 10);
                    if (NoOfWiresOld10.HasValue && WireDiameterOld10.HasValue)
                        WireAreaOld10 = Math.Round(((Math.Pow((WireDiameterOld10.Value / 2), 2)) * NoOfWiresOld10.Value * Math.PI), 10);

                    OldArea = (WireAreaOld1.HasValue ? WireAreaOld1.Value : 0) +
                        (WireAreaOld2.HasValue ? WireAreaOld2.Value : 0) +
                        (WireAreaOld3.HasValue ? WireAreaOld3.Value : 0) +
                        (WireAreaOld4.HasValue ? WireAreaOld4.Value : 0) +
                        (WireAreaOld5.HasValue ? WireAreaOld5.Value : 0) +
                        (WireAreaOld6.HasValue ? WireAreaOld6.Value : 0) +
                        (WireAreaOld7.HasValue ? WireAreaOld7.Value : 0) +
                        (WireAreaOld8.HasValue ? WireAreaOld8.Value : 0) +
                        (WireAreaOld9.HasValue ? WireAreaOld9.Value : 0) +
                        (WireAreaOld10.HasValue ? WireAreaOld10.Value : 0);

                    if (OldArea.HasValue && OldArea.Value == 0)
                    {
                        OldArea = null;
                        ResultMessages.Add("Enter the data in the boxes (Left) and click calc!");
                    }

                    break;
                case DBHelper.WCCalculationZone.NewData:
                    if (NoOfWiresNew1.HasValue && WireDiameterNew1.HasValue)
                        WireAreaNew1 = Math.Round(((Math.Pow((WireDiameterNew1.Value / 2), 2)) * NoOfWiresNew1.Value * Math.PI), 10);
                    if (NoOfWiresNew2.HasValue && WireDiameterNew2.HasValue)
                        WireAreaNew2 = Math.Round(((Math.Pow((WireDiameterNew2.Value / 2), 2)) * NoOfWiresNew2.Value * Math.PI), 10);
                    if (NoOfWiresNew3.HasValue && WireDiameterNew3.HasValue)
                        WireAreaNew3 = Math.Round(((Math.Pow((WireDiameterNew3.Value / 2), 2)) * NoOfWiresNew3.Value * Math.PI), 10);
                    if (NoOfWiresNew4.HasValue && WireDiameterNew4.HasValue)
                        WireAreaNew4 = Math.Round(((Math.Pow((WireDiameterNew4.Value / 2), 2)) * NoOfWiresNew4.Value * Math.PI), 10);
                    if (NoOfWiresNew5.HasValue && WireDiameterNew5.HasValue)
                        WireAreaNew5 = Math.Round(((Math.Pow((WireDiameterNew5.Value / 2), 2)) * NoOfWiresNew5.Value * Math.PI), 10);
                    if (NoOfWiresNew6.HasValue && WireDiameterNew6.HasValue)
                        WireAreaNew6 = Math.Round(((Math.Pow((WireDiameterNew6.Value / 2), 2)) * NoOfWiresNew6.Value * Math.PI), 10);
                    if (NoOfWiresNew7.HasValue && WireDiameterNew7.HasValue)
                        WireAreaNew7 = Math.Round(((Math.Pow((WireDiameterNew7.Value / 2), 2)) * NoOfWiresNew7.Value * Math.PI), 10);
                    if (NoOfWiresNew8.HasValue && WireDiameterNew8.HasValue)
                        WireAreaNew8 = Math.Round(((Math.Pow((WireDiameterNew8.Value / 2), 2)) * NoOfWiresNew8.Value * Math.PI), 10);
                    if (NoOfWiresNew9.HasValue && WireDiameterNew9.HasValue)
                        WireAreaNew9 = Math.Round(((Math.Pow((WireDiameterNew9.Value / 2), 2)) * NoOfWiresNew9.Value * Math.PI), 10);
                    if (NoOfWiresNew10.HasValue && WireDiameterNew10.HasValue)
                        WireAreaNew10 = Math.Round(((Math.Pow((WireDiameterNew10.Value / 2), 2)) * NoOfWiresNew10.Value * Math.PI), 10);

                    NewArea = (WireAreaNew1.HasValue ? WireAreaNew1.Value : 0) +
                        (WireAreaNew2.HasValue ? WireAreaNew2.Value : 0) +
                        (WireAreaNew3.HasValue ? WireAreaNew3.Value : 0) +
                        (WireAreaNew4.HasValue ? WireAreaNew4.Value : 0) +
                        (WireAreaNew5.HasValue ? WireAreaNew5.Value : 0) +
                        (WireAreaNew6.HasValue ? WireAreaNew6.Value : 0) +
                        (WireAreaNew7.HasValue ? WireAreaNew7.Value : 0) +
                        (WireAreaNew8.HasValue ? WireAreaNew8.Value : 0) +
                        (WireAreaNew9.HasValue ? WireAreaNew9.Value : 0) +
                        (WireAreaNew10.HasValue ? WireAreaNew10.Value : 0);

                    if (NewArea.HasValue && NewArea.Value == 0)
                    {
                        NewArea = null;
                        ResultMessages.Add("Enter the data in the boxes (Right) and click calc!");
                    }

                    break;
                case DBHelper.WCCalculationZone.StripSquare:

                    if (StripSectionWidth.HasValue && StripSectionDepth.HasValue)
                    {
                        this.NoOfWiresOld1 = 1;
                        this.StripSectionArea = Math.Round((this.StripSectionWidth.Value * this.StripSectionDepth.Value), 10);
                        this.WireDiameterOld1 = Math.Round((Math.Pow((this.StripSectionArea.Value / Math.PI), 0.5) * 2), 5);
                        this.WireAreaOld1 = Math.Round((this.StripSectionWidth.Value * this.StripSectionDepth.Value), 10);

                        OldArea = (WireAreaOld1.HasValue ? WireAreaOld1.Value : 0) +
                        (WireAreaOld2.HasValue ? WireAreaOld2.Value : 0) +
                        (WireAreaOld3.HasValue ? WireAreaOld3.Value : 0) +
                        (WireAreaOld4.HasValue ? WireAreaOld4.Value : 0) +
                        (WireAreaOld5.HasValue ? WireAreaOld5.Value : 0) +
                        (WireAreaOld6.HasValue ? WireAreaOld6.Value : 0) +
                        (WireAreaOld7.HasValue ? WireAreaOld7.Value : 0) +
                        (WireAreaOld8.HasValue ? WireAreaOld8.Value : 0) +
                        (WireAreaOld9.HasValue ? WireAreaOld9.Value : 0) +
                        (WireAreaOld10.HasValue ? WireAreaOld10.Value : 0);
                    }
                    else
                    {
                        ResultMessages.Add("Enter Width and Depth and click calc!");
                    }

                    break;
            }
            
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cInstance"></param>
        private void SetCurrentInstance(WireCalculatorInstance cInstance)
        {
            if (cInstance != null)
            {
                this.NoOfWiresOld1 = cInstance.NoOfWiresOld1;
                this.NoOfWiresOld2 = cInstance.NoOfWiresOld2;
                this.NoOfWiresOld3 = cInstance.NoOfWiresOld3;
                this.NoOfWiresOld4 = cInstance.NoOfWiresOld4;
                this.NoOfWiresOld5 = cInstance.NoOfWiresOld5;
                this.NoOfWiresOld6 = cInstance.NoOfWiresOld6;
                this.NoOfWiresOld7 = cInstance.NoOfWiresOld7;
                this.NoOfWiresOld8 = cInstance.NoOfWiresOld8;
                this.NoOfWiresOld9 = cInstance.NoOfWiresOld9;
                this.NoOfWiresOld10 = cInstance.NoOfWiresOld10;

                this.WireDiameterOld1 = cInstance.WireDiameterOld1;
                this.WireDiameterOld2 = cInstance.WireDiameterOld2;
                this.WireDiameterOld3 = cInstance.WireDiameterOld3;
                this.WireDiameterOld4 = cInstance.WireDiameterOld4;
                this.WireDiameterOld5 = cInstance.WireDiameterOld5;
                this.WireDiameterOld6 = cInstance.WireDiameterOld6;
                this.WireDiameterOld7 = cInstance.WireDiameterOld7;
                this.WireDiameterOld8 = cInstance.WireDiameterOld8;
                this.WireDiameterOld9 = cInstance.WireDiameterOld9;
                this.WireDiameterOld10 = cInstance.WireDiameterOld10;

                this.WireAreaOld1 = cInstance.WireAreaOld1;
                this.WireAreaOld2 = cInstance.WireAreaOld2;
                this.WireAreaOld3 = cInstance.WireAreaOld3;
                this.WireAreaOld4 = cInstance.WireAreaOld4;
                this.WireAreaOld5 = cInstance.WireAreaOld5;
                this.WireAreaOld6 = cInstance.WireAreaOld6;
                this.WireAreaOld7 = cInstance.WireAreaOld7;
                this.WireAreaOld8 = cInstance.WireAreaOld8;
                this.WireAreaOld9 = cInstance.WireAreaOld9;
                this.WireAreaOld10 = cInstance.WireAreaOld10;

                this.NoOfWiresNew1 = cInstance.NoOfWiresNew1;
                this.NoOfWiresNew2 = cInstance.NoOfWiresNew2;
                this.NoOfWiresNew3 = cInstance.NoOfWiresNew3;
                this.NoOfWiresNew4 = cInstance.NoOfWiresNew4;
                this.NoOfWiresNew5 = cInstance.NoOfWiresNew5;
                this.NoOfWiresNew6 = cInstance.NoOfWiresNew6;
                this.NoOfWiresNew7 = cInstance.NoOfWiresNew7;
                this.NoOfWiresNew8 = cInstance.NoOfWiresNew8;
                this.NoOfWiresNew9 = cInstance.NoOfWiresNew9;
                this.NoOfWiresNew10 = cInstance.NoOfWiresNew10;

                this.WireDiameterNew1 = cInstance.WireDiameterNew1;
                this.WireDiameterNew2 = cInstance.WireDiameterNew2;
                this.WireDiameterNew3 = cInstance.WireDiameterNew3;
                this.WireDiameterNew4 = cInstance.WireDiameterNew4;
                this.WireDiameterNew5 = cInstance.WireDiameterNew5;
                this.WireDiameterNew6 = cInstance.WireDiameterNew6;
                this.WireDiameterNew7 = cInstance.WireDiameterNew7;
                this.WireDiameterNew8 = cInstance.WireDiameterNew8;
                this.WireDiameterNew9 = cInstance.WireDiameterNew9;
                this.WireDiameterNew10 = cInstance.WireDiameterNew10;

                this.WireAreaNew1 = cInstance.WireAreaNew1;
                this.WireAreaNew2 = cInstance.WireAreaNew2;
                this.WireAreaNew3 = cInstance.WireAreaNew3;
                this.WireAreaNew4 = cInstance.WireAreaNew4;
                this.WireAreaNew5 = cInstance.WireAreaNew5;
                this.WireAreaNew6 = cInstance.WireAreaNew6;
                this.WireAreaNew7 = cInstance.WireAreaNew7;
                this.WireAreaNew8 = cInstance.WireAreaNew8;
                this.WireAreaNew9 = cInstance.WireAreaNew9;
                this.WireAreaNew10 = cInstance.WireAreaNew10;

                this.EstimatorSectionNoOfWires = cInstance.EstimatorSectionNoOfWires;
                this.StripSectionWidth = cInstance.StripSectionWidth;
                this.StripSectionDepth = cInstance.StripSectionDepth;
            }
        }

        #endregion
    }
}
