﻿// -----------------------------------------------------------------------
// <copyright file="DBHelper.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace WindingDesign.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Security.Cryptography;
    using System.Xml;
    using System.Web;

    /// <summary>
    /// Helper class to facilitate db functions and needs arround it
    /// </summary>
    public static class DBHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private static WindingDesignEntities currentEntityContext;

        /// <summary>
        /// 
        /// </summary>
        public enum LoginResponse
        {
            EMAILDOESNOTEXISTS,
            INCORRECTPASSWORD,
            LOGINSUCESS
        }

        /// <summary>
        /// 
        /// </summary>
        public enum WCCalculationZone
        {
            OldData = 1,
            NewData = 2,
            Estimator = 3,
            StripSquare = 4,
            CalculateAll = 5
        }

        /// <summary>
        /// 
        /// </summary>
        public static WindingDesignEntities CurrentEntityContext
        {
            get
            {
                if (currentEntityContext == null)
                {
                    currentEntityContext = WindingDesignObjectFactory.GetObjectContext<WindingDesignEntities>();
                }

                return currentEntityContext;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <returns></returns>
        public static Client ReturnClientByEmailAddress(string EmailAddress)
        {
            Client ReturnUser = (from usrs in CurrentEntityContext.Clients where usrs.EmailAddress == EmailAddress select usrs).FirstOrDefault();
            return ReturnUser;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <returns></returns>
        public static Client ReturnClientByClientID(int ClientID)
        {
            Client ReturnUser = (from usrs in CurrentEntityContext.Clients where usrs.ClientID == ClientID select usrs).FirstOrDefault();
            return ReturnUser;
        }

        /// <summary>
        /// Encrypts the password
        /// </summary>
        /// <param name="PasswordPlainText"></param>
        /// <param name="SaltPassword"></param>
        /// <returns></returns>
        public static string EncryptPassword(string PasswordPlainText, string SaltPassword)
        {
            string ReturnEncryptedPassword = string.Empty;
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(PasswordPlainText);
            byte[] Salt = Encoding.ASCII.GetBytes(SaltPassword.Length.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(SaltPassword, Salt);
            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(PlainText, 0, PlainText.Length);
            cryptoStream.FlushFinalBlock();
            byte[] CipherBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            ReturnEncryptedPassword = Convert.ToBase64String(CipherBytes);
            return ReturnEncryptedPassword;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PasswordEncryptedText"></param>
        /// <param name="SaltPassword"></param>
        /// <returns></returns>
        public static string DecryptPassword(string PasswordEncryptedText, string SaltPassword)
        {
            string ReturnDecryptedPassword = string.Empty;
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            byte[] EncryptedData = Convert.FromBase64String(PasswordEncryptedText);
            byte[] Salt = Encoding.ASCII.GetBytes(SaltPassword.Length.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(SaltPassword, Salt);
            ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream(EncryptedData);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);
            byte[] PlainText = new byte[EncryptedData.Length];
            int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);
            memoryStream.Close();
            cryptoStream.Close();
            ReturnDecryptedPassword = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);
            return ReturnDecryptedPassword;
        }
    }
}
