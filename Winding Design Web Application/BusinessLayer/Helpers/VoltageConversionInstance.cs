﻿// -----------------------------------------------------------------------
// <copyright file="VoltageConversionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace WindingDesign.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class VoltageConversionInstance
    {
        #region Calculation Region

        /// <summary>
        /// 
        /// </summary>
        public enum ConversionType
        {
            [StringValue("Star to Star")]
            StarToStar = 1,

            [StringValue("Star to Delta")]
            StarToDelta = 2,

            [StringValue("Delta to Star")]
            DeltaToStar = 3,

            [StringValue("Delta to Delta")]
            DeltaToDelta = 4
        }

        /// <summary>
        /// 
        /// </summary>
        public ConversionType CurrentConversionType { get; set; }        

        /// <summary>
        /// 
        /// </summary>
        public List<string> ResultMessages { get; set; }

        /// <summary>
        /// Gets or sets the job identification number.
        /// </summary>
        /// <value>
        /// The job identification number.
        /// </value>
        public string JobIdentificationNumber { get; set; }

        #endregion

        #region Original Data

        public double? SupplyVoltageOriginal { get; set; }
        public double? FullLoadCurrentOriginal { get; set; }
        public double? PhaseVoltageOriginal { get; set; }
        public double? PhaseCurrentOriginal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ConnectionOriginal
        {
            get
            {
                switch (CurrentConversionType)
                {
                    case ConversionType.StarToStar:
                        return "Star";
                    case ConversionType.StarToDelta:
                        return "Star";
                    case ConversionType.DeltaToStar:
                        return "Delta";
                    case ConversionType.DeltaToDelta:
                        return "Delta";
                }

                return "Star";
            }
        }

        public double? SupplyFrequencyOriginal { get; set; }
        public double? OutputPowerOriginal { get; set; }
        public double? TurnsOriginal { get; set; }
        public double? WireDiameterOriginal { get; set; }
        public double? NoOfPathsOriginal { get; set; }
        public double? FLSpeedOriginal { get; set; }
        public double? CoolingOriginal { get; set; }

        #endregion

        #region New Data

        public double? SupplyVoltageNew { get; set; }
        public double? FullLoadCurrentNew { get; set; }
        public double? PhaseVoltageNew { get; set; }
        public double? PhaseCurrentNew { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ConnectionNew
        {
            get
            {
                switch (CurrentConversionType)
                {
                    case ConversionType.StarToStar:
                        return "Star";
                    case ConversionType.StarToDelta:
                        return "Delta";
                    case ConversionType.DeltaToStar:
                        return "Star";
                    case ConversionType.DeltaToDelta:
                        return "Delta";
                }

                return "Star";
            }
        }

        public double? SupplyFrequencyNew { get; set; }
        public double? OutputPowerNew { get; set; }
        public double? TurnsNew { get; set; }
        public double? WireDiameterNew { get; set; }
        public double? NoOfPathsNew { get; set; }
        public double? FLSpeedNew { get; set; }
        public double? CoolingNew { get; set; }

        #endregion

        #region Business Logic

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cType"></param>
        /// <param name="cInstance"></param>
        /// <returns></returns>
        public VoltageConversionInstance Calculate(VoltageConversionInstance cInstance)
        {
            this.SetCurrentInstance(cInstance);
            ResultMessages = new List<string>();

            if (this.SupplyVoltageOriginal.HasValue && this.FullLoadCurrentOriginal.HasValue && this.SupplyFrequencyOriginal.HasValue && this.OutputPowerOriginal.HasValue && this.TurnsOriginal.HasValue && this.WireDiameterOriginal.HasValue &&
                this.NoOfPathsOriginal.HasValue && this.FLSpeedOriginal.HasValue && this.SupplyVoltageNew.HasValue && this.SupplyFrequencyNew.HasValue && this.NoOfPathsNew.HasValue)
            {
                this.CoolingOriginal = 1;

                if (this.FullLoadCurrentOriginal.HasValue && this.SupplyFrequencyNew.HasValue && this.SupplyFrequencyOriginal.HasValue && this.SupplyVoltageOriginal.HasValue && this.SupplyVoltageNew.HasValue)
                {
                    this.FullLoadCurrentNew = Math.Round((this.FullLoadCurrentOriginal.Value * (this.SupplyFrequencyNew.Value / this.SupplyFrequencyOriginal.Value) * (this.SupplyVoltageOriginal.Value / this.SupplyVoltageNew.Value)), 2);
                }

                switch (CurrentConversionType)
                {
                    case ConversionType.StarToStar:
                        if (this.SupplyVoltageOriginal.HasValue)
                        {
                            this.PhaseVoltageOriginal = Math.Round((this.SupplyVoltageOriginal.Value / Math.Pow(3, 0.5)), 0);
                        }

                        if (this.FullLoadCurrentOriginal.HasValue)
                        {
                            this.PhaseCurrentOriginal = Math.Round(this.FullLoadCurrentOriginal.Value, 1);
                        }

                        if (this.SupplyVoltageNew.HasValue)
                        {
                            this.PhaseVoltageNew = Math.Round((this.SupplyVoltageNew.Value / Math.Pow(3, 0.5)), 0);
                        }

                        if (this.FullLoadCurrentNew.HasValue)
                        {
                            this.PhaseCurrentNew = Math.Round(this.FullLoadCurrentNew.Value, 1);
                        }

                        break;
                    case ConversionType.StarToDelta:
                        if (this.SupplyVoltageOriginal.HasValue)
                        {
                            this.PhaseVoltageOriginal = Math.Round((this.SupplyVoltageOriginal.Value / Math.Pow(3, 0.5)), 0);
                        }

                        if (this.FullLoadCurrentOriginal.HasValue)
                        {
                            this.PhaseCurrentOriginal = Math.Round(this.FullLoadCurrentOriginal.Value, 1);
                        }

                        if (this.SupplyVoltageNew.HasValue)
                        {
                            this.PhaseVoltageNew = Math.Round(this.SupplyVoltageNew.Value, 0);
                        }

                        if (this.FullLoadCurrentNew.HasValue)
                        {
                            this.PhaseCurrentNew = Math.Round((this.FullLoadCurrentNew.Value / Math.Pow(3, 0.5)), 1);
                        }

                        break;
                    case ConversionType.DeltaToStar:
                        if (this.SupplyVoltageOriginal.HasValue)
                        {
                            this.PhaseVoltageOriginal = Math.Round(this.SupplyVoltageOriginal.Value, 0);
                        }

                        if (this.FullLoadCurrentOriginal.HasValue)
                        {
                            this.PhaseCurrentOriginal = Math.Round((this.FullLoadCurrentOriginal.Value / Math.Pow(3, 0.5)), 1);
                        }

                        if (this.SupplyVoltageNew.HasValue)
                        {
                            this.PhaseVoltageNew = Math.Round((this.SupplyVoltageNew.Value / Math.Pow(3, 0.5)), 0);
                        }

                        if (this.FullLoadCurrentNew.HasValue)
                        {
                            this.PhaseCurrentNew = Math.Round(this.FullLoadCurrentNew.Value, 1);
                        }
                        break;
                    case ConversionType.DeltaToDelta:
                        if (this.SupplyVoltageOriginal.HasValue)
                        {
                            this.PhaseVoltageOriginal = Math.Round(this.SupplyVoltageOriginal.Value, 0);
                        }

                        if (this.FullLoadCurrentOriginal.HasValue)
                        {
                            this.PhaseCurrentOriginal = Math.Round((this.FullLoadCurrentOriginal.Value / Math.Pow(3, 0.5)), 1);
                        }

                        if (this.SupplyVoltageNew.HasValue)
                        {
                            this.PhaseVoltageNew = Math.Round(this.SupplyVoltageNew.Value, 0);
                        }

                        if (this.FullLoadCurrentNew.HasValue)
                        {
                            this.PhaseCurrentNew = Math.Round((this.FullLoadCurrentNew.Value / Math.Pow(3, 0.5)), 1);
                        }

                        break;
                }

                if (this.OutputPowerOriginal.HasValue && this.SupplyFrequencyNew.HasValue && this.SupplyFrequencyOriginal.HasValue)
                {
                    this.OutputPowerNew = (this.OutputPowerOriginal.Value * this.SupplyFrequencyNew.Value) / this.SupplyFrequencyOriginal.Value;
                }

                if (this.FLSpeedOriginal.HasValue && this.SupplyFrequencyNew.HasValue && this.SupplyFrequencyOriginal.HasValue)
                {
                    this.FLSpeedNew = Math.Round(((this.FLSpeedOriginal.Value * this.SupplyFrequencyNew.Value) / this.SupplyFrequencyOriginal.Value), 0);
                }

                if (this.SupplyFrequencyNew.HasValue && this.SupplyFrequencyOriginal.HasValue)
                {
                    this.CoolingNew = Math.Round(((this.SupplyFrequencyNew.Value * 100) / this.SupplyFrequencyOriginal.Value), 0) / 100;
                }

                if (this.TurnsOriginal.HasValue && this.SupplyVoltageNew.HasValue && this.SupplyVoltageOriginal.HasValue && this.NoOfPathsNew.HasValue && this.NoOfPathsOriginal.HasValue && this.SupplyFrequencyOriginal.HasValue && this.SupplyFrequencyNew.HasValue)
                {
                    this.TurnsNew = Math.Round((this.TurnsOriginal.Value * (this.SupplyVoltageNew.Value / this.SupplyVoltageOriginal.Value) * (this.NoOfPathsNew.Value / this.NoOfPathsOriginal.Value) * Math.Pow((this.SupplyFrequencyOriginal.Value / this.SupplyFrequencyNew.Value), 0.5)), 2);
                }

                switch (CurrentConversionType)
                {
                    case ConversionType.StarToDelta:
                        if (this.TurnsNew.HasValue)
                        {
                            this.TurnsNew = Math.Round((this.TurnsNew.Value * Math.Pow(3, 0.5)), 2);
                        }
                        break;
                    case ConversionType.DeltaToStar:
                        if (this.TurnsNew.HasValue)
                        {
                            this.TurnsNew = Math.Round((this.TurnsNew.Value / Math.Pow(3, 0.5)), 2);
                        }
                        break;
                }

                double Area1 = 0.0;
                double Area2 = 0.0;

                if (this.WireDiameterOriginal.HasValue && this.SupplyFrequencyNew.HasValue && this.SupplyFrequencyOriginal.HasValue && this.SupplyVoltageOriginal.HasValue && this.SupplyVoltageNew.HasValue && this.NoOfPathsNew.HasValue && this.NoOfPathsOriginal.HasValue)
                {
                    Area1 = Math.Pow((this.WireDiameterOriginal.Value / 2), 2) * Math.PI;
                    Area2 = Area1 * (this.SupplyFrequencyNew.Value / this.SupplyFrequencyOriginal.Value) * (this.SupplyVoltageOriginal.Value / this.SupplyVoltageNew.Value) * (this.NoOfPathsOriginal.Value / this.NoOfPathsNew.Value);

                    switch (CurrentConversionType)
                    {
                        case ConversionType.StarToDelta:
                            Area2 = Area2 / Math.Pow(3, 0.5);
                            break;
                        case ConversionType.DeltaToStar:
                            Area2 = Area2 * Math.Pow(3, 0.5);
                            break;
                    }

                    this.WireDiameterNew = Math.Round((Math.Pow((Area2 / Math.PI), 0.5) * 2), 3);
                }

                ResultMessages.Add("Your new data is " + this.TurnsNew.Value.ToString() + " turns of " + this.WireDiameterNew.Value.ToString() + " mm. Dia (see in wire calc).");

                if (CoolingNew.HasValue)
                {
                    if (CoolingNew.Value > 1)
                    {
                        ResultMessages.Add("Your air flow can increase by up to " + ((CoolingNew.Value * 100) - 100).ToString() + "%, depending on the load rating of the cooling fan.");
                    }
                    else if (CoolingNew.Value < 1)
                    {
                        ResultMessages.Add("Your air flow will decrease by " + (((CoolingNew.Value * 100) - 100) * (-1)).ToString() + "%.");
                    }
                    else if (CoolingNew.Value == 1)
                    {
                        ResultMessages.Add("Your air flow is the same.");
                    }

                    if (CoolingNew.Value < CoolingOriginal)
                    {
                        ResultMessages.Add("If cooling is reduced, wire diameter may need to increase. Monitor temperature rise on test.");
                    }
                }
            }
            else
            {
                ResultMessages.Add("Enter the data in the boxes (Left and Right) and click calc!");
            }

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cInstance"></param>
        private void SetCurrentInstance(VoltageConversionInstance cInstance)
        {
            if (cInstance != null)
            {
                this.SupplyVoltageOriginal = cInstance.SupplyVoltageOriginal;
                this.FullLoadCurrentOriginal = cInstance.FullLoadCurrentOriginal;
                this.PhaseVoltageOriginal = cInstance.PhaseVoltageOriginal;
                this.PhaseCurrentOriginal = cInstance.PhaseCurrentOriginal;
                this.SupplyFrequencyOriginal = cInstance.SupplyFrequencyOriginal;
                this.OutputPowerOriginal = cInstance.OutputPowerOriginal;
                this.TurnsOriginal = cInstance.TurnsOriginal;
                this.WireDiameterOriginal = cInstance.WireDiameterOriginal;
                this.NoOfPathsOriginal = cInstance.NoOfPathsOriginal;
                this.FLSpeedOriginal = cInstance.FLSpeedOriginal;
                this.CoolingOriginal = cInstance.CoolingOriginal;

                this.SupplyVoltageNew = cInstance.SupplyVoltageNew;
                this.FullLoadCurrentNew = cInstance.FullLoadCurrentNew;
                this.PhaseVoltageNew = cInstance.PhaseVoltageNew;
                this.PhaseCurrentNew = cInstance.PhaseCurrentNew;
                this.SupplyFrequencyNew = cInstance.SupplyFrequencyNew;
                this.OutputPowerNew = cInstance.OutputPowerNew;
                this.TurnsNew = cInstance.TurnsNew;
                this.WireDiameterNew = cInstance.WireDiameterNew;
                this.NoOfPathsNew = cInstance.NoOfPathsNew;
                this.FLSpeedNew = cInstance.FLSpeedNew;
                this.CoolingNew = cInstance.CoolingNew;

                this.CurrentConversionType = cInstance.CurrentConversionType;
            }
        }

        #endregion
    }
}
