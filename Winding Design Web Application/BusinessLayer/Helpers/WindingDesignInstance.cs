﻿// -----------------------------------------------------------------------
// <copyright file="WindingDesignInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace WindingDesign.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class WindingDesignInstance
    {
        public WireCalculatorInstance WireCalculator { get; set; }
        public VoltageConversionInstance VoltageConversion { get; set; }
        public PowerConversionInstance PowerConversion { get; set; }
        public CoilConversionInstance CoilConversion { get; set; }
        public SpeedConversionInstance SpeedConversion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="WireDia"></param>
        public void SetWireDiameterToWireCalculator(double? WireDia)
        {
            if (this.WireCalculator != null)
            {
                this.WireCalculator.NoOfWiresOld1 = 1;
                this.WireCalculator.WireDiameterOld1 = WireDia;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Poles"></param>
        /// <param name="Slots"></param>
        public void SetOriginalSlotsAndPolesToCoilConversion(double? Poles, double? Slots)
        {
            if (this.CoilConversion != null)
            {
                this.CoilConversion.NoOfPoles = Poles;
                this.CoilConversion.NoOfSlots = Slots;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ResetCalculatorInstances()
        {
            this.WireCalculator = new WireCalculatorInstance();
            this.VoltageConversion = new VoltageConversionInstance();
            this.PowerConversion = new PowerConversionInstance();
            this.CoilConversion = new CoilConversionInstance();
            this.SpeedConversion = new SpeedConversionInstance();
        }
    }
}
