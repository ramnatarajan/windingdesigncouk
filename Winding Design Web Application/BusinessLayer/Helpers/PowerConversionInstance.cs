﻿// -----------------------------------------------------------------------
// <copyright file="PowerConversionInstance.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace WindingDesign.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PowerConversionInstance
    {
        #region Calculation Region

        /// <summary>
        /// 
        /// </summary>
        public List<string> ResultMessages { get; set; }

        /// <summary>
        /// Gets or sets the job identification number.
        /// </summary>
        /// <value>
        /// The job identification number.
        /// </value>
        public string JobIdentificationNumber { get; set; }

        #endregion

        #region Original Data

        public double? OutputPowerOriginal { get; set; }
        public double? NoOfPolesOriginal { get; set; }
        public double? CoreLengthOriginal { get; set; }
        public double? CoreDiameterOriginal { get; set; }
        public double? NoOfCurrentPathsOriginal { get; set; }
        public double? TurnsOriginal { get; set; }
        public double? WireDiameterOriginal { get; set; }
        public double? WireAreaOriginal { get; set; }
        public double? MaxCoreRatingOriginal { get; set; }

        #endregion

        #region New Data

        public double? OutputPowerNew { get; set; }
        public double? NoOfCurrentPathsNew { get; set; }
        public double? TurnsNew { get; set; }
        public double? WireAreaNew { get; set; }
        public double? WireDiameterNew { get; set; }

        #endregion

        #region Business Logic

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cType"></param>
        /// <param name="cInstance"></param>
        /// <returns></returns>
        public PowerConversionInstance Calculate(PowerConversionInstance cInstance)
        {
            this.SetCurrentInstance(cInstance);
            ResultMessages = new List<string>();

            if (this.OutputPowerOriginal.HasValue && this.NoOfPolesOriginal.HasValue && this.CoreLengthOriginal.HasValue && this.CoreDiameterOriginal.HasValue && this.NoOfCurrentPathsOriginal.HasValue && this.TurnsOriginal.HasValue && this.WireDiameterOriginal.HasValue
                && this.OutputPowerNew.HasValue && this.NoOfCurrentPathsNew.HasValue)
            {
                double Volume = (this.CoreDiameterOriginal.Value / 25.4) * (this.CoreDiameterOriginal.Value / 25.4) * (this.CoreLengthOriginal.Value / 25.4);
                double DLValue = 0;

                switch (Convert.ToInt32(this.NoOfPolesOriginal.Value))
                {
                    case 2:
                        DLValue = 12.9;
                        break;
                    case 4:
                        DLValue = 15.6;
                        break;
                    case 6:
                        DLValue = 27.5;
                        break;
                    case 8:
                        DLValue = 38.4;
                        break;
                }

                this.MaxCoreRatingOriginal = Math.Round((Volume / DLValue), 4);
                this.TurnsNew = Math.Round((this.TurnsOriginal.Value * Math.Pow((this.OutputPowerOriginal.Value / this.OutputPowerNew.Value), 0.5)), 4);
                this.WireAreaOriginal = Math.Round((Math.Pow((this.WireDiameterOriginal.Value / 2), 2) * Math.PI), 4);
                this.WireAreaNew = Math.Round((this.WireAreaOriginal.Value * (this.OutputPowerNew.Value / this.OutputPowerOriginal.Value)), 4);
                this.WireDiameterNew = Math.Round((Math.Pow((this.WireAreaNew.Value / Math.PI), 0.5) * 2), 4);

                ResultMessages.Add("Max. Core Rating (kW's): " + this.MaxCoreRatingOriginal.Value.ToString());
                ResultMessages.Add("New Turns: " + this.TurnsNew.Value.ToString());
                ResultMessages.Add("Original Wire Area: " + this.WireAreaOriginal.Value.ToString());
                ResultMessages.Add("New Wire Area: " + this.WireAreaNew.Value.ToString());
                ResultMessages.Add("New Wire Diameter: " + this.WireDiameterNew.Value.ToString());
            }
            else
            {
                ResultMessages.Add("Enter the data in the boxes (Left and Right) and click calc!");
            }

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cInstance"></param>
        private void SetCurrentInstance(PowerConversionInstance cInstance)
        {
            if (cInstance != null)
            {
                this.OutputPowerOriginal = cInstance.OutputPowerOriginal;
                this.NoOfPolesOriginal = cInstance.NoOfPolesOriginal;
                this.CoreLengthOriginal = cInstance.CoreLengthOriginal;
                this.CoreDiameterOriginal = cInstance.CoreDiameterOriginal;
                this.NoOfCurrentPathsOriginal = cInstance.NoOfCurrentPathsOriginal;
                this.TurnsOriginal = cInstance.TurnsOriginal;
                this.WireDiameterOriginal = cInstance.WireDiameterOriginal;
                this.MaxCoreRatingOriginal = cInstance.MaxCoreRatingOriginal;
                this.WireAreaOriginal = cInstance.WireAreaOriginal;

                this.OutputPowerNew = cInstance.OutputPowerNew;
                this.NoOfCurrentPathsNew = cInstance.NoOfCurrentPathsNew;
                this.TurnsNew = cInstance.TurnsNew;
                this.WireDiameterNew = cInstance.WireDiameterNew;
                this.WireAreaNew = cInstance.WireAreaNew;
            }
        }

        #endregion
    }
}
