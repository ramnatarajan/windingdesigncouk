﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data;
using System.Data.Linq;
using System.Threading;
using System.Diagnostics;
using System.Reflection;

namespace WindingDesign.BusinessLayer
{
    public static class WindingDesignObjectFactory
    {
        /// <summary>
        /// Internal method that handles creating a context that is scoped to the HttpContext Items collection
        /// by creating and holding the ObjectContext there.
        /// </summary>
        /// <typeparam name="TObjectContext"></typeparam>
        /// <param name="createContext"></param>
        /// <returns></returns>
        private static TObjectContext GetWebRequestScopedObjectContext<TObjectContext>(Boolean createContext)
            where TObjectContext : ObjectContext, new()
        {
            return GetWebRequestScopedObjectContext<TObjectContext>(createContext, "");
        }

        /// <summary>
        /// Internal method that handles creating a context that is scoped to the HttpContext Items collection
        /// by creating and holding the ObjectContext there.
        /// </summary>
        /// <returns></returns>
        private static TObjectContext GetWebRequestScopedObjectContext<TObjectContext>
            (Boolean createContext, string connectionString) where TObjectContext : ObjectContext, new()
        {
            TObjectContext context;
            string key = string.Empty;

            if (key == string.Empty)
                key = "__EFDC_" + typeof(TObjectContext).Name +
                     HttpContext.Current.GetHashCode().ToString("x") + Thread.CurrentContext.ContextID.ToString();

            context = (TObjectContext)HttpContext.Current.Items[key];
            if (context == null && createContext)
            {
                if (connectionString == string.Empty)
                {
                    context = new TObjectContext();
                }
                else
                {
                    // Use Reflection to set the connection string
                    context = (TObjectContext)typeof(TObjectContext).GetConstructor(
                         new System.Type[] { typeof(string) }).Invoke(new object[] { connectionString });
                }

                if (context != null)
                    HttpContext.Current.Items[key] = context;
            }
            return context;
        }

        /// <summary>
        /// Creates a Thread Scoped ObjectContext that can be reused.
        /// </summary>
        /// <typeparam name="TObjectContext"></typeparam>
        /// <returns></returns>
        private static TObjectContext GetThreadScopedObjectContext<TObjectContext>
            (Boolean createContext) where TObjectContext : ObjectContext, new()
        {
            return GetThreadScopedObjectContext<TObjectContext>(createContext, "");
        }

        /// <summary>
        /// Creates a Thread Scoped ObjectContext that can be reused.
        /// </summary>
        /// <typeparam name="TObjectContext"></typeparam>
        /// <param name="createContext"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        private static TObjectContext GetThreadScopedObjectContext<TObjectContext>
            (Boolean createContext, string connectionString) where TObjectContext : ObjectContext, new()
        {
            string key = string.Empty;
            if (key == string.Empty)
            {
                key = "__EFDC_" + typeof(TObjectContext).Name +
                    Thread.CurrentContext.ContextID.ToString();
            }
            LocalDataStoreSlot threadData = Thread.GetNamedDataSlot(key);
            TObjectContext context = null;
            if (threadData != null)
            {
                context = (TObjectContext)Thread.GetData(threadData);
            }
            if (context == null && createContext)
            {
                if (connectionString == string.Empty)
                {
                    context = new TObjectContext();
                }
                else
                {
                    EntityConnection conn = new EntityConnection(connectionString);
                    // Use Reflection to set the connection string
                    context = (TObjectContext)typeof(TObjectContext).GetConstructor(
                         new System.Type[] { typeof(EntityConnection) }).Invoke(new object[] { conn });
                }
                if (context != null)
                {
                    if (threadData == null)
                        threadData = Thread.AllocateNamedDataSlot(key);
                    Thread.SetData(threadData, context);
                }
            }
            return context;
        }

        /// <summary>
        /// Returns either Web Request scoped ObjectContext or a Thread scoped
        /// request object if not running a Web request (ie. HttpContext.Current)
        /// </summary>
        /// <typeparam name="TObjectContext"></typeparam>
        /// <returns></returns>
        public static TObjectContext GetObjectContext<TObjectContext>() where TObjectContext : ObjectContext, new()
        {
            if (HttpContext.Current != null)
                return GetWebRequestScopedObjectContext<TObjectContext>(true);
            return GetThreadScopedObjectContext<TObjectContext>(true);
        }

        /// <summary>
        /// Returns either Web Request scoped ObjectContext or a Thread scoped
        /// request object if not running a Web request (ie. HttpContext.Current)
        /// </summary>
        /// <typeparam name="TObjectContext"></typeparam>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static TObjectContext GetObjectContext<TObjectContext>(string connectionString) where TObjectContext : ObjectContext, new()
        {
            if (HttpContext.Current != null)
                return GetWebRequestScopedObjectContext<TObjectContext>(true, connectionString);
            return GetThreadScopedObjectContext<TObjectContext>(true, connectionString);
        }


        /// <summary>
        /// Disposes the Context
        /// </summary>
        /// <typeparam name="TObjectContext"></typeparam>
        public static void DisposeContext<TObjectContext>() where TObjectContext : ObjectContext, IDisposable, new()
        {
            DisposeContext<TObjectContext>("");
        }


        /// <summary>
        /// Disposes the Context
        /// </summary>
        public static void DisposeContext<TObjectContext>(string connectionString) where TObjectContext : ObjectContext, IDisposable, new()
        {
            TObjectContext context;
            if (HttpContext.Current != null)
            {
                context = GetWebRequestScopedObjectContext<TObjectContext>(false, connectionString);
            }
            else
            {
                context = GetThreadScopedObjectContext<TObjectContext>(false, connectionString);
            }
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
        }
    }
}
