﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paypal.Master" AutoEventWireup="true"
    CodeBehind="BuyLicense.aspx.cs" Inherits="WindingDesign.WebApp.BuyLicense" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <table class="AboutUsLayout">
                <tr>
                    <td style="vertical-align: text-top; text-align: right;">
                        <div class="AboutUsLeft">
                            <img alt="Tony Ball" src="Images/TonyBallLogo.png" height="200px" width="150px" />
                            <br />
                            <p class="AboutAuthorNameStyle">
                                Tony Ball
                            </p>
                        </div>
                    </td>
                    <td class="AboutUsRight">
                        <div class="AboutUsRight">
                            <h2>
                                Buy License for Winding Design</h2>
                            <h3 style="color: Red">
                                This page is under construction. Please do not click buy now.
                            </h3>
                            <p>
                                Please Note: The telephone payment phone number is not a technical helpline and
                                is not manned by technical staff. If you have any queries please email the author.</p>
                            <h2>
                                How To Buy
                            </h2>
                            <span class="Mainbodycopy">Item: 1 Year License for Winding Design Portal<br />
                                Price in Great Britain Pounds: £80</span>
                            <br />
                            <p class="MaintextBlack">
                                <span class="Mainbodycopy">By Cheque: Send your full delivery address with a written
                                    order and include a cheque for the correct amount. Please also include your email
                                    address details that you used to register with Winding Design Portal.</span></p>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="36%" align="left" valign="top">
                                        <p align="left">
                                            <span class="MaintextBlack"><span class="Mainbodycopy"><span style="font-weight: bold">
                                                Please send to: </span>
                                                <br />
                                                <br />
                                                Engineering Publications Ltd.<br />
                                                Unit 4, Culverin Square<br />
                                                Limberline Rd, Hilsea,<br />
                                                Portsmouth, PO3 5BU</span><br />
                                                <br />
                                            </span>
                                        </p>
                                    </td>
                                    <td width="64%" align="left" valign="top">
                                        <p align="left">
                                            <span class="MaintextBlack">By phone:
                                                <br />
                                                <br />
                                            </span><span class="Mainbodycopy">Between the hours of 07.00am until 07.00pm, Monday
                                                to Sunday:<br />
                                                Telephone - 07956 518797 (UK)<br />
                                                Telephone - (+44) 7956 518797 (non UK)</span></p>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="95%" height="60" align="center" valign="middle">
                                        <span class="MaintextBlack">PURCHASE ONLINE WITH PAYPAL: </span><span class="Mainbodycopy">
                                            <br />
                                            <br />
                                            Please select a Buy Now button below and follow the steps when prompted.<br />
                                        </span>
                                        <p>
                                            We will extend your license expiry for one year once we get the confirmation from
                                            Pay Pal after your successful purchase. If it is urgent please call us in the below
                                            number or email us after payment so that we can prioritize your order.
                                        </p>
                                        <p>
                                            Note: You have to have an account registered with Winding Design Portal before you
                                            can buy a license.<br />
                                            <br />
                                        </p>
                                    </td>
                                    <td width="5%" align="center" valign="middle">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center" valign="middle">
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                        <input type="hidden" name="cmd" value="_s-xclick" />
                                        <input type="hidden" name="hosted_button_id" value="7F8FGFNQTMMRG" />
                                        <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif"
                                            name="submit" alt="PayPal — The safer, easier way to pay online." />
                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif"
                                            width="1" height="1" />
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                            <table height="60" border="0" cellpadding="0" cellspacing="10">
                                <tr>
                                    <td height="50" align="center" valign="middle" class="IntrotextBlue">
                                        Call us on 02392 660336 or email: <a href="mailto:tony@engineeringpublications.co.uk">
                                            <font color="#003399">tony@engineeringpublications.co.uk</font></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
