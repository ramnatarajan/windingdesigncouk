﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayPal.ascx.cs" Inherits="WindingDesign.WebApp.Controls.PayPal" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" valign="middle">
            <span class="Mainbodycopy" style="font-weight: bold">UK</span><br />
            <br />
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_s-xclick" />
            <input type="hidden" name="hosted_button_id" value="7F8FGFNQTMMRG" />
            <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif"
                name="submit" alt="PayPal — The safer, easier way to pay online." />
            <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif"
                width="1" height="1" />
            </form>
        </td>
        <td align="center" valign="middle">
            <span class="Mainbodycopy" style="font-weight: bold">Europe</span><br />
            <br />
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_s-xclick" />
            <input type="hidden" name="hosted_button_id" value="3RAZYUDYBLQSL" />
            <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif"
                name="submit" alt="PayPal — The safer, easier way to pay online." />
            <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif"
                width="1" height="1" />
            </form>
        </td>
        <td align="center" valign="middle">
            <span class="Mainbodycopy" style="font-weight: bold">Outside Europe</span><br />
            <br />
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_s-xclick" />
            <input type="hidden" name="hosted_button_id" value="3RAZYUDYBLQSL" />
            <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif"
                name="submit" alt="PayPal — The safer, easier way to pay online." />
            <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif"
                width="1" height="1" />
            </form>
        </td>
    </tr>
</table>
