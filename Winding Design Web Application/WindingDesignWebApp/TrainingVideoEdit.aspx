﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TrainingVideoEdit.aspx.cs" Inherits="WindingDesign.WebApp.TrainingVideoEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        $('#HomeTabs').tabs();
    });
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <table class="WireCalcTableTop">
                <tr>
                    <td>
                        <div class="WireCalcJobDetails">
                            <table style="width: 100%; height: 100%">
                                <tr>
                                    <td class="WireCalcOldDataTableLabel" style="text-align: left; height: 28px; width: 100%;">
                                        &nbsp;&nbsp;<asp:Label ID="lblEditVideoTitle" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="WireDesignEditTable">
                <table class="WireDesignEditTable">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="35%" style="text-align: right; vertical-align: middle;">
                            <asp:Label ID="lblReq1" CssClass="RequiredField" runat="server" Text="*" />
                            Title
                        </td>
                        <td>
                            &nbsp;&nbsp;<asp:TextBox ID="txtTitle" Width="254px" ValidationGroup="EditVideo"
                                CssClass="SiteTextBoxes" runat="server" MaxLength="250" />
                        </td>
                        <td style="text-align: left;">
                            <asp:RequiredFieldValidator ID="vldTitle" runat="server" ValidationGroup="EditVideo"
                                CssClass="RequiredField" ErrorMessage="Title is required" ControlToValidate="txtTitle" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; vertical-align: middle;">
                            <asp:Label ID="Label1" CssClass="RequiredField" runat="server" Text="*" />
                            Description
                        </td>
                        <td width="30%">
                            &nbsp;&nbsp;<asp:TextBox ID="txtDescription" Width="250px" ValidationGroup="EditVideo"
                                TextMode="MultiLine" CssClass="MultilineTextBox" runat="server" MaxLength="500" />
                        </td>
                        <td style="text-align: left;">
                            <asp:RequiredFieldValidator ID="vldDescription" runat="server" ValidationGroup="EditVideo"
                                CssClass="RequiredField" ErrorMessage="Description is required" ControlToValidate="txtDescription" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; vertical-align: middle;">
                            <asp:Label ID="Label2" CssClass="RequiredField" runat="server" Text="*" />
                            Embed Video Path
                        </td>
                        <td width="30%">
                            &nbsp;&nbsp;<asp:TextBox ID="txtEmbedVideoPath" Width="250px" ValidationGroup="EditVideo"
                                CssClass="MultilineTextBox" runat="server" MaxLength="500" />
                        </td>
                        <td style="text-align: left;">
                            <asp:RequiredFieldValidator ID="vldReqEmbed" runat="server" ValidationGroup="EditVideo"
                                CssClass="RequiredField" ErrorMessage="Video URL is required" ControlToValidate="txtEmbedVideoPath" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <div style="margin-left: 10px; margin-top: 10px;">
                                <asp:Button ID="btnSaveVideo" class="btn btn-success fileinput-button" ValidationGroup="EditVideo"
                                    runat="server" Text="Save" onclick="btnSaveVideo_Click" />
                                <% if (CurrentTrainingVideoID != 0)
                                   { %>
                                <asp:Button ID="btnDelete" class="btn btn-success fileinput-button" runat="server"
                                    Text="Delete" onclick="btnDelete_Click" />
                                <%} %>
                                <asp:Button ID="btnCancel" class="btn btn-success fileinput-button"
                                    runat="server" Text="Cancel" onclick="btnCancel_Click" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
