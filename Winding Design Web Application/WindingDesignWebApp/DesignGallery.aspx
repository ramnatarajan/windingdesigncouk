﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="DesignGallery.aspx.cs" Inherits="WindingDesign.WebApp.DesignGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#HomeTabs').tabs();

            $('#WireDesigns tbody tr').each(function () {
                var nTds = $('td', this);
                var DialogID = $(nTds[5]).text();
                nTds[1].setAttribute('title', "OpenEditDesign");
                nTds[2].setAttribute('title', DialogID);
                nTds[6].setAttribute('title', "OpenEditDesign");
                $('#' + DialogID).dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {},
                    width: 842,
                    resizable: false
                });
            });

            $('#WireDesigns tbody tr td').click(function (e) {
                if (this.title != "OpenEditDesign") {
                    $('#' + this.title).dialog('open');
                    return false;
                }
            });

            var oTable = $('#WireDesigns').dataTable({
                "bJQueryUI": true,
                "aoColumns": [{ "bVisible": false }, null, { "sWidth": "7em", "bSortable": false }, { "bVisible": false }, { "bVisible": false }, { "bVisible": false }, { "sWidth": "2em" }],
                "aaSorting": [],
                "oLanguage": { "sSearch": "Search " },
                "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, 'All']],
                "iDisplayLength": 5
            });
        });
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upVoltConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="text-align: left; height: 28px; width: 100%;">
                                                &nbsp;&nbsp;<asp:Label runat="server" ID="lblTitle" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table id="WireDesigns" class="display" style="vertical-align: text-top;text-align:left;" cellspacing="0"
                        cellpadding="0" border="0">
                        <thead>
                            <tr>
                                <th>
                                </th>
                                <th>
                                    Description
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <%if (IsAdmin)
                                  { %>
                                <th>
                                </th>
                                <%}
                                  else
                                  { %>
                                  <th style="display:none;"></th>
                                    <%} %>
                            </tr>
                        </thead>
                        <tbody>
                            <% if (DesignSource != null && DesignSource.Count > 0)
                               {
                                   foreach (WindingDesign.WebApp.WireDesign imgEntry in DesignSource)
                                   {
                            %>
                            <tr>
                                <td style="vertical-align: text-top; margin-left: 10px;">
                                    <% =imgEntry.ShortDescription %>
                                </td>
                                <td style="vertical-align: text-top; margin-left: 10px;">
                                    <p style="font-size:12px;text-align:left;font-weight:bold;"><% =imgEntry.ShortDescription %></p>
                                    <p style="font-size:12px;text-align:justify;"><% =imgEntry.LongDescription %></p>
                                    <a href="PrinitDesign.aspx?DesignURL=<% =imgEntry.OriginalImagePath %>" target="_blank">Print Image</a>
                                </td>
                                <td style="text-align:center;">
                                    <a id='dialoglink<% =imgEntry.DesignID %>' href="#">
                                        <img alt="" src="<% =imgEntry.ThumbnailPath %>" width="100px;" height="100px;" />
                                        <span id="dialog<% =imgEntry.DesignID %>" title="<% =imgEntry.ShortDescription %>">
                                        <p style="font-size:12px;text-align:justify;">&nbsp;&nbsp;&nbsp;<% =imgEntry.LongDescription %></p>
                                            <img alt="" src="<% =imgEntry.ImagePath %>" height="600px" width="840px"/></span></a>
                                </td>
                                <td>
                                    <% =imgEntry.SearchStringText%>
                                </td>
                                <td>dialoglink<% =imgEntry.DesignID %></td>
                                <td>dialog<% =imgEntry.DesignID %></td>
                                <%if (IsAdmin)
                                  { %>
                                <td>
                                    <div style="text-align:center;"><a id="EditDesign" class="ui-state-default ui-corner-all" href="WireDesignEdit.aspx?WireDesignID=<% =imgEntry.DesignID %>">&nbsp;Edit&nbsp;</a></div>
                                </td>
                                <%}
                                  else
                                  { %>
                                  <td></td>
                                  <%} %>
                            </tr>
                            <%
                                }
                               }
                            %>
                        </tbody>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%if (IsAdmin)
              { %>
            <table>
                  <tr>
                      <td>
                         <br /><a id="A1" class="ui-state-default ui-corner-all" href="WireDesignEdit.aspx">&nbsp;Add Design&nbsp;</a>
                      </td>
                  </tr>
            </table>
            <%} %>
        </div>
    </div>
</asp:Content>
