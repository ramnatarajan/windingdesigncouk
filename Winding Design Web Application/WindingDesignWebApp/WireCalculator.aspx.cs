﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WindingDesign.BusinessLayer;

namespace WindingDesign.WebApp
{
    public partial class WireCalculator : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateFields();
            }
        }

        #region Page Button Events

        /// <summary>
        /// 
        /// </summary>
        public enum FieldsClearType
        {
            ClearAll = 0,
            ClearOldData = 1,
            ClearNewData = 2,
            ClearEstimatorSection = 3,
            ClearStripSection = 4
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCCalcOD_Click(object sender, EventArgs e)
        {
            Calculate(DBHelper.WCCalculationZone.OldData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCClearOD_Click(object sender, EventArgs e)
        {
            ClearFields(FieldsClearType.ClearOldData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCCalcAll_Click(object sender, EventArgs e)
        {
            Calculate(DBHelper.WCCalculationZone.CalculateAll);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCClearAll_Click(object sender, EventArgs e)
        {
            ClearFields(FieldsClearType.ClearAll);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCCalcND_Click(object sender, EventArgs e)
        {
            Calculate(DBHelper.WCCalculationZone.NewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCClearND_Click(object sender, EventArgs e)
        {
            ClearFields(FieldsClearType.ClearNewData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCCalcEst_Click(object sender, EventArgs e)
        {
            Calculate(DBHelper.WCCalculationZone.Estimator);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCClearEst_Click(object sender, EventArgs e)
        {
            ClearFields(FieldsClearType.ClearEstimatorSection);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCCalcStrip_Click(object sender, EventArgs e)
        {
            Calculate(DBHelper.WCCalculationZone.StripSquare);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWCClearStrip_Click(object sender, EventArgs e)
        {
            ClearFields(FieldsClearType.ClearStripSection);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cZone"></param>
        private void Calculate(DBHelper.WCCalculationZone cZone)
        {
            if (cZone == DBHelper.WCCalculationZone.StripSquare)
            {
                ClearFields(FieldsClearType.ClearNewData);
                ClearFields(FieldsClearType.ClearOldData);
            }

            CaptureFields();

            WireCalculatorInstance.CalculatorMode cMode = WireCalculatorInstance.CalculatorMode.Imperial;

            if (rdoCalcTypeMetric.Checked)
            {
                cMode = WireCalculatorInstance.CalculatorMode.Metric;
            }

            CurrentToolInstance.WireCalculator = CurrentToolInstance.WireCalculator.Calculate(cZone, cMode, CurrentToolInstance.WireCalculator);

            PopulateFields();
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.WireCalculator != null)
            {
                txtJobIdentificationNumber.Text = CurrentToolInstance.WireCalculator.JobIdentificationNumber;

                txtWCODNoOfWires1.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld1.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld1.Value.ToString() : string.Empty;
                txtWCODNoOfWires2.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld2.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld2.Value.ToString() : string.Empty;
                txtWCODNoOfWires3.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld3.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld3.Value.ToString() : string.Empty;
                txtWCODNoOfWires4.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld4.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld4.Value.ToString() : string.Empty;
                txtWCODNoOfWires5.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld5.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld5.Value.ToString() : string.Empty;
                txtWCODNoOfWires6.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld6.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld6.Value.ToString() : string.Empty;
                txtWCODNoOfWires7.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld7.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld7.Value.ToString() : string.Empty;
                txtWCODNoOfWires8.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld8.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld8.Value.ToString() : string.Empty;
                txtWCODNoOfWires9.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld9.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld9.Value.ToString() : string.Empty;
                txtWCODNoOfWires10.Text = CurrentToolInstance.WireCalculator.NoOfWiresOld10.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresOld10.Value.ToString() : string.Empty;

                txtWCODDia1.Text = CurrentToolInstance.WireCalculator.WireDiameterOld1.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld1.Value.ToString() : string.Empty;
                txtWCODDia2.Text = CurrentToolInstance.WireCalculator.WireDiameterOld2.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld2.Value.ToString() : string.Empty;
                txtWCODDia3.Text = CurrentToolInstance.WireCalculator.WireDiameterOld3.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld3.Value.ToString() : string.Empty;
                txtWCODDia4.Text = CurrentToolInstance.WireCalculator.WireDiameterOld4.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld4.Value.ToString() : string.Empty;
                txtWCODDia5.Text = CurrentToolInstance.WireCalculator.WireDiameterOld5.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld5.Value.ToString() : string.Empty;
                txtWCODDia6.Text = CurrentToolInstance.WireCalculator.WireDiameterOld6.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld6.Value.ToString() : string.Empty;
                txtWCODDia7.Text = CurrentToolInstance.WireCalculator.WireDiameterOld7.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld7.Value.ToString() : string.Empty;
                txtWCODDia8.Text = CurrentToolInstance.WireCalculator.WireDiameterOld8.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld8.Value.ToString() : string.Empty;
                txtWCODDia9.Text = CurrentToolInstance.WireCalculator.WireDiameterOld9.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld9.Value.ToString() : string.Empty;
                txtWCODDia10.Text = CurrentToolInstance.WireCalculator.WireDiameterOld10.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterOld10.Value.ToString() : string.Empty;

                txtWCODArea1.Text = CurrentToolInstance.WireCalculator.WireAreaOld1.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld1.Value.ToString() : string.Empty;
                txtWCODArea2.Text = CurrentToolInstance.WireCalculator.WireAreaOld2.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld2.Value.ToString() : string.Empty;
                txtWCODArea3.Text = CurrentToolInstance.WireCalculator.WireAreaOld3.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld3.Value.ToString() : string.Empty;
                txtWCODArea4.Text = CurrentToolInstance.WireCalculator.WireAreaOld4.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld4.Value.ToString() : string.Empty;
                txtWCODArea5.Text = CurrentToolInstance.WireCalculator.WireAreaOld5.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld5.Value.ToString() : string.Empty;
                txtWCODArea6.Text = CurrentToolInstance.WireCalculator.WireAreaOld6.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld6.Value.ToString() : string.Empty;
                txtWCODArea7.Text = CurrentToolInstance.WireCalculator.WireAreaOld7.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld7.Value.ToString() : string.Empty;
                txtWCODArea8.Text = CurrentToolInstance.WireCalculator.WireAreaOld8.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld8.Value.ToString() : string.Empty;
                txtWCODArea9.Text = CurrentToolInstance.WireCalculator.WireAreaOld9.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld9.Value.ToString() : string.Empty;
                txtWCODArea10.Text = CurrentToolInstance.WireCalculator.WireAreaOld10.HasValue ? CurrentToolInstance.WireCalculator.WireAreaOld10.Value.ToString() : string.Empty;

                txtWCOldArea.Text = CurrentToolInstance.WireCalculator.OldArea.HasValue ? CurrentToolInstance.WireCalculator.OldArea.Value.ToString() : string.Empty;

                txtWCNDNoOfWires1.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew1.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew1.Value.ToString() : string.Empty;
                txtWCNDNoOfWires2.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew2.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew2.Value.ToString() : string.Empty;
                txtWCNDNoOfWires3.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew3.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew3.Value.ToString() : string.Empty;
                txtWCNDNoOfWires4.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew4.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew4.Value.ToString() : string.Empty;
                txtWCNDNoOfWires5.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew5.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew5.Value.ToString() : string.Empty;
                txtWCNDNoOfWires6.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew6.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew6.Value.ToString() : string.Empty;
                txtWCNDNoOfWires7.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew7.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew7.Value.ToString() : string.Empty;
                txtWCNDNoOfWires8.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew8.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew8.Value.ToString() : string.Empty;
                txtWCNDNoOfWires9.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew9.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew9.Value.ToString() : string.Empty;
                txtWCNDNoOfWires10.Text = CurrentToolInstance.WireCalculator.NoOfWiresNew10.HasValue ? CurrentToolInstance.WireCalculator.NoOfWiresNew10.Value.ToString() : string.Empty;

                txtWCNDDia1.Text = CurrentToolInstance.WireCalculator.WireDiameterNew1.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew1.Value.ToString() : string.Empty;
                txtWCNDDia2.Text = CurrentToolInstance.WireCalculator.WireDiameterNew2.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew2.Value.ToString() : string.Empty;
                txtWCNDDia3.Text = CurrentToolInstance.WireCalculator.WireDiameterNew3.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew3.Value.ToString() : string.Empty;
                txtWCNDDia4.Text = CurrentToolInstance.WireCalculator.WireDiameterNew4.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew4.Value.ToString() : string.Empty;
                txtWCNDDia5.Text = CurrentToolInstance.WireCalculator.WireDiameterNew5.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew5.Value.ToString() : string.Empty;
                txtWCNDDia6.Text = CurrentToolInstance.WireCalculator.WireDiameterNew6.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew6.Value.ToString() : string.Empty;
                txtWCNDDia7.Text = CurrentToolInstance.WireCalculator.WireDiameterNew7.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew7.Value.ToString() : string.Empty;
                txtWCNDDia8.Text = CurrentToolInstance.WireCalculator.WireDiameterNew8.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew8.Value.ToString() : string.Empty;
                txtWCNDDia9.Text = CurrentToolInstance.WireCalculator.WireDiameterNew9.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew9.Value.ToString() : string.Empty;
                txtWCNDDia10.Text = CurrentToolInstance.WireCalculator.WireDiameterNew10.HasValue ? CurrentToolInstance.WireCalculator.WireDiameterNew10.Value.ToString() : string.Empty;

                txtWCNDArea1.Text = CurrentToolInstance.WireCalculator.WireAreaNew1.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew1.Value.ToString() : string.Empty;
                txtWCNDArea2.Text = CurrentToolInstance.WireCalculator.WireAreaNew2.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew2.Value.ToString() : string.Empty;
                txtWCNDArea3.Text = CurrentToolInstance.WireCalculator.WireAreaNew3.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew3.Value.ToString() : string.Empty;
                txtWCNDArea4.Text = CurrentToolInstance.WireCalculator.WireAreaNew4.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew4.Value.ToString() : string.Empty;
                txtWCNDArea5.Text = CurrentToolInstance.WireCalculator.WireAreaNew5.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew5.Value.ToString() : string.Empty;
                txtWCNDArea6.Text = CurrentToolInstance.WireCalculator.WireAreaNew6.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew6.Value.ToString() : string.Empty;
                txtWCNDArea7.Text = CurrentToolInstance.WireCalculator.WireAreaNew7.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew7.Value.ToString() : string.Empty;
                txtWCNDArea8.Text = CurrentToolInstance.WireCalculator.WireAreaNew8.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew8.Value.ToString() : string.Empty;
                txtWCNDArea9.Text = CurrentToolInstance.WireCalculator.WireAreaNew9.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew9.Value.ToString() : string.Empty;
                txtWCNDArea10.Text = CurrentToolInstance.WireCalculator.WireAreaNew10.HasValue ? CurrentToolInstance.WireCalculator.WireAreaNew10.Value.ToString() : string.Empty;

                txtWCNewArea.Text = CurrentToolInstance.WireCalculator.NewArea.HasValue ? CurrentToolInstance.WireCalculator.NewArea.Value.ToString() : string.Empty;

                txtWCEstNoOfWires.Text = CurrentToolInstance.WireCalculator.EstimatorSectionNoOfWires.HasValue ? CurrentToolInstance.WireCalculator.EstimatorSectionNoOfWires.Value.ToString() : string.Empty;

                txtWCStripArea.Text = CurrentToolInstance.WireCalculator.StripSectionArea.HasValue ? CurrentToolInstance.WireCalculator.StripSectionArea.Value.ToString() : string.Empty;
                txtWCStripWidth.Text = CurrentToolInstance.WireCalculator.StripSectionWidth.HasValue ? CurrentToolInstance.WireCalculator.StripSectionWidth.Value.ToString() : string.Empty;
                txtWCStripDepth.Text = CurrentToolInstance.WireCalculator.StripSectionDepth.HasValue ? CurrentToolInstance.WireCalculator.StripSectionDepth.Value.ToString() : string.Empty;

                switch (CurrentToolInstance.WireCalculator.CurrentCalculationMode)
                {
                    case WireCalculatorInstance.CalculatorMode.Imperial:
                        rdoCalcTypeImperial.Checked = true;
                        break;
                    case WireCalculatorInstance.CalculatorMode.Metric:
                        rdoCalcTypeMetric.Checked = true;
                        break;
                }
            }

            lblResultmessage.Visible = CurrentToolInstance != null && CurrentToolInstance.WireCalculator != null && CurrentToolInstance.WireCalculator.ResultMessages != null && CurrentToolInstance.WireCalculator.ResultMessages.Count > 0 ? true : false;
            grdCalcResults.DataSource = CurrentToolInstance != null && CurrentToolInstance.WireCalculator != null && CurrentToolInstance.WireCalculator.ResultMessages != null && CurrentToolInstance.WireCalculator.ResultMessages.Count > 0 ? CurrentToolInstance.WireCalculator.ResultMessages : null;
            grdCalcResults.DataBind();

            if (!Page.IsPostBack)
            {
                AttachAlphaRestrictionToTextBoxes(Page.Controls);
            }

            SetMaxLengthAndToolTipToTextBoxes(Page.Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        private void CaptureFields()
        {
            if (CurrentToolInstance != null && CurrentToolInstance.WireCalculator != null)
            {
                CurrentToolInstance.WireCalculator.JobIdentificationNumber = txtJobIdentificationNumber.Text.Trim();

                CurrentToolInstance.WireCalculator.NoOfWiresOld1 = GetDouble(txtWCODNoOfWires1.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld2 = GetDouble(txtWCODNoOfWires2.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld3 = GetDouble(txtWCODNoOfWires3.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld4 = GetDouble(txtWCODNoOfWires4.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld5 = GetDouble(txtWCODNoOfWires5.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld6 = GetDouble(txtWCODNoOfWires6.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld7 = GetDouble(txtWCODNoOfWires7.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld8 = GetDouble(txtWCODNoOfWires8.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld9 = GetDouble(txtWCODNoOfWires9.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresOld10 = GetDouble(txtWCODNoOfWires10.Text.Trim());

                CurrentToolInstance.WireCalculator.WireDiameterOld1 = GetDouble(txtWCODDia1.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld2 = GetDouble(txtWCODDia2.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld3 = GetDouble(txtWCODDia3.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld4 = GetDouble(txtWCODDia4.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld5 = GetDouble(txtWCODDia5.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld6 = GetDouble(txtWCODDia6.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld7 = GetDouble(txtWCODDia7.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld8 = GetDouble(txtWCODDia8.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld9 = GetDouble(txtWCODDia9.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterOld10 = GetDouble(txtWCODDia10.Text.Trim());

                CurrentToolInstance.WireCalculator.NoOfWiresNew1 = GetDouble(txtWCNDNoOfWires1.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew2 = GetDouble(txtWCNDNoOfWires2.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew3 = GetDouble(txtWCNDNoOfWires3.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew4 = GetDouble(txtWCNDNoOfWires4.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew5 = GetDouble(txtWCNDNoOfWires5.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew6 = GetDouble(txtWCNDNoOfWires6.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew7 = GetDouble(txtWCNDNoOfWires7.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew8 = GetDouble(txtWCNDNoOfWires8.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew9 = GetDouble(txtWCNDNoOfWires9.Text.Trim());
                CurrentToolInstance.WireCalculator.NoOfWiresNew10 = GetDouble(txtWCNDNoOfWires10.Text.Trim());

                CurrentToolInstance.WireCalculator.WireDiameterNew1 = GetDouble(txtWCNDDia1.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew2 = GetDouble(txtWCNDDia2.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew3 = GetDouble(txtWCNDDia3.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew4 = GetDouble(txtWCNDDia4.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew5 = GetDouble(txtWCNDDia5.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew6 = GetDouble(txtWCNDDia6.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew7 = GetDouble(txtWCNDDia7.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew8 = GetDouble(txtWCNDDia8.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew9 = GetDouble(txtWCNDDia9.Text.Trim());
                CurrentToolInstance.WireCalculator.WireDiameterNew10 = GetDouble(txtWCNDDia10.Text.Trim());

                CurrentToolInstance.WireCalculator.EstimatorSectionNoOfWires = GetDouble(txtWCEstNoOfWires.Text.Trim());

                CurrentToolInstance.WireCalculator.StripSectionWidth = GetDouble(txtWCStripWidth.Text.Trim());
                CurrentToolInstance.WireCalculator.StripSectionDepth = GetDouble(txtWCStripDepth.Text.Trim());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cType"></param>
        private void ClearFields(FieldsClearType cType)
        {
            switch (cType)
            {
                case FieldsClearType.ClearAll:
                    ClearFields(FieldsClearType.ClearOldData);
                    ClearFields(FieldsClearType.ClearNewData);
                    ClearFields(FieldsClearType.ClearEstimatorSection);
                    ClearFields(FieldsClearType.ClearStripSection);
                    CurrentToolInstance.WireCalculator = new WireCalculatorInstance();
                    txtJobIdentificationNumber.Text = string.Empty;
                    break;
                case FieldsClearType.ClearOldData:
                    txtWCODNoOfWires1.Text = string.Empty;
                    txtWCODNoOfWires2.Text = string.Empty;
                    txtWCODNoOfWires3.Text = string.Empty;
                    txtWCODNoOfWires4.Text = string.Empty;
                    txtWCODNoOfWires5.Text = string.Empty;
                    txtWCODNoOfWires6.Text = string.Empty;
                    txtWCODNoOfWires7.Text = string.Empty;
                    txtWCODNoOfWires8.Text = string.Empty;
                    txtWCODNoOfWires9.Text = string.Empty;
                    txtWCODNoOfWires10.Text = string.Empty;

                    txtWCODDia1.Text = string.Empty;
                    txtWCODDia2.Text = string.Empty;
                    txtWCODDia3.Text = string.Empty;
                    txtWCODDia4.Text = string.Empty;
                    txtWCODDia5.Text = string.Empty;
                    txtWCODDia6.Text = string.Empty;
                    txtWCODDia7.Text = string.Empty;
                    txtWCODDia8.Text = string.Empty;
                    txtWCODDia9.Text = string.Empty;
                    txtWCODDia10.Text = string.Empty;

                    txtWCODArea1.Text = string.Empty;
                    txtWCODArea2.Text = string.Empty;
                    txtWCODArea3.Text = string.Empty;
                    txtWCODArea4.Text = string.Empty;
                    txtWCODArea5.Text = string.Empty;
                    txtWCODArea6.Text = string.Empty;
                    txtWCODArea7.Text = string.Empty;
                    txtWCODArea8.Text = string.Empty;
                    txtWCODArea9.Text = string.Empty;
                    txtWCODArea10.Text = string.Empty;

                    txtWCOldArea.Text = string.Empty;
                    break;
                case FieldsClearType.ClearNewData:
                    txtWCNDNoOfWires1.Text = string.Empty;
                    txtWCNDNoOfWires2.Text = string.Empty;
                    txtWCNDNoOfWires3.Text = string.Empty;
                    txtWCNDNoOfWires4.Text = string.Empty;
                    txtWCNDNoOfWires5.Text = string.Empty;
                    txtWCNDNoOfWires6.Text = string.Empty;
                    txtWCNDNoOfWires7.Text = string.Empty;
                    txtWCNDNoOfWires8.Text = string.Empty;
                    txtWCNDNoOfWires9.Text = string.Empty;
                    txtWCNDNoOfWires10.Text = string.Empty;

                    txtWCNDDia1.Text = string.Empty;
                    txtWCNDDia2.Text = string.Empty;
                    txtWCNDDia3.Text = string.Empty;
                    txtWCNDDia4.Text = string.Empty;
                    txtWCNDDia5.Text = string.Empty;
                    txtWCNDDia6.Text = string.Empty;
                    txtWCNDDia7.Text = string.Empty;
                    txtWCNDDia8.Text = string.Empty;
                    txtWCNDDia9.Text = string.Empty;
                    txtWCNDDia10.Text = string.Empty;

                    txtWCNDArea1.Text = string.Empty;
                    txtWCNDArea2.Text = string.Empty;
                    txtWCNDArea3.Text = string.Empty;
                    txtWCNDArea4.Text = string.Empty;
                    txtWCNDArea5.Text = string.Empty;
                    txtWCNDArea6.Text = string.Empty;
                    txtWCNDArea7.Text = string.Empty;
                    txtWCNDArea8.Text = string.Empty;
                    txtWCNDArea9.Text = string.Empty;
                    txtWCNDArea10.Text = string.Empty;

                    txtWCNewArea.Text = string.Empty;
                    break;
                case FieldsClearType.ClearEstimatorSection:
                    txtWCEstNoOfWires.Text = string.Empty;
                    break;
                case FieldsClearType.ClearStripSection:
                    txtWCStripArea.Text = string.Empty;
                    txtWCStripWidth.Text = string.Empty;
                    txtWCStripDepth.Text = string.Empty;
                    break;
            }

            grdCalcResults.DataSource = null;
            grdCalcResults.DataBind();
            lblResultmessage.Visible = false;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CalculationTypeChanged(object sender, EventArgs e)
        {
            CaptureFields();

            if (rdoCalcTypeMetric.Checked)
            {
                lblOldDataType.Text = " mm's";
                lblNewDataType.Text = " mm's";
            }
            else
            {
                lblOldDataType.Text = " inches";
                lblNewDataType.Text = " inches";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Calculate(DBHelper.WCCalculationZone.CalculateAll);
            Session["WCInstance"] = CurrentToolInstance.WireCalculator;

            ResponseHelper.Redirect("PrintCalcJob.aspx?CalcType=" + EnumHelper.GetStringValue(CalculationToolType.WireCalculator), "_blank", "menubar=0,width=800,height=600");
        }
    }
}