﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Sample.aspx.cs" Inherits="WindingDesign.WebApp.Sample" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <asp:UpdatePanel ID="upHome" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                $(function () {
                    $("#accordion").accordion({ header: "h3", autoHeight: false });
                    $('#tabs').tabs();
                    $('#datepicker').datepicker({
                        inline: true
                    });
                    $('#dialog').dialog({
                        autoOpen: false,
                        width: 600,
                        modal: true,
                        buttons: {
                            "Ok": function () {
                                $(this).dialog("close");
                            },
                            "Cancel": function () {
                                $(this).dialog("close");
                            }
                        }
                    });

                    $('#dialog_link').click(function () {
                        $('#dialog').dialog('open');
                        return false;
                    });
                    $('#dialog_link, ul#icons li').hover(
					function () { $(this).addClass('ui-state-hover'); },
					function () { $(this).removeClass('ui-state-hover'); }
				);
                });
            </script>
            <style type="text/css">
                /*demo page css*/
                body
                {
                    font: 62.5% "Trebuchet MS" , sans-serif;
                    margin: 50px;
                }
                .demoHeaders
                {
                    margin-top: 2em;
                }
                #dialog_link
                {
                    padding: .4em 1em .4em 20px;
                    text-decoration: none;
                    position: relative;
                }
                #dialog_link span.ui-icon
                {
                    margin: 0 5px 0 0;
                    position: absolute;
                    left: .2em;
                    top: 50%;
                    margin-top: -8px;
                }
                ul#icons
                {
                    margin: 0;
                    padding: 0;
                }
                ul#icons li
                {
                    margin: 2px;
                    position: relative;
                    padding: 4px 0;
                    cursor: pointer;
                    float: left;
                    list-style: none;
                }
                ul#icons span.ui-icon
                {
                    float: left;
                    margin: 0 4px;
                }
            </style>
            <h2 class="demoHeaders">
                Accordion</h2>
            <div id="accordion" style="width: 100%">
                <div>
                    <h3>
                        <a href="#">First</a></h3>
                    <div>
                        Phasellus mattis tincidunt nibh. Cras orci urna, blandit id, pretium vel, aliquet
                        ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in enim dictum
                        bibendum. Phasellus mattis tincidunt nibh. Cras orci urna, blandit id, pretium vel,
                        aliquet ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in enim
                        dictum bibendum. Phasellus mattis tincidunt nibh. Cras orci urna, blandit id, pretium
                        vel, aliquet ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in
                        enim dictum bibendum. Phasellus mattis tincidunt nibh. Cras orci urna, blandit id,
                        pretium vel, aliquet ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed
                        lorem in enim dictum bibendum.</div>
                </div>
                <div>
                    <h3>
                        <a href="#">Second</a></h3>
                    <div>
                        Phasellus mattis tincidunt nibh.</div>
                </div>
                <div>
                    <h3>
                        <a href="#">Third</a></h3>
                    <div>
                        Nam dui erat, auctor a, dignissim quis.</div>
                </div>
            </div>
            <h2 class="demoHeaders">
                Tabs</h2>
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">First</a></li>
                    <li><a href="#tabs-2">Second</a></li>
                    <li><a href="#tabs-3">Third</a></li>
                </ul>
                <div id="tabs-1">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                <div id="tabs-2">
                    Phasellus mattis tincidunt nibh. Cras orci urna, blandit id, pretium vel, aliquet
                    ornare, felis. Maecenas scelerisque sem non nisl. Fusce sed lorem in enim dictum
                    bibendum.</div>
                <div id="tabs-3">
                    Nam dui erat, auctor a, dignissim quis, sollicitudin eu, felis. Pellentesque nisi
                    urna, interdum eget, sagittis et, consequat vestibulum, lacus. Mauris porttitor
                    ullamcorper augue.</div>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_s-xclick" />
                <input type="hidden" name="hosted_button_id" value="7F8FGFNQTMMRG" />
                <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_buynowCC_LG.gif"
                    border="0" name="submit" alt="PayPal — The safer, easier way to pay online." />
                <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif"
                    width="1" height="1" />
                </form>
            </div>
            <!-- Datepicker -->
            <div>
                <h2 class="demoHeaders">
                    Datepicker</h2>
                <p>
                    Date:
                    <input type="text" id="datepicker"></p>
            </div>
            <div>
                <h2 class="demoHeaders">
                    Dialog</h2>
                <p>
                    <a href="#" id="dialog_link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-newwin">
                    </span>Open Dialog</a></p>
                <!-- ui-dialog -->
                <div id="dialog" title="Dialog Title">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
