﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    public partial class TrainingVideoEdit : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        private int currentTrainingVideoID;

        /// <summary>
        /// 
        /// </summary>
        private BusinessLayer.TrainingVideo currentTrainingVideo;

        /// <summary>
        /// 
        /// </summary>
        public int CurrentTrainingVideoID
        {
            get
            {
                currentTrainingVideoID = Request.QueryString["TrainingVideoID"] != null && Request.QueryString["TrainingVideoID"].ToString() != "" ? Convert.ToInt32(Request.QueryString["TrainingVideoID"].ToString()) : 0;
                return currentTrainingVideoID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public BusinessLayer.TrainingVideo CurrentTrainingVideo
        {
            get
            {
                if (currentTrainingVideo == null)
                {
                    if (CurrentTrainingVideoID != 0)
                    {
                        currentTrainingVideo = (from tv in CurrentEntityContext.TrainingVideos where tv.TrainingVideoID == CurrentTrainingVideoID select tv).FirstOrDefault();
                    }
                    else
                    {
                        currentTrainingVideo = new BusinessLayer.TrainingVideo();
                    }
                }

                return currentTrainingVideo;
            }

            set
            {
                currentTrainingVideo = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CurrentTrainingVideo != null)
                { PopulateFields(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void PopulateFields()
        {
            if (CurrentTrainingVideo != null)
            {
                txtTitle.Text = CurrentTrainingVideo.ShortDescription;
                txtDescription.Text = CurrentTrainingVideo.LongDescription;
                txtEmbedVideoPath.Text = CurrentTrainingVideo.VideoEmbedPath;

                if (CurrentTrainingVideoID == 0)
                {
                    lblEditVideoTitle.Text = "Add New Video";
                }
                else
                {
                    lblEditVideoTitle.Text = "Edit Video";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteTrainingVideo(CurrentTrainingVideo.TrainingVideoID);
            Response.Redirect("TrainingVideos.aspx");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("TrainingVideos.aspx");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveVideo_Click(object sender, EventArgs e)
        {
            CurrentTrainingVideo.ShortDescription = txtTitle.Text.Trim();
            CurrentTrainingVideo.LongDescription = txtDescription.Text.Trim();
            CurrentTrainingVideo.VideoEmbedPath = txtEmbedVideoPath.Text.Trim();

            AddEditTrainingVideo(CurrentTrainingVideo.TrainingVideoID, CurrentTrainingVideo.ShortDescription, CurrentTrainingVideo.LongDescription, CurrentTrainingVideo.VideoEmbedPath);

            Response.Redirect("TrainingVideos.aspx");
        }
    }
}