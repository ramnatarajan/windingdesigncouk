﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WindingDesign.BusinessLayer;

namespace WindingDesign.WebApp
{
    public partial class WireDesignEdit : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        private int currentWireDesignID;

        /// <summary>
        /// 
        /// </summary>
        private BusinessLayer.WireDesign currentWireDesign;

        /// <summary>
        /// 
        /// </summary>
        public int CurrentWireDesignID
        {
            get
            {
                currentWireDesignID = Request.QueryString["WireDesignID"] != null && Request.QueryString["WireDesignID"].ToString() != "" ? Convert.ToInt32(Request.QueryString["WireDesignID"].ToString()) : 0;
                return currentWireDesignID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public BusinessLayer.WireDesign CurrentWireDesign
        {
            get
            {
                if (currentWireDesign == null)
                {
                    if (CurrentWireDesignID != 0)
                    {
                        currentWireDesign = (from wd in CurrentEntityContext.WireDesigns where wd.WireDesignID == CurrentWireDesignID select wd).FirstOrDefault();
                    }
                    else
                    {
                        currentWireDesign = new BusinessLayer.WireDesign();
                    }
                }

                return currentWireDesign;
            }

            set
            {
                currentWireDesign = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CurrentWireDesign != null)
                { PopulateFields(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void PopulateFields()
        {
            lstDesignTypes.DataSource = DesignTypeSource;
            lstDesignTypes.DataTextField = "Description";
            lstDesignTypes.DataValueField = "DesignTypeID";
            lstDesignTypes.DataBind();

            if (CurrentWireDesign != null)
            {
                txtTitle.Text = CurrentWireDesign.Title;
                txtDescription.Text = CurrentWireDesign.Description;
                if (CurrentWireDesignID == 0)
                {
                    lblEditDesignTitle.Text = "Add New Design";
                }
                else
                {
                    lblEditDesignTitle.Text = "Edit Design";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUploadImage_Click(object sender, EventArgs e)
        {
            CurrentWireDesign.Title = txtTitle.Text.Trim();
            CurrentWireDesign.Description = txtDescription.Text.Trim();

            AddEditWireDesign(CurrentWireDesign.WireDesignID, CurrentWireDesign.Title, CurrentWireDesign.Description, fluImageUploader.PostedFile, Convert.ToInt32(lstDesignTypes.SelectedValue));

            Response.Redirect("DesignGallery.aspx");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteWireDesign(CurrentWireDesign.WireDesignID);
            Response.Redirect("DesignGallery.aspx");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("DesignGallery.aspx");
        }
    }
}