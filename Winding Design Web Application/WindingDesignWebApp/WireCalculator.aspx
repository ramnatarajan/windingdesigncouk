﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="WireCalculator.aspx.cs" Inherits="WindingDesign.WebApp.WireCalculator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upWireCalc" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="text-align: left; width: 70%;">
                                                &nbsp;&nbsp;The Wire Calculator
                                            </td>
                                            <td style="text-align: right; width: 30%;">
                                                Job Number&nbsp;
                                                <asp:TextBox ID="txtJobIdentificationNumber" Width="70px" CssClass="SiteTextBoxes"
                                                    runat="server" MaxLength="10" />
                                                &nbsp;
                                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="CalcButtons" OnClick="btnPrint_Click"
                                                    ToolTip="Please allow popups to use print functionality" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTable">
                        <tr>
                            <td>
                                <div class="WireCalcLeft">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Old Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                &nbsp;&nbsp;Enter number of wires & diameters in<asp:Label ID="lblOldDataType" runat="server"
                                                    Text=" mm's" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;&nbsp;No.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            Dia.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            Area
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires1" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia1" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea1" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires2" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia2" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea2" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires3" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia3" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea3" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires4" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia4" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea4" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires5" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia5" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea5" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires6" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia6" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea6" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires7" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia7" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea7" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires8" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia8" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea8" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires9" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia9" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea9" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCODNoOfWires10" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCODDia10" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCODArea10" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                        </td>
                                                        <td style="text-align: right;">
                                                            Old Area =&nbsp;
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCOldArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="94%">
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <asp:Button ID="btnWCCalcOD" CssClass="CalcButtons" runat="server" Width="50px" Text="Calc"
                                                                OnClick="btnWCCalcOD_Click" />
                                                            <asp:Button ID="btnWCClearOD" CssClass="CalcButtons" runat="server" Width="50px"
                                                                Text="Clear" OnClick="btnWCClearOD_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="vertical-align: text-top;">
                                <div class="WireCalcMiddle">
                                    <table class="WireCalcMiddleTable">
                                        <tr>
                                            <td class="WireCalcMiddleTableTdLeft">
                                                <asp:RadioButton ID="rdoCalcTypeMetric" CssClass="WireCalcMiddleTableRadio" runat="server"
                                                    Text="Metric" Checked="true" GroupName="CalcType" OnCheckedChanged="CalculationTypeChanged"
                                                    AutoPostBack="True" />&nbsp;
                                            </td>
                                            <td class="WireCalcMiddleTableTdRight">
                                                &nbsp;<asp:RadioButton ID="rdoCalcTypeImperial" CssClass="WireCalcMiddleTableRadio"
                                                    runat="server" Text="Imperial" GroupName="CalcType" OnCheckedChanged="CalculationTypeChanged"
                                                    AutoPostBack="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcMiddleTableTdLeft">
                                                <asp:Button ID="btnWCCalcAll" CssClass="CalcButtons" runat="server" Text="Calc All"
                                                    OnClick="btnWCCalcAll_Click" />
                                            </td>
                                            <td class="WireCalcMiddleTableTdRight">
                                                &nbsp;&nbsp;<asp:Button ID="btnWCClearAll" CssClass="CalcButtons" runat="server"
                                                    Text="Clear All" OnClick="btnWCClearAll_Click" />&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="text-align: center; width: 100%;">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    <asp:Label runat="server" ID="lblResultmessage" Text="Results" /></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="grdCalcResults" runat="server" AutoGenerateColumns="False" BorderColor="Transparent"
                                        AllowPaging="False" ShowHeader="False">
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False" ControlStyle-BorderWidth="0px">
                                                <ItemTemplate>
                                                    <div class="ResultValidationMessage">
                                                        <asp:Label ID="lblMessage" runat="server" Text='<%# Container.DataItem %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                            <td>
                                <div class="WireCalcRight">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    New Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                &nbsp;&nbsp;Enter number of wires & diameters in<asp:Label ID="lblNewDataType" runat="server"
                                                    Text=" mm's" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;&nbsp;No.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            Dia.
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            Area
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires1" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia1" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea1" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires2" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia2" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea2" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires3" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia3" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea3" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires4" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia4" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea4" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires5" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia5" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea5" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires6" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia6" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea6" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires7" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia7" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea7" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires8" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia8" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea8" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires9" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia9" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea9" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                            &nbsp;&nbsp;
                                                            <asp:TextBox ID="txtWCNDNoOfWires10" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd2">
                                                            <asp:TextBox ID="txtWCNDDia10" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                                MaxLength="8" />
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNDArea10" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WireCalcOldDataInnerTableTd1">
                                                        </td>
                                                        <td style="text-align: right;">
                                                            New Area =&nbsp;
                                                        </td>
                                                        <td class="WireCalcOldDataInnerTableTd3">
                                                            <asp:TextBox ID="txtWCNewArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="94%">
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right;">
                                                            <asp:Button ID="btnWCCalcND" CssClass="CalcButtons" runat="server" Text="Calc" Width="50px"
                                                                OnClick="btnWCCalcND_Click" />
                                                            <asp:Button ID="btnWCClearND" CssClass="CalcButtons" runat="server" Text="Clear"
                                                                Width="50px" OnClick="btnWCClearND_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 0px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTableBottom">
                        <tr>
                            <td>
                                <div class="WireCalcBottomLeft">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Estimator</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 60%;">
                                                <br />
                                                &nbsp;&nbsp;&nbsp;How many wires you want to work with
                                            </td>
                                            <td style="width: 10%; text-align: left;">
                                                <br />
                                                <asp:TextBox ID="txtWCEstNoOfWires" Width="25px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="8" />
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <br />
                                                <asp:Button ID="btnWCCalcEst" CssClass="CalcButtons" runat="server" Text="Calc" Width="50px"
                                                    OnClick="btnWCCalcEst_Click" />
                                                <asp:Button ID="btnWCClearEst" CssClass="CalcButtons" runat="server" Text="Clear"
                                                    Width="50px" OnClick="btnWCClearEst_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%; height: 25px;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td>
                                <div class="WireCalcBottomRight">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Strip And Square Section</div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 24%;">
                                                &nbsp;&nbsp;&nbsp;&nbsp;Width<br />
                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtWCStripWidth" Width="50px" CssClass="SiteTextBoxes"
                                                    runat="server" MaxLength="8" />
                                            </td>
                                            <td style="width: 24%; text-align: left">
                                                Depth<br />
                                                <asp:TextBox ID="txtWCStripDepth" Width="50px" CssClass="SiteTextBoxes" runat="server"
                                                    MaxLength="8" />
                                            </td>
                                            <td style="width: 24%; text-align: left">
                                                Area<br />
                                                <asp:TextBox ID="txtWCStripArea" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                    runat="server" />
                                            </td>
                                            <td style="width: 40%; text-align: left">
                                                <br />
                                                <asp:Button ID="btnWCCalcStrip" CssClass="CalcButtons" runat="server" Text="Calc"
                                                    Width="50px" OnClick="btnWCCalcStrip_Click" />
                                                <asp:Button ID="btnWCClearStrip" CssClass="CalcButtons" runat="server" Text="Clear"
                                                    Width="50px" OnClick="btnWCClearStrip_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 90%; text-align: left; height: 25px;">
                                                &nbsp;&nbsp;<i>The radii of the corners have not been taken into account</i>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
