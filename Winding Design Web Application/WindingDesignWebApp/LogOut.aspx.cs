﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WindingDesign.BusinessLayer;

namespace WindingDesign.WebApp
{
    public partial class LogOut : PageBase
    {   
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["CurrentClientID"] = null;
            CurrentToolInstance.ResetCalculatorInstances();
            HttpContext.Current.Session["CTInstance"] = null;
            Response.Redirect("LogIn.aspx");
        }
    }
}
