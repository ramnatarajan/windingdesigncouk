﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WindingDesign.WebApp
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PrintCalcJob : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        private BusinessLayer.WireCalculatorInstance currentWireCalculatorInstance;

        /// <summary>
        /// 
        /// </summary>
        private BusinessLayer.VoltageConversionInstance currentVoltageConversionInstance;
        
        /// <summary>
        /// 
        /// </summary>
        private BusinessLayer.PowerConversionInstance currentPowerConversionInstance;

        /// <summary>
        /// 
        /// </summary>
        private BusinessLayer.CoilConversionInstance currentCoilConversionInstance;

        /// <summary>
        /// 
        /// </summary>
        private BusinessLayer.SpeedConversionInstance currentSpeedConversionInstance;

        /// <summary>
        /// Gets the current label.
        /// </summary>
        /// <value>
        /// The current label.
        /// </value>
        public string CurrentLabel
        {
            get
            {
                switch (CurrentToolType)
                {
                    case CalculationToolType.WireCalculator:
                        return "Wire Calculator - Job Number: " + CurrentWireCalculatorInstance.JobIdentificationNumber + " - Job Date: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                    case CalculationToolType.VoltageConversion:
                        return "Voltage Conversion - Job Number: " + CurrentVoltageConversionInstance.JobIdentificationNumber + " - Job Date: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                    case CalculationToolType.PowerConversion:
                        return "Power Conversion - Job Number: " + CurrentPowerConversionInstance.JobIdentificationNumber + " - Job Date: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                    case CalculationToolType.CoilConversion:
                        return "Coil Conversion - Job Number: " + CurrentCoilConversionInstance.JobIdentificationNumber + " - Job Date: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                    case CalculationToolType.SpeedConversion:
                        return "Speed Conversion - Job Number: " + CurrentSpeedConversionInstance.JobIdentificationNumber + " - Job Date: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                }

                return "Wire Calculator - Job Date: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
            }
        }

        /// <summary>
        /// Gets the current wire calculator instance.
        /// </summary>
        /// <value>
        /// The current wire calculator instance.
        /// </value>
        public BusinessLayer.WireCalculatorInstance CurrentWireCalculatorInstance
        {
            get
            {
                if (currentWireCalculatorInstance == null && Session["WCInstance"] != null)
                {
                    currentWireCalculatorInstance = (BusinessLayer.WireCalculatorInstance)Session["WCInstance"];
                }

                return currentWireCalculatorInstance;
            }
        }

        /// <summary>
        /// Gets the current voltage conversion instance.
        /// </summary>
        /// <value>
        /// The current voltage conversion instance.
        /// </value>
        public BusinessLayer.VoltageConversionInstance CurrentVoltageConversionInstance
        {
            get
            {
                if (currentVoltageConversionInstance == null && Session["VCInstance"] != null)
                {
                    currentVoltageConversionInstance = (BusinessLayer.VoltageConversionInstance)Session["VCInstance"];
                }

                return currentVoltageConversionInstance;
            }
        }

        /// <summary>
        /// Gets the current power conversion instance.
        /// </summary>
        /// <value>
        /// The current power conversion instance.
        /// </value>
        public BusinessLayer.PowerConversionInstance CurrentPowerConversionInstance
        {
            get
            {
                if (currentPowerConversionInstance == null && Session["PCInstance"] != null)
                {
                    currentPowerConversionInstance = (BusinessLayer.PowerConversionInstance)Session["PCInstance"];
                }

                return currentPowerConversionInstance;
            }
        }

        /// <summary>
        /// Gets the current coil conversion instance.
        /// </summary>
        /// <value>
        /// The current coil conversion instance.
        /// </value>
        public BusinessLayer.CoilConversionInstance CurrentCoilConversionInstance
        {
            get
            {
                if (currentCoilConversionInstance == null && Session["CCInstance"] != null)
                {
                    currentCoilConversionInstance = (BusinessLayer.CoilConversionInstance)Session["CCInstance"];
                }

                return currentCoilConversionInstance;
            }
        }

        /// <summary>
        /// Gets the current speed conversion instance.
        /// </summary>
        /// <value>
        /// The current speed conversion instance.
        /// </value>
        public BusinessLayer.SpeedConversionInstance CurrentSpeedConversionInstance
        {
            get
            {
                if (currentSpeedConversionInstance == null && Session["SCInstance"] != null)
                {
                    currentSpeedConversionInstance = (BusinessLayer.SpeedConversionInstance)Session["SCInstance"];
                }

                return currentSpeedConversionInstance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateFields();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateFields()
        {
            lblTitle.Text = CurrentLabel;

            switch (CurrentToolType)
            {
                case CalculationToolType.WireCalculator:
                    grdCalcResults.DataSource = CurrentWireCalculatorInstance != null && CurrentWireCalculatorInstance.ResultMessages != null ? CurrentWireCalculatorInstance.ResultMessages : null;
                    grdCalcResults.DataBind();
                    break;
                case CalculationToolType.VoltageConversion:
                    grdCalcResults.DataSource = CurrentVoltageConversionInstance != null && CurrentVoltageConversionInstance.ResultMessages != null ? CurrentVoltageConversionInstance.ResultMessages : null;
                    grdCalcResults.DataBind();
                    break;
                case CalculationToolType.PowerConversion:
                    grdCalcResults.DataSource = CurrentPowerConversionInstance != null && CurrentPowerConversionInstance.ResultMessages != null ? CurrentPowerConversionInstance.ResultMessages : null;
                    grdCalcResults.DataBind();
                    break;
                case CalculationToolType.CoilConversion:
                    grdCalcResults.DataSource = CurrentCoilConversionInstance != null && CurrentCoilConversionInstance.ResultMessages != null ? CurrentCoilConversionInstance.ResultMessages : null;
                    grdCalcResults.DataBind();
                    break;
                case CalculationToolType.SpeedConversion:
                    grdCalcResults.DataSource = CurrentSpeedConversionInstance != null && CurrentSpeedConversionInstance.ResultMessages != null ? CurrentSpeedConversionInstance.ResultMessages : null;
                    grdCalcResults.DataBind();
                    break;
            }
        }
    }
}