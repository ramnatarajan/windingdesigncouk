﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintCalcJob.aspx.cs" Inherits="WindingDesign.WebApp.PrintCalcJob" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Engineering Publications - Print Your Work</title>
    <link rel="stylesheet" type="text/css" href="Styles/Site.css" />
    <link type="text/css" href="Styles/cupertino/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
</head>
<body onload="window.print();">
    <form id="frmPrintCalc" runat="server">
    <div class="PrintJobMainBox">
        <div class="PrintJobBoxTrDiv">
            <table>
                <tr>
                    <td>
                        <p class="PrintJobHeading">
                            Engineering Publications LTD<br />
                            <asp:Label ID="lblTitle" runat="server" Text="Wire Calculator" CssClass="WireCalcOldDataTableLabel" />
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <% switch (CurrentToolType)
           {
               case CalculationToolType.WireCalculator:
                   if (CurrentWireCalculatorInstance != null)
                   {
        %>
        <div class="PrintJobBoxTrDiv">
            <table class="PrintJobBoxTrDiv" style="text-align: left;">
                <tr>
                    <td>
                        <p class="PrintJobHeading">
                            Old data</p>
                        Number of wires & diameters in
                        <%= CurrentWireCalculatorInstance.CurrentCalculationModeString %>
                    </td>
                    <td>
                        <p class="PrintJobHeading">
                            New data</p>
                        Number of wires & diameters in
                        <%= CurrentWireCalculatorInstance.CurrentCalculationModeString %>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table class="PrintJobBoxTrDiv">
                <tr>
                    <td>
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top">
                            <tr>
                                <td>
                                    <p class="PrintJobHeading">
                                        No.</p>
                                </td>
                                <td>
                                    <p class="PrintJobHeading">
                                        Dia.</p>
                                </td>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        &nbsp;Area</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld1%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld1%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld2%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld2%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld3%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld3%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld4%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld4%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld5%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld5%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld6%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld6%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld6%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld7%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld7%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld7%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld8%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld8%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld8%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld9%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld9%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld9%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresOld10%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterOld10%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaOld10%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        &nbsp;Old Area</p>
                                </td>
                                <td>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.OldArea%>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top">
                            <tr>
                                <td>
                                    <p class="PrintJobHeading">
                                        No.</p>
                                </td>
                                <td>
                                    <p class="PrintJobHeading">
                                        Dia.</p>
                                </td>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        &nbsp;Area</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew1%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew1%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew2%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew2%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew3%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew3%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew4%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew4%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew5%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew5%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew6%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew6%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew6%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew7%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew7%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew7%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew8%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew8%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew8%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew9%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew9%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew9%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.NoOfWiresNew10%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireDiameterNew10%>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.WireAreaNew10%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        &nbsp;New Area</p>
                                </td>
                                <td>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentWireCalculatorInstance.NewArea%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="PrintJobBoxTrDiv">
            <table class="PrintJobBoxTrDiv" style="text-align: left;">
                <tr>
                    <td>
                        <p class="PrintJobHeading">
                            Estimator</p>
                    </td>
                    <td>
                        <p class="PrintJobHeading">
                            Strip And Square Section</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        Number of wires you want to work with :
                        <%= CurrentWireCalculatorInstance.EstimatorSectionNoOfWires%>
                    </td>
                    <td style="text-align: left;">
                        Width:
                        <%= CurrentWireCalculatorInstance.StripSectionWidth%>, Depth:
                        <%= CurrentWireCalculatorInstance.StripSectionDepth%>, Area :<%= CurrentWireCalculatorInstance.StripSectionArea%>
                    </td>
                </tr>
            </table>
        </div>
        <%
                   }
                   break;
               case CalculationToolType.VoltageConversion:
                   if (CurrentVoltageConversionInstance != null)
                   {
        %>
        <table class="PrintJobBoxTrDiv">
            <tr>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 15px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 15px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        Original data</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Supply Voltage
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.SupplyVoltageOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Full Load Current
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.FullLoadCurrentOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Phase Voltage
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.PhaseVoltageOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Phase Current
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.PhaseCurrentOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Connection
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.ConnectionOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Supply Frequency
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.SupplyFrequencyOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Output Power (kW's)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.OutputPowerOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Turns
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.TurnsOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Dia.
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.WireDiameterOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No. of Paths
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.NoOfPathsOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Full Load Speed
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.FLSpeedOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cooling
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.CoolingOriginal%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 35px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 35px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        New data</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Supply Voltage
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.SupplyVoltageNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Approx. Full Load Current
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.FullLoadCurrentNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Phase Voltage
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.PhaseVoltageNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Approx. Phase Current
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.PhaseCurrentNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Connection
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.ConnectionNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Supply Frequency
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.SupplyFrequencyNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Output Power (kW's)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.OutputPowerNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Turns
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.TurnsNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Dia.
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.WireDiameterNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No. of Paths
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.NoOfPathsNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Full Load Speed
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.FLSpeedNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cooling
                                </td>
                                <td>
                                    &nbsp;<%= CurrentVoltageConversionInstance.CoolingNew%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <%
                   }
                   break;
               case CalculationToolType.PowerConversion:
                   if (CurrentPowerConversionInstance != null)
                   {
        %>
        <table class="PrintJobBoxTrDiv">
            <tr>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 15px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 15px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        Original data</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Output Power in kW's
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.OutputPowerOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No of Poles
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.NoOfPolesOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Core Length (mm)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.CoreLengthOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Core Diameter (mm)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.CoreDiameterOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No.of Current Paths
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.NoOfCurrentPathsOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Turns/Slot
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.TurnsOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Diameter
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.WireDiameterOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Area
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.WireAreaOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Max. Core Rating (kW's)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.MaxCoreRatingOriginal%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 35px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 35px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        New data</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Output Power in kW's
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.OutputPowerNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No.of Current Paths
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.NoOfCurrentPathsNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Turns
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.TurnsNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    New Area
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.WireAreaNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Diameter
                                </td>
                                <td>
                                    &nbsp;<%= CurrentPowerConversionInstance.WireDiameterNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <%
                   }
                   break;
               case CalculationToolType.CoilConversion:
                   if (CurrentCoilConversionInstance != null)
                   { 
        %>
        <table class="PrintJobBoxTrDiv">
            <tr>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 15px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 15px;">
                            <tr>
                                <td style="text-align: left;">
                                    Number of Poles
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.NoOfPoles%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Number of Slots
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.NoOfSlots%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Original Coils/Group
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilGroupOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Original Coil Sides/Slot
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSidesSlotOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Full Span
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.FullSpan%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Original Coil Type
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.WindingOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    New Coil Type
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.WindingNew%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 15px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 15px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        Original Coil Span and Turns</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Span
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanOriginal1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanOriginal2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanOriginal3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanOriginal4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanOriginal5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanOriginal6%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Turns
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsOriginal1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsOriginal2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsOriginal3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsOriginal4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsOriginal5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsOriginal6%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    CF
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFOriginal1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFOriginal2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFOriginal3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFOriginal4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFOriginal5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFOriginal6%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="PrintJobHeading">
                                        Effective Turns/Coil</p>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentCoilConversionInstance.EffectiveTurnsCoilOriginal%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 35px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 15px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        New Coil Span and Turns</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Span
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanNew1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanNew2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanNew3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanNew4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanNew5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilSpanNew6%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Turns
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsNew1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsNew2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsNew3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsNew4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsNew5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilTurnsNew6%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    CF
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFNew1%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFNew2%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFNew3%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFNew4%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFNew5%>
                                </td>
                                <td>
                                    &nbsp;<%= CurrentCoilConversionInstance.CoilCFNew6%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="PrintJobHeading">
                                        New Average Turns/Coil</p>
                                </td>
                                <td style="text-align: left;">
                                    &nbsp;<%= CurrentCoilConversionInstance.EffectiveTurnsCoilNew%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <%
                   }
                   break;
               case CalculationToolType.SpeedConversion:
                   if (CurrentSpeedConversionInstance != null)
                   {
        %>
        <table class="PrintJobBoxTrDiv">
            <tr>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 15px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 15px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        Original data</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    Output Power in kW's
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.OutputPowerOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No of Slots
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.NoOfSlotsOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No of Poles
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.NoOfPolesOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Core Length (mm)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.CoreLengthOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Core Diameter (mm)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.CoreDiameterOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No.of Current Paths
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.NoOfCurrentPathsOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Turns/Slot
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.TurnsSlotOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Old Span 1 -
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.SpanOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Diameter
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.WireDiameterOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Area
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.WireAreaOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Max Core Rating (kW's)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.MaxCoreRatingOriginal%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Turns/Slot at Full Span
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.TurnsSlotFullSpanOriginal%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td>
                    <div style="vertical-align: top; text-align: left; margin-left: 35px;">
                        <table class="PrintJobBoxTrDiv" style="vertical-align: top; text-align: left; margin-left: 35px;">
                            <tr>
                                <td style="text-align: left;">
                                    <p class="PrintJobHeading">
                                        New data</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Output Power in kW's
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.OutputPowerNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No of Poles
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.NoOfPolesNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Max Core Rating (KW's)
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.MaxCoreRatingNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    No of Current Paths
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.NoOfCurrentPathsNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Turns/Slot
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.TurnsSlotNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Area
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.AreaNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wire Diameter
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.WireDiameterNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Full Span 1 -
                                </td>
                                <td>
                                    &nbsp;<%= CurrentSpeedConversionInstance.FullSpanNew%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <%
                   }
                   break;
           } %>
        <div class="PrintJobBoxTrDiv">
            <table class="PrintJobBoxTrDiv">
                <tr>
                    <td style="text-align: left;">
                        <table style="text-align: left; width: 100%;">
                            <tr>
                                <td>
                                    <div class="PrintJobHeading">
                                        <asp:Label runat="server" ID="lblResultmessage" Text="Results" /></div>
                                </td>
                            </tr>
                        </table>
                        <asp:GridView ID="grdCalcResults" runat="server" AutoGenerateColumns="False" BorderColor="Transparent"
                            AllowPaging="False" ShowHeader="False">
                            <Columns>
                                <asp:TemplateField ShowHeader="False" ControlStyle-BorderWidth="0px">
                                    <ItemTemplate>
                                        <div>
                                            <asp:Label ID="lblMessage" runat="server" Text='<%# Container.DataItem %>' />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
