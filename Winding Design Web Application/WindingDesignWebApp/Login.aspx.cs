﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WindingDesign.BusinessLayer;

namespace WindingDesign.WebApp
{
    public partial class Login : PageBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoginButton_Click(object sender, EventArgs e)
        {
            DBHelper.LoginResponse cLoginResponse = ValidateCurrentClient(txtEmail.Text.Trim(), txtPassword.Text.Trim());

            switch (cLoginResponse)
            {
                case DBHelper.LoginResponse.LOGINSUCESS:
                    CurrentToolInstance.ResetCalculatorInstances();
                    HttpContext.Current.Session["CTInstance"] = null;
                    Response.Redirect("WireCalculator.aspx");
                    break;
                case DBHelper.LoginResponse.INCORRECTPASSWORD:
                    FailureText.Text = "Invalid password, please try again.";
                    break;
                case DBHelper.LoginResponse.EMAILDOESNOTEXISTS:
                    FailureText.Text = "Email does not exist, please create an account first.";
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPasswordReminder_Click(object sender, EventArgs e)
        {
            BusinessLayer.Client passRemind = DBHelper.ReturnClientByEmailAddress(txtReminderEmail.Text.Trim());

            if (passRemind != null)
            {
                string RecieversName = passRemind.Title + " " + passRemind.FirstName + " " + passRemind.LastName;
                SendEmail("Winding Design" + Environment.NewLine, RecieversName, "admin@windingdesign.co.uk", passRemind.EmailAddress, "Message from WindingDesign.co.uk", "Your password for your account with www.windingdesign.co.uk is: " + DBHelper.DecryptPassword(passRemind.EncryptedPassword, passRemind.Salt) + Environment.NewLine);
                FailureText.Text = "The password was sent to your registered email address.";
                txtReminderEmail.Text = "";
                txtReminderEmail.Focus();
            }
            else
            {
                FailureText.Text = "Sorry, your email does not exists in our system. Please create an account with us.";
            }
        }
    }
}
