﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace WindingDesign.WebApp
{
    public partial class Home : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AttachAlphaRestrictionToTextBoxes(Page.Controls);
                grdCalcResults.DataSource = new List<string> { "validation message 1", "validation message 2", "validation message 3", "validation message 4" };
                grdCalcResults.DataBind();
                grdVoltageConvResults.DataSource = new List<string> { "validation message 1", "validation message 2", "validation message 3", "validation message 4" };
                grdVoltageConvResults.DataBind();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        private void AttachAlphaRestrictionToTextBoxes(ControlCollection page)
        {
            foreach (Control c in page)
            {
                if (c.ID != null && c.GetType() == typeof(TextBox))
                {
                    ((TextBox)c).Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
                }

                if (c.HasControls())
                {
                    AttachAlphaRestrictionToTextBoxes(c.Controls);
                }
            }
        }
    }
}
