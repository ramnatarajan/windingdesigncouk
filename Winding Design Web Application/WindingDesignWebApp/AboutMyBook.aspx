﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paypal.Master" AutoEventWireup="true"
    CodeBehind="AboutMyBook.aspx.cs" Inherits="WindingDesign.WebApp.AboutMyBook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <table class="AboutUsLayout">
                <tr>
                    <td style="vertical-align: text-top; text-align: right;">
                        <div class="AboutUsLeft">
                            <img alt="Tony Ball" src="Images/Book.png" />
                        </div>
                    </td>
                    <td class="AboutUsRight">
                        <div class="AboutUsRight">
                            <h2>
                                About My Book</h2>
                            <p>
                                A handbook written by Tony Ball, Technical Author, has over 300 pages of Technical
                                and practical information. It has proven to be an invaluable aid for Maintenance
                                Engineers, Industrial Electricians, Students and others with engineering backgrounds.
                                Much time and effort has gone into researching, writing and proving the answers
                                to those grey areas in engineering.</p>
                            <h2>
                                Please read Carefully
                            </h2>
                            <p>
                                Should you have any technical queries, please email us and we will endeavour to
                                answer them quickly. We plan to add a forum soon which will enable you to talk online
                                with the author and other engineers. We think that this will be particularly useful
                                in time. If you choose to order a book, we hope you find it very useful and we would
                                appreciate any comments by email.</p>
                            <p>
                                For orders to another country or orders of more than one book to the same address,
                                please email for p & p charges. Price breaks start at orders of 10 or more.</p>
                            <p>
                                Please Note: The telephone payment phone number is not a technical helpline and
                                is not manned by technical staff. If you have any queries please email the author.</p>
                            <h2>
                                How To Order
                            </h2>
                            <span class="Mainbodycopy">Item: Motors, Control and Transmission<br />
                                Original UK price &pound;35 + P&amp;P
                                <br />
                                <span style="font-weight: bold">Promotional UK price: &pound;28.25 including P &amp;
                                    P</span></span><br />
                            <span style="font-weight: bold">Promotional European price: &pound;29.75 including P
                                &amp; P</span><br />
                            <span style="font-weight: bold">Promotional USA &amp; Non European price: &pound;33.50
                                including P &amp; P</span>
                            <p class="MaintextBlack">
                                <span class="Mainbodycopy">By Cheque: Send your full delivery address with a written
                                    order and include a cheque for the correct amount.</span></p>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="36%" align="left" valign="top">
                                        <p align="left">
                                            <span class="MaintextBlack"><span class="Mainbodycopy"><span style="font-weight: bold">
                                                Please send to: </span>
                                                <br />
                                                Engineering Publications Ltd.<br />
                                                Unit 4, Culverin Square<br />
                                                Limberline Rd, Hilsea,<br />
                                                Portsmouth, PO3 5BU</span><br />
                                                <br />
                                            </span>
                                        </p>
                                    </td>
                                    <td width="64%" align="left" valign="top">
                                        <p align="left">
                                            <span class="MaintextBlack">By phone:
                                                <br />
                                            </span><span class="Mainbodycopy">Between the hours of 07.00am until 07.00pm, Monday
                                                to Sunday:<br />
                                                Telephone - 07956 518797 (UK)<br />
                                                Telephone - (+44) 7956 518797 (non UK)</span></p>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="95%" height="60" align="center" valign="middle">
                                        <span class="MaintextBlack">PURCHASE ONLINE WITH PAYPAL: </span><span class="Mainbodycopy">
                                            <br />
                                            Please select a Buy Now button below and follow the steps when prompted.<br />
                                            You can select additional copies when you checkout through PayPal<br />
                                            <br />
                                        </span>
                                    </td>
                                    <td width="5%" align="center" valign="middle">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <WindingControls:PayPal runat="server" ID="ucPaypal" />
                            <p align="center" class="Maintext">
                                &nbsp;</p>
                            <p align="center" class="style6">
                                Order from another country please contact us.
                            </p>
                            <table height="60" border="0" cellpadding="0" cellspacing="10">
                                <tr>
                                    <td height="50" align="center" valign="middle" class="IntrotextBlue">
                                        Call us on 02392 660336 or email: <a href="mailto:tony@engineeringpublications.co.uk">
                                            <font color="#003399">tony@engineeringpublications.co.uk</font></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
