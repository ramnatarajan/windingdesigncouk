﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="VoltageConversion.aspx.cs" Inherits="WindingDesign.WebApp.VoltageConversion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: "h3", autoHeight: false });
            $('#HomeTabs').tabs();
        });

        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8 || key == 13) {
                keychar = String.fromCharCode(8);
            }
            return reg.test(keychar);
        }
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <asp:UpdatePanel ID="upVoltConv" runat="server">
                <ContentTemplate>
                    <table class="WireCalcTableTop">
                        <tr>
                            <td>
                                <div class="WireCalcJobDetails">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="text-align: left; width: 70%;">
                                                &nbsp;&nbsp;Converting Voltage Frequency and Current Paths
                                            </td>
                                            <td style="text-align: right; width: 30%;">
                                                Job Number&nbsp;
                                                <asp:TextBox ID="txtJobIdentificationNumber" Width="70px" CssClass="SiteTextBoxes"
                                                    runat="server" MaxLength="10" />
                                                &nbsp;
                                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="CalcButtons" OnClick="btnPrint_Click"
                                                    ToolTip="Please allow popups to use print functionality" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="WireCalcTable">
                        <tr>
                            <td>
                                <div class="ConvCalcLeft">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    Original Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODSupplyVoltage" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Full Load Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODFullLoadCurrent" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Phase Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODPhaseVoltage" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Phase Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODPhaseCurrent" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Connection
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODConnection" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Frequency
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODSupplyFrequency" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power (kW's)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOutputPower" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Turns
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalTurns" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Wire Dia.
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalWireDia" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No. of Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalNoOfPaths" Width="80px" CssClass="SiteTextBoxes"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Full Load Speed
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalFLSpeed" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Cooling
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCODOriginalCooling" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="vertical-align: text-top;">
                                <div class="PowerCalcMiddle">
                                    <div class="ConvMiddleTop">
                                        <table class="ConvMiddleTable">
                                            <tr>
                                                <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                    <div class="WireCalcOldDataTableLabel">
                                                        Conversion Type</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="ConvMiddleradiolist">
                                                        <asp:RadioButtonList ID="rdoListConvType" AutoPostBack="true" CssClass="ConvMiddleradiolist"
                                                            runat="server" OnSelectedIndexChanged="rdoListConvType_SelectedIndexChanged">
                                                            <asp:ListItem Selected="true">Star to Star</asp:ListItem>
                                                            <asp:ListItem>Star to Delta</asp:ListItem>
                                                            <asp:ListItem>Delta to Star</asp:ListItem>
                                                            <asp:ListItem>Delta to Delta</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnVCCalcAll" CssClass="CalcButtons" runat="server" Text="Calc All"
                                                        OnClick="btnVCCalcAll_Click" />
                                                    &nbsp;<asp:Button ID="btnVCClearAll" CssClass="CalcButtons" runat="server" Text="Clear All"
                                                        OnClick="btnVCClearAll_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="ConvMiddleBottom">
                                        <table style="text-align: center; width: 100%;">
                                            <tr>
                                                <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                    <div class="WireCalcOldDataTableLabel">
                                                        <asp:Label runat="server" ID="lblResultmessage" Text="Results" /></div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="grdVoltageConvResults" runat="server" AutoGenerateColumns="False"
                                            BorderColor="Transparent" AllowPaging="False" ShowHeader="False">
                                            <Columns>
                                                <asp:TemplateField ShowHeader="False" ControlStyle-BorderWidth="0px">
                                                    <ItemTemplate>
                                                        <div class="ResultValidationMessage">
                                                            <asp:Label ID="lblMessage" runat="server" Text='<%# Container.DataItem %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="ConvCalcRight">
                                    <table class="WireCalcOldDataTable">
                                        <tr>
                                            <td class="WireCalcOldDataTableLabel" style="height: 25px;">
                                                <div class="WireCalcOldDataTableLabel">
                                                    New Data</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="WireCalcOldDataTableText">
                                                <table class="WireCalcOldDataInnerTable">
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDSupplyVoltage" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Approx. Full Load Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewLoadCurrent" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Phase Voltage
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDPhaseVoltage" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Approx. Phase Current
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDPhaseCurrent" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Connection
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDConnection" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Supply Frequency
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDSupplyFrequency" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Output Power (kW's)
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDOutputPower" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Turns
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewTurns" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Wire Dia.
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewWireDia" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            No. of Paths
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewNoOfPaths" Width="80px" CssClass="SiteTextBoxes" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Full Load Speed
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDNewFLSpeed" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ConvCalcLeftTableTd1">
                                                            Change in Cooling
                                                        </td>
                                                        <td class="ConvCalcLeftTableTd2">
                                                            <asp:TextBox ID="txtVCNDChangeInCooling" Width="80px" ReadOnly="true" CssClass="SiteTextBoxesPink"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
