﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="WireDesignEdit.aspx.cs" Inherits="WindingDesign.WebApp.WireDesignEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#HomeTabs').tabs();
        });
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <table class="WireCalcTableTop">
                <tr>
                    <td>
                        <div class="WireCalcJobDetails">
                            <table style="width: 100%; height: 100%">
                                <tr>
                                    <td class="WireCalcOldDataTableLabel" style="text-align: left; height: 28px; width: 100%;">
                                        &nbsp;&nbsp;<asp:Label ID="lblEditDesignTitle" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="WireDesignEditTable">
                <table class="WireDesignEditTable">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="35%" style="text-align: right; vertical-align: middle;">
                            <asp:Label ID="lblReq1" CssClass="RequiredField" runat="server" Text="*" />
                            Title
                        </td>
                        <td>
                            &nbsp;&nbsp;<asp:TextBox ID="txtTitle" Width="254px" ValidationGroup="EditDesign"
                                CssClass="SiteTextBoxes" runat="server" MaxLength="250" />
                        </td>
                        <td style="text-align: left;">
                            <asp:RequiredFieldValidator ID="vldTitle" runat="server" ValidationGroup="EditDesign"
                                CssClass="RequiredField" ErrorMessage="Title is required" ControlToValidate="txtTitle" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; vertical-align: middle;">
                            <asp:Label ID="Label1" CssClass="RequiredField" runat="server" Text="*" />
                            Description
                        </td>
                        <td width="30%">
                            &nbsp;&nbsp;<asp:TextBox ID="txtDescription" Width="250px" ValidationGroup="EditDesign"
                                TextMode="MultiLine" CssClass="MultilineTextBox" runat="server" MaxLength="500" />
                        </td>
                        <td style="text-align: left;">
                            <asp:RequiredFieldValidator ID="vldDescription" runat="server" ValidationGroup="EditDesign"
                                CssClass="RequiredField" ErrorMessage="Description is required" ControlToValidate="txtDescription" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; vertical-align: middle;">
                            <asp:Label ID="Label2" CssClass="RequiredField" runat="server" Text="*" />
                            Design Type
                        </td>
                        <td width="30%">
                            &nbsp;&nbsp;<asp:DropDownList ID="lstDesignTypes" Width="250" CssClass="textEntry"
                                        runat="server" />
                        </td>
                        <td style="text-align: left;">
                            <asp:RequiredFieldValidator ID="vldReqDesignType" runat="server" ValidationGroup="EditDesign"
                                CssClass="RequiredField" ErrorMessage="Design Type is required" ControlToValidate="lstDesignTypes" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <div style="margin-left: 10px; margin-top: 10px;">
                                <asp:FileUpload ID="fluImageUploader" runat="server" ValidationGroup="EditDesign"
                                    class="btn btn-success fileinput-button" Width="235px" />
                            </div>
                        </td>
                        <td style="text-align: left;">
                            <asp:RequiredFieldValidator ID="vldUploadImage" runat="server" CssClass="RequiredField"
                                ErrorMessage="Choose a Image" ValidationGroup="EditDesign" ControlToValidate="fluImageUploader" />
                            <asp:RegularExpressionValidator ID="regexValidator" runat="server" CssClass="RequiredField" ControlToValidate="fluImageUploader"
                                ErrorMessage="Only JPEG images are allowed" ValidationGroup="EditDesign" ValidationExpression="(.*\.([Jj][Pp][Gg])|.*\.([Jj][Pp][Ee][Gg])$)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <div style="margin-left: 10px; margin-top: 10px;">
                                <asp:Button ID="btnUploadImage" class="btn btn-success fileinput-button" ValidationGroup="EditDesign"
                                    runat="server" Text="Upload" OnClick="btnUploadImage_Click" />
                                <% if (CurrentWireDesignID != 0)
                                   { %>
                                <asp:Button ID="btnDelete" class="btn btn-success fileinput-button" runat="server"
                                    Text="Delete" OnClick="btnDelete_Click" />
                                <%} %>
                                <asp:Button ID="btnCancel" class="btn btn-success fileinput-button"
                                    runat="server" Text="Cancel" onclick="btnCancel_Click" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
