﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Register.aspx.cs" Inherits="WindingDesign.WebApp.Register" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#HomeTabs').tabs();
        });
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <table class="WireCalcTableTop">
                <tr>
                    <td>
                        <div class="WireCalcJobDetails">
                            <table style="width: 100%; height: 100%">
                                <tr>
                                    <td class="WireCalcOldDataTableLabel" style="text-align: left; height: 28px; width: 100%;">
                                        &nbsp;&nbsp;<asp:Label ID="lblTitle" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <span class="failureNotification RequiredField" style="text-align: left;">
                <asp:Literal ID="ErrorMessage" runat="server" />
            </span>
            <div style="text-align: left;">
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification RequiredField"
                    ValidationGroup="RegisterUserValidationGroup" />
            </div>
            <div class="accountInfo">
                <fieldset class="register">
                    <legend>Account Information</legend>
                    <div class="WireDesignEditTable">
                        <table class="WireDesignEditTable">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span><asp:Label ID="lblName" CssClass="SiteLabels"
                                        runat="server" Text="Name (Title|First|Last)" />
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:DropDownList ID="lstTitle" Width="70px" CssClass="SiteTextBoxes"
                                        runat="server">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>Mr</asp:ListItem>
                                        <asp:ListItem>Miss</asp:ListItem>
                                        <asp:ListItem>Mrs</asp:ListItem>
                                        <asp:ListItem>Dr</asp:ListItem>
                                        <asp:ListItem>Others</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtFirstName" Width="110px" CssClass="SiteTextBoxes" runat="server"
                                        MaxLength="30" />
                                    <asp:TextBox ID="txtLastName" Width="110px" CssClass="SiteTextBoxes" runat="server"
                                        MaxLength="30" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="lstTitle"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Title is required."
                                        ToolTip="Title is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFirstName"
                                        CssClass="failureNotification RequiredField" ErrorMessage="First Name is required."
                                        ToolTip="First Name is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtLastName"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Last Name is required."
                                        ToolTip="Last Name is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    Company Name
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtCompanyName" runat="server" Width="200" CssClass="textEntry"
                                        MaxLength="250" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    VAT Number
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtVATNumber" Width="200" runat="server" CssClass="textEntry"
                                        MaxLength="15" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>E-mail
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtEmail" runat="server" Width="200" CssClass="textEntry"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="txtEmail"
                                        CssClass="failureNotification RequiredField" ErrorMessage="E-mail is required."
                                        ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="vldEmail" CssClass="failureNotification RequiredField"
                                        runat="server" ErrorMessage="Not a valid email address." ToolTip="Not a valid email address."
                                        ValidationExpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"
                                        ControlToValidate="txtEmail" ValidationGroup="RegisterUserValidationGroup">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>Password
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtPassword" runat="server" Width="200" CssClass="passwordEntry"
                                        TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="txtPassword"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Password is required."
                                        ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>Confirm Password
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtConfirmPassword" runat="server" Width="200" CssClass="passwordEntry"
                                        TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="txtConfirmPassword" CssClass="failureNotification RequiredField"
                                        Display="Dynamic" ErrorMessage="Confirm Password is required." ID="ConfirmPasswordRequired"
                                        runat="server" ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="txtPassword"
                                        ControlToValidate="txtConfirmPassword" CssClass="failureNotification RequiredField"
                                        Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                        ValidationGroup="RegisterUserValidationGroup">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>Address
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtAddressLine1" Width="200" runat="server" CssClass="textEntry"
                                        MaxLength="150" />
                                    <asp:RequiredFieldValidator ID="vldReqAddressLine1" runat="server" ControlToValidate="txtAddressLine1"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Address is required."
                                        ToolTip="Address is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtAddressLine2" Width="200" runat="server" CssClass="textEntry"
                                        MaxLength="150" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>County
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtCounty" runat="server" Width="200" CssClass="textEntry"
                                        MaxLength="150" />
                                    <asp:RequiredFieldValidator ID="vldReqCounty" runat="server" ControlToValidate="txtCounty"
                                        CssClass="failureNotification RequiredField" ErrorMessage="County is required."
                                        ToolTip="County is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>Post/Zip Code
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtPostCode" Width="200" runat="server" CssClass="textEntry"
                                        MaxLength="15" />
                                    <asp:RequiredFieldValidator ID="vldReqPostCode" runat="server" ControlToValidate="txtPostCode"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Post/Zip Code is required."
                                        ToolTip="Post/Zip Code is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>Country
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:DropDownList ID="lstCountries" Width="205" CssClass="textEntry"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="vldReqCountries" runat="server" ControlToValidate="lstCountries"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Country is required."
                                        ToolTip="Country is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>Telephone Number
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtTelephone" Width="200" runat="server" CssClass="textEntry"
                                        MaxLength="30" />
                                    <asp:RequiredFieldValidator ID="vldReqTelephone" runat="server" ControlToValidate="txtTelephone"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Telephone Number is required."
                                        ToolTip="Telephone Number is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    Mobile Number
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtMobile" Width="200" runat="server" CssClass="textEntry"
                                        MaxLength="30" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<img id="imgCaptcha" src="../GenerateCaptchaImage.aspx" width="200px" alt="Captch Image" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    Please type in the text from the image
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtCaptcha" Width="200" runat="server" CssClass="textEntry"
                                        MaxLength="30" />
                                    <asp:RequiredFieldValidator ID="vldReqCaptcha" runat="server" ControlToValidate="txtCaptcha"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Please type in the text from the image."
                                        ToolTip="Please type in the text from the image." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
                <p class="submitButton" style="text-align: left;">
                    <asp:Button ID="btnCreateUserButton" runat="server" CssClass="CalcButtons" CommandName="MoveNext"
                        ValidationGroup="RegisterUserValidationGroup" OnClick="btnCreateUserButton_Click" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>
