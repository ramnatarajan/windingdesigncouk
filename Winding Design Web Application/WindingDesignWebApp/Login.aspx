﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="WindingDesign.WebApp.Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#HomeTabs').tabs();
            var dlg = $('#dialog').dialog({
                autoOpen: false,
                width: 500,
                modal: true
            });

            var discDlg = $('#disclaimerDialog').dialog({
                autoOpen: false,
                width: 500,
                modal: true
            });

            $('#dialog_link').click(function () {
                $('#dialog').dialog('open');
                return false;
            });

            $("input[id$='LoginButton']").click(function () {
                $('#disclaimerDialog').dialog('open');
                return false;
            });

            discDlg.parent().appendTo(jQuery("form:first"));
            dlg.parent().appendTo(jQuery("form:first"));            
        });
    </script>
    <div id="HomeTabs">
        <div id="HTab" class="TabMinHeightDefaults">
            <table class="WireCalcTableTop">
                <tr>
                    <td>
                        <div class="WireCalcJobDetails">
                            <table style="width: 100%; height: 100%">
                                <tr>
                                    <td class="WireCalcOldDataTableLabel" style="text-align: left; height: 28px; width: 100%;">
                                        &nbsp;&nbsp;Log In
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <p style="text-align: left;">
                Please enter your email and password.
                <asp:HyperLink ID="RegisterHyperLink" runat="server" EnableViewState="false">Register</asp:HyperLink>
                if you don't have an account.
            </p>
            <span class="failureNotification RequiredField">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification RequiredField"
                ValidationGroup="LoginUserValidationGroup" />
            <div class="accountInfo">
                <fieldset class="login">
                    <legend>Account Information</legend>
                    <div class="WireDesignEditTable">
                        <table class="WireDesignEditTable">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>E-mail
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtEmail" runat="server" Width="200" CssClass="textEntry"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="txtEmail"
                                        CssClass="failureNotification RequiredField" ErrorMessage="E-mail is required."
                                        ToolTip="E-mail is required." ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="vldEmail" CssClass="failureNotification RequiredField"
                                        runat="server" ErrorMessage="Not a valid email address." ToolTip="Not a valid email address."
                                        ValidationExpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"
                                        ControlToValidate="txtEmail" ValidationGroup="LoginUserValidationGroup">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%" style="text-align: right; vertical-align: middle;">
                                    <span class="RequiredField">*&nbsp;</span>Password
                                </td>
                                <td style="text-align: left">
                                    &nbsp;&nbsp;<asp:TextBox ID="txtPassword" runat="server" Width="200" CssClass="passwordEntry"
                                        TextMode="Password" />
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="txtPassword"
                                        CssClass="failureNotification RequiredField" ErrorMessage="Password is required."
                                        ToolTip="Password is required." ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
                <p class="submitButton" style="text-align: left;">
                    <asp:Button ID="LoginButton" runat="server" CssClass="CalcButtons" CommandName="Login"
                        Text="Log In" ValidationGroup="LoginUserValidationGroup" />
                    <input type="button" value="Password Reminder" id="dialog_link" class="CalcButtons" />
                </p>
                <div id="disclaimerDialog" title="Disclaimer" style="text-align: justify; font-size: 13px;">
                    <div class="accountInfo">
                        <fieldset class="disclaimer">
                            &nbsp;&nbsp;&nbsp;&nbsp;Although every effort has been made to ensure that the calculators
                            and information on this site are accurate, the author cannot take responsibility
                            for any errors in using it. It is the responsibility of the engineer to thoroughly
                            test the new motor windings to ensure they are satisfactory on completion and this
                            may include load testing when the motor is back in situ. In some areas the engineer
                            is also responsible for making decisions such as the levels of flux density, output
                            load speed, temperature rise etc. It is advisable that the motor windings are secured
                            and the motor is tested before applying varnish or resin to the windings (in the
                            white).<br />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;Last but not least, only change a winding if you absolutely
                            have to. The original designer (in most if not all cases) is the one that knew best.
                            <div style="text-align: left;">
                                <asp:ValidationSummary ID="DisclaimerValidationSummary" runat="server" CssClass="failureNotification RequiredField"
                                    ValidationGroup="LoginUserValidationGroup" />
                            </div>
                        </fieldset>
                        <p style="text-align: center;">
                            <asp:Button ID="btnLogInRedirect" runat="server" Text="Ok" CssClass="CalcButtons"
                                OnClick="LoginButton_Click" />
                        </p>
                    </div>
                </div>
                <div id="dialog" title="Password Reminder">
                    <span class="failureNotification RequiredField">
                        <asp:Literal ID="PasswordReminderErrorMessage" runat="server"></asp:Literal>
                    </span>
                    <div style="text-align: left;">
                        <asp:ValidationSummary ID="PasswordValidationSummary" runat="server" CssClass="failureNotification RequiredField"
                            ValidationGroup="PasswordValidationGroup" />
                    </div>
                    <div class="accountInfo">
                        <fieldset class="register">
                            <div class="WireDesignEditTable">
                                <table class="WireDesignEditTable">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="35%" style="text-align: right; vertical-align: middle;">
                                            <span class="RequiredField">*&nbsp;</span>E-mail
                                        </td>
                                        <td style="text-align: left">
                                            &nbsp;&nbsp;<asp:TextBox ID="txtReminderEmail" runat="server" Width="200" CssClass="textEntry"
                                                Font-Size="Small" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReminderEmail"
                                                CssClass="failureNotification RequiredField" ErrorMessage="E-mail is required."
                                                ToolTip="E-mail is required." ValidationGroup="PasswordValidationGroup">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="failureNotification RequiredField"
                                                runat="server" ErrorMessage="Not a valid email address." ToolTip="Not a valid email address."
                                                ValidationExpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"
                                                ControlToValidate="txtReminderEmail" ValidationGroup="PasswordValidationGroup">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </fieldset>
                        <p style="text-align: center;">
                            <asp:Button ID="btnPasswordReminder" runat="server" Text="Send" CssClass="CalcButtons"
                                CommandName="MoveNext" ValidationGroup="PasswordValidationGroup" OnClick="btnPasswordReminder_Click" />
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
