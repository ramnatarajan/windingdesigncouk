﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Objects;
using System.Data.EntityClient;
using WindingDesign.BusinessLayer;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Reflection;

namespace WindingDesign.WebApp
{
    /// <summary>
    /// 
    /// </summary>
    public class Emailer
    {
        private string _sendersName = string.Empty;
        private string _recieversName = string.Empty;
        private string _from = string.Empty;
        private string _to = string.Empty;
        private string _subject = string.Empty;
        private string _body = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string SendersName
        {
            get
            { return _sendersName; }
            set
            { _sendersName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string RecieversName
        {
            get
            { return _recieversName; }
            set
            { _recieversName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string From
        {
            get
            { return _from; }
            set
            { _from = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string To
        {
            get
            { return _to; }
            set
            { _to = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Subject
        {
            get
            { return _subject; }
            set
            { _subject = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Body
        {
            get
            { return _body; }
            set
            { _body = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_from"></param>
        /// <param name="_to"></param>
        /// <param name="_subject"></param>
        /// <param name="_body"></param>
        /// <returns></returns>
        public string ThreadSendEmail()
        {
            try
            {
                System.Net.Mail.SmtpClient smtp = new SmtpClient("windingdesign.co.uk", 25);
                smtp.EnableSsl = false;
                NetworkCredential cred = new NetworkCredential("admin@windingdesign.co.uk", "solent123");
                System.Net.Mail.MailMessage msg = new MailMessage(From, To, Subject, ReturnMessageBody(SendersName, RecieversName, Body));
                smtp.Credentials = cred;
                msg.IsBodyHtml = true;
                smtp.Send(msg);
                msg.Dispose();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SendersName"></param>
        /// <param name="RecieversName"></param>
        /// <param name="MsgBody"></param>
        /// <returns></returns>
        private string ReturnMessageBody(string SendersName, string RecieversName, string MsgBody)
        {
            StringBuilder _messageBody = new StringBuilder();
            _messageBody.Append("Hi " + RecieversName + ",<br />");
            _messageBody.Append("<br />");
            _messageBody.Append(MsgBody + Environment.NewLine);
            _messageBody.Append("<br /><br />Thank You,<br />" + SendersName + "<br />" + Environment.NewLine);
            _messageBody.Append("www.WindingDesign.co.uk");
            _messageBody.Append("<br /><br />-----------------------<br>");
            _messageBody.Append("This is a automated message. Please do not reply to this email.");
            return _messageBody.ToString();
        }
    }
}
