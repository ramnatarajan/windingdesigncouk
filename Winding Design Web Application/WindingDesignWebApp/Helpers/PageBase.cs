﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WindingDesign.BusinessLayer;
using System.Xml;
using System.Threading;
using System.Reflection;

namespace WindingDesign.WebApp
{
    public class PageBase : System.Web.UI.Page
    {
        /// <summary>
        /// 
        /// </summary>
        private ClientDetail currentClient;

        /// <summary>
        /// 
        /// </summary>
        private List<WireDesign> designSource;

        /// <summary>
        /// 
        /// </summary>
        private List<TrainingVideo> trainingVideoSource;

        /// <summary>
        /// 
        /// </summary>
        private List<DesignType> designTypeSource;

        /// <summary>
        /// 
        /// </summary>
        private DesignType currentDesignType;

        /// <summary>
        /// 
        /// </summary>
        private List<string> countriesList;

        /// <summary>
        /// 
        /// </summary>
        public ImageHelper ImageTool = new ImageHelper();

        /// <summary>
        /// 
        /// </summary>
        private WindingDesignEntities currentEntityContext;

        /// <summary>
        /// 
        /// </summary>
        public enum CalculationToolType
        {
            [StringValue("WireCalculator")]
            WireCalculator = 1,

            [StringValue("VoltageConversion")]
            VoltageConversion = 2,

            [StringValue("PowerConversion")]
            PowerConversion = 3,

            [StringValue("CoilConversion")]
            CoilConversion = 4,

            [StringValue("SpeedConversion")]
            SpeedConversion = 5
        }

        /// <summary>
        /// Gets the alpha numeric text boxes.
        /// </summary>
        /// <value>
        /// The alpha numeric text boxes.
        /// </value>
        public List<string> AlphaNumericTextBoxes
        {
            get
            {
                return new List<string>
                {
                    "txtjobidentificationnumber"
                };
            }
        }

        /// <summary>
        /// Gets the tool tip skip list.
        /// </summary>
        /// <value>
        /// The tool tip skip list.
        /// </value>
        public List<string> ToolTipSkipList
        {
            get
            {
                return new List<string>
                {
                    "txtpcodwirediameter",
                    "txtpassword"
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public CalculationToolType CurrentToolType
        {
            get
            {
                return EnumHelper.GetToolTypeEnumValue((Request.QueryString["CalcType"] != null && Request.QueryString["CalcType"].ToString() != string.Empty ? Request.QueryString["CalcType"].ToString() : string.Empty));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static BusinessLayer.WindingDesignInstance CurrentToolInstance
        {
            get
            {
                WindingDesignInstance cInstance = HttpContext.Current.Session["CTInstance"] as WindingDesignInstance;

                if (cInstance == null)
                {
                    cInstance = new WindingDesignInstance();
                    cInstance.WireCalculator = new WireCalculatorInstance();
                    cInstance.VoltageConversion = new VoltageConversionInstance();
                    cInstance.PowerConversion = new PowerConversionInstance();
                    cInstance.CoilConversion = new CoilConversionInstance();
                    cInstance.SpeedConversion = new SpeedConversionInstance();

                    HttpContext.Current.Session["CTInstance"] = cInstance;
                }

                return cInstance;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public WindingDesignEntities CurrentEntityContext
        {
            get
            {
                if (currentEntityContext == null)
                {
                    currentEntityContext = WindingDesignObjectFactory.GetObjectContext<WindingDesignEntities>();
                }

                return currentEntityContext;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ClientDetail CurrentClient
        {
            get
            {
                int cClientID = Session["CurrentClientID"] != null && Session["CurrentClientID"].ToString() != string.Empty ? Convert.ToInt32(Session["CurrentClientID"].ToString()) : 0;

                if (cClientID != 0 && currentClient == null)
                {
                    BusinessLayer.Client cClient = (from usrs in CurrentEntityContext.Clients where usrs.ClientID == cClientID select usrs).FirstOrDefault();

                    if (cClient != null)
                    {
                        currentClient = new ClientDetail
                        {
                            ClientID = cClient.ClientID,
                            Title = cClient.Title,
                            FirstName = cClient.FirstName,
                            LastName = cClient.LastName,
                            EmailAddress = cClient.EmailAddress,
                            CompanyName = cClient.CompanyName,
                            VATNumber = cClient.VATNumber,
                            AddressLine1 = cClient.AddressLine1,
                            AddressLine2 = cClient.AddressLine2,
                            County = cClient.County,
                            PostCode = cClient.PostCode,
                            Country = cClient.Country,
                            TelephoneNumber = cClient.TelephoneNumber,
                            MobileNumber = cClient.MobileNumber,
                            Salt = cClient.Salt,
                            EncryptedPassword = cClient.EncryptedPassword,
                            RegisteredDate = cClient.RegisteredDate,
                            LastModifiedDate = cClient.LastModifiedDate,
                            LicenceExpiryDate = cClient.LicenceExpiryDate,
                            IsLocked = cClient.IsLocked,
                            IsAdmin = cClient.IsAdmin
                        };
                    }
                }

                return currentClient;
            }

            set
            {
                currentClient = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CurrentTypeID
        {
            get
            {
                if (Request.QueryString["Type"] != null && Request.QueryString["Type"].ToString() != "")
                {
                    return Convert.ToInt32(Request.QueryString["Type"].ToString());
                }

                return 1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DesignType CurrentDesignType
        {
            get
            {
                currentDesignType = (from types in CurrentEntityContext.DesignTypes where types.DesignTypeID == CurrentTypeID select types).FirstOrDefault();

                return currentDesignType;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<WireDesign> DesignSource
        {
            get
            {
                if (designSource == null || (designSource != null && designSource.Count == 0))
                {
                    designSource = (from designs in CurrentEntityContext.WireDesigns
                                    where designs.DesignType.DesignTypeID == CurrentTypeID
                                    select new WireDesign
                                    {
                                        DesignID = designs.WireDesignID,
                                        ShortDescription = designs.Title,
                                        LongDescription = designs.Description,
                                        ThumbnailPath = designs.ThumbnailPath,
                                        ImagePath = designs.ImagePath,
                                        OriginalImagePath = designs.OriginalImagePath
                                    }).ToList();

                }

                return designSource;
            }

            set
            {
                designSource = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<TrainingVideo> TrainingVideoSource
        {
            get
            {
                if (trainingVideoSource == null || (trainingVideoSource != null && trainingVideoSource.Count == 0))
                {
                    trainingVideoSource = (from videos in CurrentEntityContext.TrainingVideos
                                           select new TrainingVideo
                                           {
                                               TrainingVideoID = videos.TrainingVideoID,
                                               ShortDescription = videos.ShortDescription,
                                               LongDescription = videos.LongDescription,
                                               VideoEmbedPath = videos.VideoEmbedPath
                                           }).ToList();

                }

                return trainingVideoSource;
            }

            set
            {
                trainingVideoSource = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<DesignType> DesignTypeSource
        {
            get
            {
                if (designTypeSource == null || (designTypeSource != null && designTypeSource.Count == 0))
                {
                    designTypeSource = (from designTypes in CurrentEntityContext.DesignTypes select designTypes).ToList();

                }

                return designTypeSource;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<string> CountriesList
        {
            get
            {
                if (countriesList == null || (countriesList != null && countriesList.Count == 0))
                {
                    countriesList = new List<string>();
                    XmlDocument cList = new XmlDocument();
                    cList.Load(HttpContext.Current.Server.MapPath("~/XMLs/Countries.xml"));
                    XmlNodeList countryList = cList.SelectNodes("Countries/Country");

                    foreach (XmlNode cCountry in countryList)
                    {
                        countriesList.Add(cCountry.InnerXml);
                    }
                }

                return countriesList;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        public void AttachAlphaRestrictionToTextBoxes(ControlCollection page)
        {
            foreach (Control c in page)
            {
                if (c != null && c.ID != null && c.GetType() == typeof(TextBox) && !AlphaNumericTextBoxes.Contains(c.ID.ToLower()))
                {
                    ((TextBox)c).Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
                }

                if (c.HasControls())
                {
                    AttachAlphaRestrictionToTextBoxes(c.Controls);
                }
            }
        }

        /// <summary>
        /// Sets the max length and tool tip to text boxes.
        /// </summary>
        /// <param name="page">The page.</param>
        public void SetMaxLengthAndToolTipToTextBoxes(ControlCollection page)
        {
            foreach (Control c in page)
            {
                if (c != null && c.ID != null && c.GetType() == typeof(TextBox) && !ToolTipSkipList.Contains(c.ID.ToLower()))
                {
                    ((TextBox)c).ToolTip = ((TextBox)c).Text;
                    ((TextBox)c).MaxLength = 10;
                }

                if (c.HasControls())
                {
                    SetMaxLengthAndToolTipToTextBoxes(c.Controls);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddEditClient(int cClientID, string Title, string FirstName, string LastName, string EmailAddress, string CompanyName, string VATNumber, string AddressLine1, string AddressLine2, string County, string Country, string PostCode, string TelephoneNumber, string MobileNumber, string Password)
        {
            BusinessLayer.Client curClient = null;

            if (cClientID != 0)
            {
                curClient = (from usrs in CurrentEntityContext.Clients where usrs.ClientID == cClientID select usrs).FirstOrDefault();
            }
            else
            {
                curClient = new Client();
            }

            curClient.Title = Title;
            curClient.FirstName = FirstName;
            curClient.LastName = LastName;
            curClient.EmailAddress = EmailAddress;
            curClient.CompanyName = CompanyName;
            curClient.VATNumber = VATNumber;
            curClient.AddressLine1 = AddressLine1;
            curClient.AddressLine2 = AddressLine2;
            curClient.County = County;
            curClient.Country = Country;
            curClient.PostCode = PostCode;
            curClient.TelephoneNumber = TelephoneNumber;
            curClient.MobileNumber = MobileNumber;
            curClient.LastModifiedDate = DateTime.Now;

            if (cClientID == 0)
            {
                curClient.RegisteredDate = DateTime.Now;
                curClient.LicenceExpiryDate = DateTime.Now.AddMonths(1);
                curClient.Salt = Guid.NewGuid().ToString().Replace("-", "");
                curClient.EncryptedPassword = DBHelper.EncryptPassword(Password, curClient.Salt);
                CurrentEntityContext.AddToClients(curClient);
            }
            else
            {
                curClient.EncryptedPassword = DBHelper.EncryptPassword(Password, CurrentClient.Salt);
            }

            CurrentEntityContext.SaveChanges();

            Session["CurrentClientID"] = curClient.ClientID.ToString();
            currentClient = null;
        }

        /// <summary>
        /// Extends the license.
        /// </summary>
        /// <param name="clientID">The client ID.</param>
        public void ExtendLicense(int clientID)
        {
            if (clientID != 0)
            {
                BusinessLayer.Client curClient = (from usrs in CurrentEntityContext.Clients where usrs.ClientID == clientID select usrs).FirstOrDefault();

                if (curClient != null)
                {
                    DateTime licenseExtendedDate = DateTime.Now;

                    if (curClient.LicenceExpiryDate.HasValue && curClient.LicenceExpiryDate.Value > DateTime.Now)
                    {
                        licenseExtendedDate = curClient.LicenceExpiryDate.Value;
                    }

                    curClient.LicenceExpiryDate = licenseExtendedDate.AddYears(1);
                    CurrentEntityContext.SaveChanges();
                }
            }

            Session["CurrentClientID"] = clientID.ToString();
            currentClient = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public BusinessLayer.DBHelper.LoginResponse ValidateCurrentClient(string EmailAddress, string Password)
        {
            BusinessLayer.DBHelper.LoginResponse ReturnResponse = BusinessLayer.DBHelper.LoginResponse.EMAILDOESNOTEXISTS;
            BusinessLayer.Client cClient = BusinessLayer.DBHelper.ReturnClientByEmailAddress(EmailAddress);

            if (cClient != null && cClient.EncryptedPassword == BusinessLayer.DBHelper.EncryptPassword(Password, cClient.Salt))
            {
                Session["CurrentClientID"] = cClient.ClientID.ToString();
                ReturnResponse = BusinessLayer.DBHelper.LoginResponse.LOGINSUCESS;
            }
            else if (cClient != null)
            {
                ReturnResponse = BusinessLayer.DBHelper.LoginResponse.INCORRECTPASSWORD;
            }

            return ReturnResponse;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ClearSession"></param>
        public void LogClientOut(bool ClearSession)
        {
            if (ClearSession)
            {
                Session["CurrentClientID"] = string.Empty;
                Session.Clear();
            }

            currentClient = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DesignID"></param>
        /// <param name="ShortDescription"></param>
        /// <param name="LongDescription"></param>
        /// <param name="ImageFile"></param>
        /// <param name="TypeID"></param>
        public void AddEditWireDesign(int? DesignID, string ShortDescription, string LongDescription, HttpPostedFile ImageFile, int TypeID)
        {
            if (ImageFile != null)
            {
                BusinessLayer.WireDesign newDesign = null;

                if (DesignID.HasValue && DesignID != 0)
                {
                    newDesign = (from wd in CurrentEntityContext.WireDesigns where wd.WireDesignID == DesignID.Value select wd).FirstOrDefault();
                }
                else
                {
                    newDesign = new BusinessLayer.WireDesign();
                }

                newDesign.Title = ShortDescription;
                newDesign.Description = LongDescription;
                newDesign.DesignType = (from dt in CurrentEntityContext.DesignTypes where dt.DesignTypeID == TypeID select dt).FirstOrDefault();

                if (DesignID == null || (DesignID.HasValue && DesignID.Value == 0))
                {
                    CurrentEntityContext.AddToWireDesigns(newDesign);
                    CurrentEntityContext.SaveChanges();
                }

                if (newDesign != null && newDesign.WireDesignID != 0)
                {
                    ImageTool.CreateWireDesignImages(ImageFile, newDesign.WireDesignID);
                    newDesign.ThumbnailPath = "/Images/WireDesigns/Thumbnails/" + newDesign.WireDesignID.ToString() + ".jpg";
                    newDesign.ImagePath = "/Images/WireDesigns/" + newDesign.WireDesignID.ToString() + ".jpg";
                    newDesign.OriginalImagePath = "/Images/WireDesigns/Prints/" + newDesign.WireDesignID.ToString() + ".jpg";
                }

                CurrentEntityContext.SaveChanges();

                DesignSource = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TrainingVideoID"></param>
        /// <param name="ShortDescription"></param>
        /// <param name="LongDescription"></param>
        /// <param name="VideoEmbedPath"></param>
        public void AddEditTrainingVideo(int? TrainingVideoID, string ShortDescription, string LongDescription, string VideoEmbedPath)
        {
            BusinessLayer.TrainingVideo newVideo = null;

            if (TrainingVideoID.HasValue && TrainingVideoID != 0)
            {
                newVideo = (from tv in CurrentEntityContext.TrainingVideos where tv.TrainingVideoID == TrainingVideoID.Value select tv).FirstOrDefault();
            }
            else
            {
                newVideo = new BusinessLayer.TrainingVideo();
            }

            newVideo.ShortDescription = ShortDescription;
            newVideo.LongDescription = LongDescription;
            newVideo.VideoEmbedPath = VideoEmbedPath;

            if (TrainingVideoID == null || (TrainingVideoID.HasValue && TrainingVideoID.Value == 0))
            {
                CurrentEntityContext.AddToTrainingVideos(newVideo);
            }

            CurrentEntityContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="WireDesignID"></param>
        public void DeleteWireDesign(int WireDesignID)
        {
            if (WireDesignID != 0)
            {
                BusinessLayer.WireDesign delDesign = (from wd in CurrentEntityContext.WireDesigns where wd.WireDesignID == WireDesignID select wd).FirstOrDefault();

                if (delDesign != null)
                {
                    ImageTool.DeleteWireDesignImages(WireDesignID);
                    CurrentEntityContext.DeleteObject(delDesign);
                    CurrentEntityContext.SaveChanges();
                    DesignSource = null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TrainingVideoID"></param>
        public void DeleteTrainingVideo(int TrainingVideoID)
        {
            if (TrainingVideoID != 0)
            {
                BusinessLayer.TrainingVideo delVideo = (from tv in CurrentEntityContext.TrainingVideos where tv.TrainingVideoID == TrainingVideoID select tv).FirstOrDefault();

                if (delVideo != null)
                {
                    CurrentEntityContext.DeleteObject(delVideo);
                    CurrentEntityContext.SaveChanges();
                    trainingVideoSource = null;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="TextValue"></param>
        /// <returns></returns>
        public int? GetInteger(string TextValue)
        {
            if (!string.IsNullOrWhiteSpace(TextValue) && TextValue != "0")
            {
                return Convert.ToInt32(TextValue.Contains(".") ? Convert.ToDouble(TextValue) : Convert.ToInt32(TextValue));
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TextValue"></param>
        /// <returns></returns>
        public double? GetDouble(string TextValue)
        {
            if (!string.IsNullOrWhiteSpace(TextValue) && TextValue != "0")
            {
                return Convert.ToDouble(TextValue);
            }

            return null;
        }

        #region Email Sending routine

        private Emailer _emailerObject;
        private Thread _emailThreader;
        private Emailer _emailerInternalObject;
        private Thread _internalEmailThread;

        /// <summary>
        /// 
        /// </summary>
        public Thread EmailThreader
        {
            get
            {
                if (_emailThreader == null)
                { _emailThreader = new Thread(new ThreadStart(EmailSender)); }
                return _emailThreader;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Emailer EmailerObject
        {
            get
            {
                if (_emailerObject == null)
                { _emailerObject = new Emailer(); }
                return _emailerObject;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SendersName"></param>
        /// <param name="RecieversName"></param>
        /// <param name="_from"></param>
        /// <param name="_to"></param>
        /// <param name="_subject"></param>
        /// <param name="_body"></param>
        /// <returns></returns>
        public string SendEmail(string SendersName, string RecieversName, string _from, string _to, string _subject, string _body)
        {
            _emailerInternalObject = new Emailer();
            _emailerInternalObject.SendersName = SendersName;
            _emailerInternalObject.RecieversName = RecieversName;
            _emailerInternalObject.From = _from;
            _emailerInternalObject.To = _to;
            _emailerInternalObject.Subject = _subject;
            _emailerInternalObject.Body = _body;
            _internalEmailThread = new Thread(new ThreadStart(InternalEmailThread));
            _internalEmailThread.Start();
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        protected void InternalEmailThread()
        {
            if (_emailerInternalObject != null)
            { _emailerInternalObject.ThreadSendEmail(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public void EmailSender()
        {
            EmailerObject.ThreadSendEmail();
        }

        #endregion
    }
}