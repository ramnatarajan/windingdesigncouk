var div1 = null;
var div2 = null;

function PopupRolloverShow( strImageName, nDiv1XPos, nDiv1YPos, strImageText, nDiv2XPos, nDiv2YPos, strTextCol, strFontType, strFontSize, bTextItallic, bTextBold, bBackground, strBackgroundCol, nDiv2Width, nPageWidth )
{
    // If page is centred, add offset
    if( nPageWidth > 0 )
    {
        var nDocWidth = document.body.clientWidth;
        var nOffset = ( nDocWidth - nPageWidth ) / 2;
        
        nDiv1XPos = nDiv1XPos + nOffset;  
        nDiv2XPos = nDiv2XPos + nOffset;
    }	
	
	if(div1 === null)
	{
		div1 = document.createElement('DIV');
		div1.style.position='absolute'
		div1.style.zIndex='100';
		document.body.appendChild(div1);
		
		div2 = document.createElement('DIV');
		div2.style.position='absolute'
		div2.style.zIndex='100';
		document.body.appendChild(div2);
	}

	onDiv = strImageName;

	SetDivText( div1, "<a onmouseout='PopupRolloverTimedHide(100)' onmouseover='onDiv=1'><img border=0 name=ib src='"+strImageName+"'></a>" );
	SetDivPos( div1, nDiv1XPos, nDiv1YPos );

	if( strImageText )
	{
		SetDivFont( div2, strTextCol, strFontType, strFontSize, bTextItallic, bTextBold, bBackground, strBackgroundCol );	
		SetDivPos( div2, nDiv2XPos, nDiv2YPos );
		SetDivText( div2,"<a onmouseout='PopupRolloverTimedHide(100)' onmouseover='onDiv=2'>"+strImageText+"</a>" );
		SetDivWidth( div2, nDiv2Width );
	}
	else
	{
		SetDivText( div2, "" );
		SetDivPos( div2, -10000, -10000 );
		SetDivWidth( div2, 10 );
	}
}

function PopupRolloverTimedHide( nHideDelay )
{
	onDiv = 0;
	timeoutID = window.setTimeout( 'PopupRolloverHide()', nHideDelay );
}

function PopupRolloverHide()
{
	if( onDiv == 0 )
	{
		SetDivText( div1,"" );
		SetDivPos( div1, -1000, -1000 );
		SetDivText( div2,"" );
		SetDivPos( div2, -1000, -1000 );
	}
}

function SetDivPos( div, nXPos, nYPos )
{
	if( div )
	{
	    if ( isNaN( nXPos+nYPos ) )
		    return;
    	
		div.style.left = nXPos+'px';
		div.style.top = nYPos+'px';
	}
}

function SetDivWidth( div, nWidth )
{
	if( div )
	{
	    div.style.width = nWidth;
	}
}

function SetDivText( div, strText )
{
	if( div )
	{
        div.innerHTML=strText;
	}
}

function SetDivFont( div, strTextCol, strFontType, strFontSize, bTextItallic, bTextBold, bBackground, strBackgroundCol )
{
	if( div )
	{
	    div.style.color = strTextCol;
	    div.style.fontSize = strFontSize+'px';
	    div.style.fontFamily = strFontType; 
    	
	    if( bTextItallic )
	        div.style.fontStyle = 'italic';
        else
	        div.style.fontStyle = 'normal';
    	    
	    if( bTextBold )
	        div.style.fontWeight = 'bold';
        else
	        div.style.fontWeight = 'normal';	    

	    if( bBackground )
            div.style.backgroundColor = strBackgroundCol;
	    else
            div.style.backgroundColor = '';
    }
}























