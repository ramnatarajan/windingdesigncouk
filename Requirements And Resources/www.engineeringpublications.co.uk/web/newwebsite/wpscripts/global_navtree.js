var global_navtree = WpNavBar.readTree({
"childArray" : [
{   sTitle:'HOME',
    bIsWebPath:true,
    sUrl:'index.html'
},
{   sTitle:'THE BOOK',
    bIsWebPath:true,
    sUrl:'engineeringhandbook.html'
},
{   sTitle:'AUTHOR\'S NOTES 1',
    bIsWebPath:true,
    sUrl:'authorsnotes.html',"childArray" : [
    {   sTitle:'AUTHOR\'S NOTES 2',
        bIsWebPath:true,
        sUrl:'page20.html'
    }]
},
{   sTitle:'CONTENTS 1',
    bIsWebPath:true,
    sUrl:'contents1.html',"childArray" : [
    {   sTitle:'CONTENTS 2',
        bIsWebPath:true,
        sUrl:'contents2.html'
    }]
},
{   sTitle:'SAMPLE PAGES',
    bIsWebPath:true,
    sUrl:'samplepages.html'
},
{   sTitle:'ORDER BOOK',
    bIsWebPath:true,
    sUrl:'orderthebook.html'
},
{   sTitle:'CONTACT US',
    bIsWebPath:true,
    sUrl:'contactus.html'
},
{   sTitle:'TECHNICAL PAGES',
    bIsWebPath:true,
    sUrl:'technicalpages.html',"childArray" : [
    {   sTitle:'MOTOR FAULTS',
        bIsWebPath:true,
        sUrl:'motorfaults.html'
    },
    {   sTitle:'MOTOR FAULTS cont..',
        bIsWebPath:true,
        sUrl:'motorfaultscont.html'
    },
    {   sTitle:'MECHANICAL FAILURE',
        bIsWebPath:true,
        sUrl:'merchanicalfailure.html'
    },
    {   sTitle:'WINDING FAILURES',
        bIsWebPath:true,
        sUrl:'windingfailures.html'
    },
    {   sTitle:'DOL STARTER',
        bIsWebPath:true,
        sUrl:'dolstarter.html'
    },
    {   sTitle:'USEFUL FORMULAE 1',
        bIsWebPath:true,
        sUrl:'page76.html'
    },
    {   sTitle:'USEFUL FORMULAE 2',
        bIsWebPath:true,
        sUrl:'page86.html'
    },
    {   sTitle:'USEFUL FORMULAE 3',
        bIsWebPath:true,
        sUrl:'page82.html'
    },
    {   sTitle:'USEFUL FORMULAE 4',
        bIsWebPath:true,
        sUrl:'page85.html'
    },
    {   sTitle:'TRADE TIPS',
        bIsWebPath:true,
        sUrl:'engineeringtradetips.html'
    }]
},
{   sTitle:'RECOMMENDED SUPPLIERS 1',
    bIsWebPath:true,
    sUrl:'recommendedsupplyers.html',"childArray" : [
    {   sTitle:'ORGANISATIONS',
        bIsWebPath:true,
        sUrl:'page43.html'
    },
    {   sTitle:'MANUFACTURERS',
        bIsWebPath:true,
        sUrl:'page45.html'
    },
    {   sTitle:'MANUFACTURERS cont..',
        bIsWebPath:true,
        sUrl:'page77.html'
    },
    {   sTitle:'REWIND, REPAIR AND MAINTENANCE',
        bIsWebPath:true,
        sUrl:'page73.html'
    },
    {   sTitle:'REWIND, REPAIR AND MAINTENANCE cont..',
        bIsWebPath:true,
        sUrl:'page74.html'
    }]
},
{   sTitle:'ADVERTISE FREE OF CHARGE',
    bIsWebPath:true,
    sUrl:'advertisefreeofcharge.html'
},
{   sTitle:'ADVERTISEMENT',
    bIsWebPath:true,
    sUrl:'page81.html'
}]
});