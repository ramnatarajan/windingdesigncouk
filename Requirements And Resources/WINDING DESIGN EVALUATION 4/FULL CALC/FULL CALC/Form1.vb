﻿Option Explicit On
Public Class Form1

    Dim FirstNumber As Single
    Dim SecondNumber As Single
    Dim AnswerNumber As Single
    Dim x As Single
    Dim ArithmeticProcess As String

    Private Sub cmd1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd1.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "1"
    End Sub

    Private Sub cmd2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd2.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "2"
    End Sub

    Private Sub cmd3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd3.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "3"
    End Sub

    Private Sub cmd4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd4.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "4"
    End Sub

    Private Sub cmd5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd5.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "5"
    End Sub

    Private Sub cmd6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd6.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "6"
    End Sub

    Private Sub cmd7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd7.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "7"
    End Sub

    Private Sub cmd8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd8.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "8"
    End Sub

    Private Sub cmd9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd9.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "9"
    End Sub

    Private Sub cmd0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmd0.Click
        If txtdisplay.Text = "0" Then txtdisplay.Text = ""
        txtdisplay.Text = txtdisplay.Text & "0"
    End Sub

    Private Sub cmddec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmddec.Click
        txtdisplay.Text = txtdisplay.Text & "."
    End Sub

    Private Sub cmdclr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdclr.Click
        txtdisplay.Text = "0"
    End Sub

    Private Sub cmdadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdadd.Click

        If RadioButton1.Checked = True Then x = 2
        If RadioButton2.Checked = True Then x = 4

        FirstNumber = Val(txtdisplay.Text)
        txtdisplay.Text = "0"
        ArithmeticProcess = "+"
    End Sub

    Private Sub cmdmult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdmult.Click
        FirstNumber = Val(txtdisplay.Text)
        txtdisplay.Text = "0"
        ArithmeticProcess = "x"
    End Sub

    Private Sub cmdsub_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsub.Click
        FirstNumber = Val(txtdisplay.Text)
        txtdisplay.Text = "0"
        ArithmeticProcess = "-"
    End Sub

    Private Sub cmddiv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmddiv.Click
        FirstNumber = Val(txtdisplay.Text)
        txtdisplay.Text = "0"
        ArithmeticProcess = "/"
    End Sub

    Private Sub cmdequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdequ.Click

        If RadioButton1.Checked = True Then x = 2
        If RadioButton2.Checked = True Then x = 4

        SecondNumber = Val(txtdisplay.Text)
        If ArithmeticProcess = "+" Then
            AnswerNumber = FirstNumber + SecondNumber
        End If

        SecondNumber = Val(txtdisplay.Text)
        If ArithmeticProcess = "-" Then
            AnswerNumber = FirstNumber - SecondNumber
        End If

        SecondNumber = Val(txtdisplay.Text)
        If ArithmeticProcess = "x" Then
            AnswerNumber = FirstNumber * SecondNumber
        End If

        SecondNumber = Val(txtdisplay.Text)
        If ArithmeticProcess = "/" Then AnswerNumber = FirstNumber / SecondNumber

        txtdisplay.Text = FormatNumber(AnswerNumber, x)
        If AnswerNumber > 9999999.9999 Then MsgBox("Number too big")
        If AnswerNumber > 9999999.9999 Then txtdisplay.Text = "0"

    End Sub

    Private Sub cmdroot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdroot.Click
        txtdisplay.Text = FormatNumber((txtdisplay.Text ^ 0.5), x)
        AnswerNumber = (txtdisplay.Text)
        If txtdisplay.Text > 9999999.9999 Then MsgBox("Number too big")
        If txtdisplay.Text > 9999999.9999 Then txtdisplay.Text = "0"

    End Sub

    Private Sub cmdsq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsq.Click
        txtdisplay.Text = FormatNumber((txtdisplay.Text ^ 2), x)
        AnswerNumber = (txtdisplay.Text)
        If txtdisplay.Text > 9999999.9999 Then MsgBox("Number too big")
        If txtdisplay.Text > 9999999.9999 Then txtdisplay.Text = "0"

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub
End Class
