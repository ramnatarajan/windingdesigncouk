﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmd0 = New System.Windows.Forms.Button()
        Me.cmd9 = New System.Windows.Forms.Button()
        Me.cmd8 = New System.Windows.Forms.Button()
        Me.cmd7 = New System.Windows.Forms.Button()
        Me.cmd6 = New System.Windows.Forms.Button()
        Me.cmd5 = New System.Windows.Forms.Button()
        Me.cmd4 = New System.Windows.Forms.Button()
        Me.cmd3 = New System.Windows.Forms.Button()
        Me.cmd2 = New System.Windows.Forms.Button()
        Me.cmd1 = New System.Windows.Forms.Button()
        Me.cmdequ = New System.Windows.Forms.Button()
        Me.cmdclr = New System.Windows.Forms.Button()
        Me.cmddiv = New System.Windows.Forms.Button()
        Me.cmdmult = New System.Windows.Forms.Button()
        Me.cmdsub = New System.Windows.Forms.Button()
        Me.cmdadd = New System.Windows.Forms.Button()
        Me.cmddec = New System.Windows.Forms.Button()
        Me.txtdisplay = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdroot = New System.Windows.Forms.Button()
        Me.cmdsq = New System.Windows.Forms.Button()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmd0
        '
        Me.cmd0.BackColor = System.Drawing.Color.Black
        Me.cmd0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd0.ForeColor = System.Drawing.Color.White
        Me.cmd0.Location = New System.Drawing.Point(16, 238)
        Me.cmd0.Name = "cmd0"
        Me.cmd0.Size = New System.Drawing.Size(40, 33)
        Me.cmd0.TabIndex = 0
        Me.cmd0.Text = "0"
        Me.cmd0.UseVisualStyleBackColor = False
        '
        'cmd9
        '
        Me.cmd9.BackColor = System.Drawing.Color.Black
        Me.cmd9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd9.ForeColor = System.Drawing.Color.White
        Me.cmd9.Location = New System.Drawing.Point(99, 137)
        Me.cmd9.Name = "cmd9"
        Me.cmd9.Size = New System.Drawing.Size(40, 33)
        Me.cmd9.TabIndex = 1
        Me.cmd9.Text = "9"
        Me.cmd9.UseVisualStyleBackColor = False
        '
        'cmd8
        '
        Me.cmd8.BackColor = System.Drawing.Color.Black
        Me.cmd8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd8.ForeColor = System.Drawing.Color.White
        Me.cmd8.Location = New System.Drawing.Point(57, 137)
        Me.cmd8.Name = "cmd8"
        Me.cmd8.Size = New System.Drawing.Size(40, 33)
        Me.cmd8.TabIndex = 2
        Me.cmd8.Text = "8"
        Me.cmd8.UseVisualStyleBackColor = False
        '
        'cmd7
        '
        Me.cmd7.BackColor = System.Drawing.Color.Black
        Me.cmd7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd7.ForeColor = System.Drawing.Color.White
        Me.cmd7.Location = New System.Drawing.Point(16, 137)
        Me.cmd7.Name = "cmd7"
        Me.cmd7.Size = New System.Drawing.Size(40, 33)
        Me.cmd7.TabIndex = 3
        Me.cmd7.Text = "7"
        Me.cmd7.UseVisualStyleBackColor = False
        '
        'cmd6
        '
        Me.cmd6.BackColor = System.Drawing.Color.Black
        Me.cmd6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd6.ForeColor = System.Drawing.Color.White
        Me.cmd6.Location = New System.Drawing.Point(99, 172)
        Me.cmd6.Name = "cmd6"
        Me.cmd6.Size = New System.Drawing.Size(40, 33)
        Me.cmd6.TabIndex = 4
        Me.cmd6.Text = "6"
        Me.cmd6.UseVisualStyleBackColor = False
        '
        'cmd5
        '
        Me.cmd5.BackColor = System.Drawing.Color.Black
        Me.cmd5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd5.ForeColor = System.Drawing.Color.White
        Me.cmd5.Location = New System.Drawing.Point(57, 172)
        Me.cmd5.Name = "cmd5"
        Me.cmd5.Size = New System.Drawing.Size(40, 33)
        Me.cmd5.TabIndex = 5
        Me.cmd5.Text = "5"
        Me.cmd5.UseVisualStyleBackColor = False
        '
        'cmd4
        '
        Me.cmd4.BackColor = System.Drawing.Color.Black
        Me.cmd4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd4.ForeColor = System.Drawing.Color.White
        Me.cmd4.Location = New System.Drawing.Point(16, 171)
        Me.cmd4.Name = "cmd4"
        Me.cmd4.Size = New System.Drawing.Size(40, 33)
        Me.cmd4.TabIndex = 6
        Me.cmd4.Text = "4"
        Me.cmd4.UseVisualStyleBackColor = False
        '
        'cmd3
        '
        Me.cmd3.BackColor = System.Drawing.Color.Black
        Me.cmd3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd3.ForeColor = System.Drawing.Color.White
        Me.cmd3.Location = New System.Drawing.Point(99, 205)
        Me.cmd3.Name = "cmd3"
        Me.cmd3.Size = New System.Drawing.Size(40, 33)
        Me.cmd3.TabIndex = 7
        Me.cmd3.Text = "3"
        Me.cmd3.UseVisualStyleBackColor = False
        '
        'cmd2
        '
        Me.cmd2.BackColor = System.Drawing.Color.Black
        Me.cmd2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd2.ForeColor = System.Drawing.Color.White
        Me.cmd2.Location = New System.Drawing.Point(57, 205)
        Me.cmd2.Name = "cmd2"
        Me.cmd2.Size = New System.Drawing.Size(40, 33)
        Me.cmd2.TabIndex = 8
        Me.cmd2.Text = "2"
        Me.cmd2.UseVisualStyleBackColor = False
        '
        'cmd1
        '
        Me.cmd1.BackColor = System.Drawing.Color.Black
        Me.cmd1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd1.ForeColor = System.Drawing.Color.White
        Me.cmd1.Location = New System.Drawing.Point(16, 204)
        Me.cmd1.Name = "cmd1"
        Me.cmd1.Size = New System.Drawing.Size(40, 33)
        Me.cmd1.TabIndex = 9
        Me.cmd1.Text = "1"
        Me.cmd1.UseVisualStyleBackColor = False
        '
        'cmdequ
        '
        Me.cmdequ.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdequ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdequ.Location = New System.Drawing.Point(140, 206)
        Me.cmdequ.Name = "cmdequ"
        Me.cmdequ.Size = New System.Drawing.Size(81, 33)
        Me.cmdequ.TabIndex = 10
        Me.cmdequ.Text = "="
        Me.cmdequ.UseVisualStyleBackColor = False
        '
        'cmdclr
        '
        Me.cmdclr.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdclr.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdclr.Location = New System.Drawing.Point(181, 238)
        Me.cmdclr.Name = "cmdclr"
        Me.cmdclr.Size = New System.Drawing.Size(40, 33)
        Me.cmdclr.TabIndex = 11
        Me.cmdclr.Text = "CL"
        Me.cmdclr.UseVisualStyleBackColor = False
        '
        'cmddiv
        '
        Me.cmddiv.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmddiv.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmddiv.Location = New System.Drawing.Point(181, 172)
        Me.cmddiv.Name = "cmddiv"
        Me.cmddiv.Size = New System.Drawing.Size(40, 33)
        Me.cmddiv.TabIndex = 12
        Me.cmddiv.Text = "/"
        Me.cmddiv.UseVisualStyleBackColor = False
        '
        'cmdmult
        '
        Me.cmdmult.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdmult.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdmult.Location = New System.Drawing.Point(181, 138)
        Me.cmdmult.Name = "cmdmult"
        Me.cmdmult.Size = New System.Drawing.Size(40, 33)
        Me.cmdmult.TabIndex = 13
        Me.cmdmult.Text = "x"
        Me.cmdmult.UseVisualStyleBackColor = False
        '
        'cmdsub
        '
        Me.cmdsub.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdsub.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdsub.Location = New System.Drawing.Point(140, 172)
        Me.cmdsub.Name = "cmdsub"
        Me.cmdsub.Size = New System.Drawing.Size(40, 33)
        Me.cmdsub.TabIndex = 14
        Me.cmdsub.Text = "-"
        Me.cmdsub.UseVisualStyleBackColor = False
        '
        'cmdadd
        '
        Me.cmdadd.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdadd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdadd.Location = New System.Drawing.Point(140, 138)
        Me.cmdadd.Name = "cmdadd"
        Me.cmdadd.Size = New System.Drawing.Size(40, 33)
        Me.cmdadd.TabIndex = 15
        Me.cmdadd.Text = "+"
        Me.cmdadd.UseVisualStyleBackColor = False
        '
        'cmddec
        '
        Me.cmddec.BackColor = System.Drawing.Color.Black
        Me.cmddec.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmddec.ForeColor = System.Drawing.Color.White
        Me.cmddec.Location = New System.Drawing.Point(57, 238)
        Me.cmddec.Name = "cmddec"
        Me.cmddec.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.cmddec.Size = New System.Drawing.Size(40, 33)
        Me.cmddec.TabIndex = 16
        Me.cmddec.Text = "."
        Me.cmddec.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmddec.UseVisualStyleBackColor = False
        '
        'txtdisplay
        '
        Me.txtdisplay.BackColor = System.Drawing.Color.White
        Me.txtdisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdisplay.Location = New System.Drawing.Point(16, 56)
        Me.txtdisplay.Name = "txtdisplay"
        Me.txtdisplay.ReadOnly = True
        Me.txtdisplay.Size = New System.Drawing.Size(202, 32)
        Me.txtdisplay.TabIndex = 17
        Me.txtdisplay.Text = "0"
        Me.txtdisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(55, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 20)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "CALCULATOR"
        '
        'cmdroot
        '
        Me.cmdroot.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdroot.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdroot.Location = New System.Drawing.Point(99, 238)
        Me.cmdroot.Name = "cmdroot"
        Me.cmdroot.Size = New System.Drawing.Size(40, 33)
        Me.cmdroot.TabIndex = 24
        Me.cmdroot.Text = "SR"
        Me.cmdroot.UseVisualStyleBackColor = False
        '
        'cmdsq
        '
        Me.cmdsq.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdsq.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdsq.Location = New System.Drawing.Point(140, 238)
        Me.cmdsq.Name = "cmdsq"
        Me.cmdsq.Size = New System.Drawing.Size(39, 33)
        Me.cmdsq.TabIndex = 25
        Me.cmdsq.Text = "SQ"
        Me.cmdsq.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RadioButton1.Checked = True
        Me.RadioButton1.ForeColor = System.Drawing.Color.Red
        Me.RadioButton1.Location = New System.Drawing.Point(147, 103)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton1.TabIndex = 26
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RadioButton2.ForeColor = System.Drawing.Color.Red
        Me.RadioButton2.Location = New System.Drawing.Point(194, 103)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(14, 13)
        Me.RadioButton2.TabIndex = 27
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(146, 118)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 17)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(193, 118)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 17)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "4"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button2.Location = New System.Drawing.Point(10, 49)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(213, 46)
        Me.Button2.TabIndex = 30
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button1.Location = New System.Drawing.Point(-1, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(238, 308)
        Me.Button1.TabIndex = 19
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(236, 308)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmdsq)
        Me.Controls.Add(Me.cmdroot)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtdisplay)
        Me.Controls.Add(Me.cmddec)
        Me.Controls.Add(Me.cmdadd)
        Me.Controls.Add(Me.cmdsub)
        Me.Controls.Add(Me.cmdmult)
        Me.Controls.Add(Me.cmddiv)
        Me.Controls.Add(Me.cmdclr)
        Me.Controls.Add(Me.cmdequ)
        Me.Controls.Add(Me.cmd1)
        Me.Controls.Add(Me.cmd2)
        Me.Controls.Add(Me.cmd3)
        Me.Controls.Add(Me.cmd4)
        Me.Controls.Add(Me.cmd5)
        Me.Controls.Add(Me.cmd6)
        Me.Controls.Add(Me.cmd7)
        Me.Controls.Add(Me.cmd8)
        Me.Controls.Add(Me.cmd9)
        Me.Controls.Add(Me.cmd0)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "©Engineering Publications Ltd."
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmd0 As System.Windows.Forms.Button
    Friend WithEvents cmd9 As System.Windows.Forms.Button
    Friend WithEvents cmd8 As System.Windows.Forms.Button
    Friend WithEvents cmd7 As System.Windows.Forms.Button
    Friend WithEvents cmd6 As System.Windows.Forms.Button
    Friend WithEvents cmd5 As System.Windows.Forms.Button
    Friend WithEvents cmd4 As System.Windows.Forms.Button
    Friend WithEvents cmd3 As System.Windows.Forms.Button
    Friend WithEvents cmd2 As System.Windows.Forms.Button
    Friend WithEvents cmd1 As System.Windows.Forms.Button
    Friend WithEvents cmdequ As System.Windows.Forms.Button
    Friend WithEvents cmdclr As System.Windows.Forms.Button
    Friend WithEvents cmddiv As System.Windows.Forms.Button
    Friend WithEvents cmdmult As System.Windows.Forms.Button
    Friend WithEvents cmdsub As System.Windows.Forms.Button
    Friend WithEvents cmdadd As System.Windows.Forms.Button
    Friend WithEvents cmddec As System.Windows.Forms.Button
    Friend WithEvents txtdisplay As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdroot As System.Windows.Forms.Button
    Friend WithEvents cmdsq As System.Windows.Forms.Button
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
