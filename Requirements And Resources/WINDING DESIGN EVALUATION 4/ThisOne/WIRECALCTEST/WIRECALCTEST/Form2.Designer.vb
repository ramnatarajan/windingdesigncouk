﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProgramsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WireCalcToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OutputPowerAndSpeedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxOldVolt = New System.Windows.Forms.TextBox()
        Me.TextBoxOldFreq = New System.Windows.Forms.TextBox()
        Me.TextBoxOldKw = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBoxOldTurn = New System.Windows.Forms.TextBox()
        Me.TextBoxOldDia = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxNewDia = New System.Windows.Forms.TextBox()
        Me.TextBoxNewTurn = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBoxNewKw = New System.Windows.Forms.TextBox()
        Me.TextBoxNewFreq = New System.Windows.Forms.TextBox()
        Me.TextBoxNewVolt = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBoxNewPath = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBoxOldPath = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBoxOldSpeed = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBoxNewSpeed = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TextBoxOldCool = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBoxNewCool = New System.Windows.Forms.TextBox()
        Me.TextBoxArea1 = New System.Windows.Forms.TextBox()
        Me.TextBoxArea2 = New System.Windows.Forms.TextBox()
        Me.TextBoxAns1 = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBoxOldAmp = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBoxNewAmp = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TextBoxOldVolt1 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TextBoxOldAmp1 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBoxNewVolt1 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TextBoxNewAmp1 = New System.Windows.Forms.TextBox()
        Me.TextBoxCon = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TextBoxCon1 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(943, 68)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 15)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "© Tony Ball April 2012"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(-1, 33)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(1113, 63)
        Me.Button1.TabIndex = 22
        Me.Button1.UseVisualStyleBackColor = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightBlue
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ProgramsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1112, 31)
        Me.MenuStrip1.TabIndex = 24
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(47, 27)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(107, 28)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ProgramsToolStripMenuItem
        '
        Me.ProgramsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WireCalcToolStripMenuItem, Me.OutputPowerAndSpeedToolStripMenuItem})
        Me.ProgramsToolStripMenuItem.Name = "ProgramsToolStripMenuItem"
        Me.ProgramsToolStripMenuItem.Size = New System.Drawing.Size(95, 27)
        Me.ProgramsToolStripMenuItem.Text = "Programs"
        '
        'WireCalcToolStripMenuItem
        '
        Me.WireCalcToolStripMenuItem.Name = "WireCalcToolStripMenuItem"
        Me.WireCalcToolStripMenuItem.Size = New System.Drawing.Size(273, 28)
        Me.WireCalcToolStripMenuItem.Text = "Wire Calculator"
        '
        'OutputPowerAndSpeedToolStripMenuItem
        '
        Me.OutputPowerAndSpeedToolStripMenuItem.Name = "OutputPowerAndSpeedToolStripMenuItem"
        Me.OutputPowerAndSpeedToolStripMenuItem.Size = New System.Drawing.Size(273, 28)
        Me.OutputPowerAndSpeedToolStripMenuItem.Text = "Output Power and Speed"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(57, 27)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button12.ForeColor = System.Drawing.Color.White
        Me.Button12.Location = New System.Drawing.Point(564, 442)
        Me.Button12.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(83, 54)
        Me.Button12.TabIndex = 180
        Me.Button12.Text = "Clear All"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.Green
        Me.Button13.ForeColor = System.Drawing.Color.White
        Me.Button13.Location = New System.Drawing.Point(467, 442)
        Me.Button13.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(83, 54)
        Me.Button13.TabIndex = 178
        Me.Button13.Text = "Calc"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(145, 140)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(151, 20)
        Me.Label15.TabIndex = 185
        Me.Label15.Text = "ORIGINAL DATA"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button5.Location = New System.Drawing.Point(48, 119)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(363, 405)
        Me.Button5.TabIndex = 169
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(135, 178)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(118, 18)
        Me.Label1.TabIndex = 189
        Me.Label1.Text = "Supply Voltage ="
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(113, 314)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 18)
        Me.Label2.TabIndex = 190
        Me.Label2.Text = "Supply Frequency ="
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(95, 342)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(160, 18)
        Me.Label3.TabIndex = 191
        Me.Label3.Text = "Output Power (kW's) ="
        '
        'TextBoxOldVolt
        '
        Me.TextBoxOldVolt.Location = New System.Drawing.Point(271, 175)
        Me.TextBoxOldVolt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldVolt.Name = "TextBoxOldVolt"
        Me.TextBoxOldVolt.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldVolt.TabIndex = 0
        '
        'TextBoxOldFreq
        '
        Me.TextBoxOldFreq.Location = New System.Drawing.Point(271, 309)
        Me.TextBoxOldFreq.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldFreq.Name = "TextBoxOldFreq"
        Me.TextBoxOldFreq.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldFreq.TabIndex = 3
        '
        'TextBoxOldKw
        '
        Me.TextBoxOldKw.Location = New System.Drawing.Point(271, 337)
        Me.TextBoxOldKw.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldKw.Name = "TextBoxOldKw"
        Me.TextBoxOldKw.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldKw.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(140, 369)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(113, 18)
        Me.Label7.TabIndex = 195
        Me.Label7.Text = "Original Turns ="
        '
        'TextBoxOldTurn
        '
        Me.TextBoxOldTurn.Location = New System.Drawing.Point(271, 364)
        Me.TextBoxOldTurn.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldTurn.Name = "TextBoxOldTurn"
        Me.TextBoxOldTurn.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldTurn.TabIndex = 5
        '
        'TextBoxOldDia
        '
        Me.TextBoxOldDia.Location = New System.Drawing.Point(271, 391)
        Me.TextBoxOldDia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldDia.Name = "TextBoxOldDia"
        Me.TextBoxOldDia.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldDia.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(115, 395)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(136, 18)
        Me.Label8.TabIndex = 198
        Me.Label8.Text = "Original Wire Dia. ="
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(792, 389)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(116, 18)
        Me.Label9.TabIndex = 210
        Me.Label9.Text = "New Wire Dia. ="
        '
        'TextBoxNewDia
        '
        Me.TextBoxNewDia.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewDia.Location = New System.Drawing.Point(925, 384)
        Me.TextBoxNewDia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewDia.Name = "TextBoxNewDia"
        Me.TextBoxNewDia.ReadOnly = True
        Me.TextBoxNewDia.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewDia.TabIndex = 209
        '
        'TextBoxNewTurn
        '
        Me.TextBoxNewTurn.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewTurn.Location = New System.Drawing.Point(925, 358)
        Me.TextBoxNewTurn.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewTurn.Name = "TextBoxNewTurn"
        Me.TextBoxNewTurn.ReadOnly = True
        Me.TextBoxNewTurn.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewTurn.TabIndex = 208
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(817, 363)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 18)
        Me.Label10.TabIndex = 207
        Me.Label10.Text = "New Turns ="
        '
        'TextBoxNewKw
        '
        Me.TextBoxNewKw.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewKw.Location = New System.Drawing.Point(925, 332)
        Me.TextBoxNewKw.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewKw.Name = "TextBoxNewKw"
        Me.TextBoxNewKw.ReadOnly = True
        Me.TextBoxNewKw.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewKw.TabIndex = 200
        '
        'TextBoxNewFreq
        '
        Me.TextBoxNewFreq.Location = New System.Drawing.Point(925, 305)
        Me.TextBoxNewFreq.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewFreq.Name = "TextBoxNewFreq"
        Me.TextBoxNewFreq.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewFreq.TabIndex = 10
        '
        'TextBoxNewVolt
        '
        Me.TextBoxNewVolt.Location = New System.Drawing.Point(925, 172)
        Me.TextBoxNewVolt.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewVolt.Name = "TextBoxNewVolt"
        Me.TextBoxNewVolt.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewVolt.TabIndex = 9
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(748, 337)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(160, 18)
        Me.Label11.TabIndex = 203
        Me.Label11.Text = "Output Power (kW's) ="
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(767, 310)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(138, 18)
        Me.Label12.TabIndex = 202
        Me.Label12.Text = "Supply Frequency ="
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(788, 178)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(118, 18)
        Me.Label13.TabIndex = 201
        Me.Label13.Text = "Supply Voltage ="
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(825, 140)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(106, 20)
        Me.Label14.TabIndex = 200
        Me.Label14.Text = "NEW DATA"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button2.Location = New System.Drawing.Point(703, 119)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(363, 405)
        Me.Button2.TabIndex = 12
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TextBoxNewPath
        '
        Me.TextBoxNewPath.Location = New System.Drawing.Point(925, 410)
        Me.TextBoxNewPath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewPath.Name = "TextBoxNewPath"
        Me.TextBoxNewPath.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewPath.TabIndex = 11
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(771, 414)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(138, 18)
        Me.Label16.TabIndex = 213
        Me.Label16.Text = "New No. of Paths ="
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(97, 422)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(154, 18)
        Me.Label17.TabIndex = 215
        Me.Label17.Text = "Original No.of Paths ="
        '
        'TextBoxOldPath
        '
        Me.TextBoxOldPath.Location = New System.Drawing.Point(271, 418)
        Me.TextBoxOldPath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldPath.Name = "TextBoxOldPath"
        Me.TextBoxOldPath.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldPath.TabIndex = 7
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.White
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(513, 524)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(79, 17)
        Me.Label18.TabIndex = 216
        Me.Label18.Text = "RESULTS"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(73, 554)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(59, 17)
        Me.Label19.TabIndex = 220
        Me.Label19.Text = "Label19"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label20.Location = New System.Drawing.Point(73, 575)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(59, 17)
        Me.Label20.TabIndex = 219
        Me.Label20.Text = "Label20"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(73, 596)
        Me.Label21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(59, 17)
        Me.Label21.TabIndex = 218
        Me.Label21.Text = "Label21"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button3.Location = New System.Drawing.Point(48, 543)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(1017, 122)
        Me.Button3.TabIndex = 217
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(99, 449)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(150, 18)
        Me.Label22.TabIndex = 224
        Me.Label22.Text = "Original F. L. Speed ="
        '
        'TextBoxOldSpeed
        '
        Me.TextBoxOldSpeed.Location = New System.Drawing.Point(271, 446)
        Me.TextBoxOldSpeed.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldSpeed.Name = "TextBoxOldSpeed"
        Me.TextBoxOldSpeed.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldSpeed.TabIndex = 8
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(776, 438)
        Me.Label23.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(130, 18)
        Me.Label23.TabIndex = 223
        Me.Label23.Text = "New F. L. Speed ="
        '
        'TextBoxNewSpeed
        '
        Me.TextBoxNewSpeed.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewSpeed.Location = New System.Drawing.Point(925, 436)
        Me.TextBoxNewSpeed.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewSpeed.Name = "TextBoxNewSpeed"
        Me.TextBoxNewSpeed.ReadOnly = True
        Me.TextBoxNewSpeed.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewSpeed.TabIndex = 222
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(125, 476)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(126, 18)
        Me.Label24.TabIndex = 228
        Me.Label24.Text = "Original Cooling ="
        '
        'TextBoxOldCool
        '
        Me.TextBoxOldCool.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxOldCool.Location = New System.Drawing.Point(271, 473)
        Me.TextBoxOldCool.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldCool.Name = "TextBoxOldCool"
        Me.TextBoxOldCool.ReadOnly = True
        Me.TextBoxOldCool.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldCool.TabIndex = 201
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(761, 460)
        Me.Label25.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(142, 18)
        Me.Label25.TabIndex = 227
        Me.Label25.Text = "Change in Cooling ="
        '
        'TextBoxNewCool
        '
        Me.TextBoxNewCool.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewCool.Location = New System.Drawing.Point(925, 459)
        Me.TextBoxNewCool.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewCool.Name = "TextBoxNewCool"
        Me.TextBoxNewCool.ReadOnly = True
        Me.TextBoxNewCool.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewCool.TabIndex = 12
        '
        'TextBoxArea1
        '
        Me.TextBoxArea1.Location = New System.Drawing.Point(252, 489)
        Me.TextBoxArea1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxArea1.Name = "TextBoxArea1"
        Me.TextBoxArea1.Size = New System.Drawing.Size(132, 22)
        Me.TextBoxArea1.TabIndex = 231
        '
        'TextBoxArea2
        '
        Me.TextBoxArea2.Location = New System.Drawing.Point(252, 480)
        Me.TextBoxArea2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxArea2.Name = "TextBoxArea2"
        Me.TextBoxArea2.Size = New System.Drawing.Size(132, 22)
        Me.TextBoxArea2.TabIndex = 232
        '
        'TextBoxAns1
        '
        Me.TextBoxAns1.Location = New System.Drawing.Point(252, 484)
        Me.TextBoxAns1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxAns1.Name = "TextBoxAns1"
        Me.TextBoxAns1.Size = New System.Drawing.Size(132, 22)
        Me.TextBoxAns1.TabIndex = 233
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.White
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(409, 42)
        Me.Label26.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(255, 40)
        Me.Label26.TabIndex = 235
        Me.Label26.Text = "       Converting Voltage" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Frequency and Current Paths"
        '
        'TextBoxOldAmp
        '
        Me.TextBoxOldAmp.Location = New System.Drawing.Point(271, 202)
        Me.TextBoxOldAmp.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldAmp.Name = "TextBoxOldAmp"
        Me.TextBoxOldAmp.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldAmp.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(117, 207)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(134, 18)
        Me.Label4.TabIndex = 237
        Me.Label4.Text = "Full Load Current ="
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(764, 204)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(141, 18)
        Me.Label6.TabIndex = 238
        Me.Label6.Text = "New Load Current ="
        '
        'TextBoxNewAmp
        '
        Me.TextBoxNewAmp.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewAmp.Location = New System.Drawing.Point(925, 201)
        Me.TextBoxNewAmp.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewAmp.Name = "TextBoxNewAmp"
        Me.TextBoxNewAmp.ReadOnly = True
        Me.TextBoxNewAmp.Size = New System.Drawing.Size(112, 22)
        Me.TextBoxNewAmp.TabIndex = 239
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(137, 234)
        Me.Label28.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(116, 18)
        Me.Label28.TabIndex = 242
        Me.Label28.Text = "Phase Voltage ="
        '
        'TextBoxOldVolt1
        '
        Me.TextBoxOldVolt1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxOldVolt1.Location = New System.Drawing.Point(271, 229)
        Me.TextBoxOldVolt1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldVolt1.Name = "TextBoxOldVolt1"
        Me.TextBoxOldVolt1.ReadOnly = True
        Me.TextBoxOldVolt1.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldVolt1.TabIndex = 241
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(139, 261)
        Me.Label29.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(116, 18)
        Me.Label29.TabIndex = 244
        Me.Label29.Text = "Phase Current ="
        '
        'TextBoxOldAmp1
        '
        Me.TextBoxOldAmp1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxOldAmp1.Location = New System.Drawing.Point(271, 256)
        Me.TextBoxOldAmp1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxOldAmp1.Name = "TextBoxOldAmp1"
        Me.TextBoxOldAmp1.ReadOnly = True
        Me.TextBoxOldAmp1.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxOldAmp1.TabIndex = 243
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(791, 233)
        Me.Label30.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(116, 18)
        Me.Label30.TabIndex = 246
        Me.Label30.Text = "Phase Voltage ="
        '
        'TextBoxNewVolt1
        '
        Me.TextBoxNewVolt1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewVolt1.Location = New System.Drawing.Point(924, 228)
        Me.TextBoxNewVolt1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewVolt1.Name = "TextBoxNewVolt1"
        Me.TextBoxNewVolt1.ReadOnly = True
        Me.TextBoxNewVolt1.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxNewVolt1.TabIndex = 245
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(792, 260)
        Me.Label31.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(116, 18)
        Me.Label31.TabIndex = 248
        Me.Label31.Text = "Phase Current ="
        '
        'TextBoxNewAmp1
        '
        Me.TextBoxNewAmp1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxNewAmp1.Location = New System.Drawing.Point(924, 255)
        Me.TextBoxNewAmp1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxNewAmp1.Name = "TextBoxNewAmp1"
        Me.TextBoxNewAmp1.ReadOnly = True
        Me.TextBoxNewAmp1.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxNewAmp1.TabIndex = 247
        '
        'TextBoxCon
        '
        Me.TextBoxCon.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxCon.Location = New System.Drawing.Point(271, 283)
        Me.TextBoxCon.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxCon.Name = "TextBoxCon"
        Me.TextBoxCon.ReadOnly = True
        Me.TextBoxCon.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxCon.TabIndex = 249
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(160, 288)
        Me.Label32.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(97, 18)
        Me.Label32.TabIndex = 250
        Me.Label32.Text = "Connection ="
        '
        'TextBoxCon1
        '
        Me.TextBoxCon1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextBoxCon1.Location = New System.Drawing.Point(924, 281)
        Me.TextBoxCon1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBoxCon1.Name = "TextBoxCon1"
        Me.TextBoxCon1.ReadOnly = True
        Me.TextBoxCon1.Size = New System.Drawing.Size(113, 22)
        Me.TextBoxCon1.TabIndex = 251
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(813, 286)
        Me.Label33.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(97, 18)
        Me.Label33.TabIndex = 252
        Me.Label33.Text = "Connection ="
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(444, 390)
        Me.Label27.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(225, 34)
        Me.Label27.TabIndex = 234
        Me.Label27.Text = "Use WireCalc to convert your " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "original wires to one wire diameter!"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(9, 50)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(144, 26)
        Me.RadioButton2.TabIndex = 254
        Me.RadioButton2.Text = "Star to Delta"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButton1)
        Me.GroupBox2.Controls.Add(Me.RadioButton3)
        Me.GroupBox2.Controls.Add(Me.RadioButton2)
        Me.GroupBox2.Controls.Add(Me.RadioButton4)
        Me.GroupBox2.Location = New System.Drawing.Point(489, 178)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(133, 135)
        Me.GroupBox2.TabIndex = 251
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Convert from:"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(9, 22)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(135, 26)
        Me.RadioButton1.TabIndex = 254
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Star to Star"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(9, 78)
        Me.RadioButton3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(144, 26)
        Me.RadioButton3.TabIndex = 257
        Me.RadioButton3.Text = "Delta to Star"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(9, 105)
        Me.RadioButton4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(153, 26)
        Me.RadioButton4.TabIndex = 256
        Me.RadioButton4.Text = "Delta to Delta"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label34.Location = New System.Drawing.Point(73, 617)
        Me.Label34.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(59, 17)
        Me.Label34.TabIndex = 253
        Me.Label34.Text = "Label34"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label35.Location = New System.Drawing.Point(73, 638)
        Me.Label35.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(59, 17)
        Me.Label35.TabIndex = 254
        Me.Label35.Text = "Label35"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(27, 68)
        Me.Label36.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(42, 17)
        Me.Label36.TabIndex = 258
        Me.Label36.Text = "Date:"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(13, 42)
        Me.Label37.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(57, 17)
        Me.Label37.TabIndex = 257
        Me.Label37.Text = "Job No:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(72, 38)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(93, 22)
        Me.TextBox1.TabIndex = 255
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(263, 44)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(0, 17)
        Me.Label38.TabIndex = 263
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(188, 68)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(43, 17)
        Me.Label39.TabIndex = 262
        Me.Label39.Text = "Time:"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(263, 44)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(0, 17)
        Me.Label41.TabIndex = 260
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(229, 68)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(44, 17)
        Me.Label42.TabIndex = 259
        Me.Label42.Text = "00:00"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(68, 66)
        Me.Label44.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(42, 17)
        Me.Label44.TabIndex = 265
        Me.Label44.Text = "Date:"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1112, 676)
        Me.Controls.Add(Me.Label44)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.TextBoxCon1)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.TextBoxCon)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.TextBoxOldAmp1)
        Me.Controls.Add(Me.TextBoxOldVolt1)
        Me.Controls.Add(Me.TextBoxOldAmp)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.TextBoxOldCool)
        Me.Controls.Add(Me.TextBoxOldSpeed)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.TextBoxOldPath)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.TextBoxOldDia)
        Me.Controls.Add(Me.TextBoxOldTurn)
        Me.Controls.Add(Me.TextBoxOldKw)
        Me.Controls.Add(Me.TextBoxOldFreq)
        Me.Controls.Add(Me.TextBoxOldVolt)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.TextBoxNewAmp1)
        Me.Controls.Add(Me.TextBoxNewVolt1)
        Me.Controls.Add(Me.TextBoxNewAmp)
        Me.Controls.Add(Me.TextBoxNewCool)
        Me.Controls.Add(Me.TextBoxNewSpeed)
        Me.Controls.Add(Me.TextBoxNewPath)
        Me.Controls.Add(Me.TextBoxNewDia)
        Me.Controls.Add(Me.TextBoxNewTurn)
        Me.Controls.Add(Me.TextBoxNewKw)
        Me.Controls.Add(Me.TextBoxNewFreq)
        Me.Controls.Add(Me.TextBoxNewVolt)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.TextBoxAns1)
        Me.Controls.Add(Me.TextBoxArea2)
        Me.Controls.Add(Me.TextBoxArea1)
        Me.Controls.Add(Me.GroupBox2)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "Form2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form2"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProgramsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WireCalcToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldVolt As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxOldFreq As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxOldKw As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldTurn As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxOldDia As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNewDia As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxNewTurn As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNewKw As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxNewFreq As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxNewVolt As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBoxNewPath As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldPath As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldSpeed As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNewSpeed As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldCool As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNewCool As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxArea1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxArea2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxAns1 As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldAmp As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNewAmp As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldVolt1 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TextBoxOldAmp1 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNewVolt1 As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNewAmp1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxCon As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents TextBoxCon1 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents OutputPowerAndSpeedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
End Class
