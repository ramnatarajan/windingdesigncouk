﻿Public Class Form2



    Dim NewDia As Single

    Private Sub WireCalcToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WireCalcToolStripMenuItem.Click
        Form1.Show()
        Me.Hide()
    End Sub


    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click

        TextBoxOldCool.Text = "1"
        Form1.TextBoxNum1.Text = 1

        'ORIGINAL DATA MESSAGE BOXES

        If TextBoxOldVolt.Text = "" Then MsgBox("Please enter the original voltage!")
        If TextBoxOldAmp.Text = "" Then MsgBox("Please enter the original current!")
        If TextBoxOldFreq.Text = "" Then MsgBox("Please enter the original frequency!")
        If TextBoxOldKw.Text = "" Then MsgBox("Please enter the original output power!")
        If TextBoxOldTurn.Text = "" Then MsgBox("Please enter the original turns!")
        If TextBoxOldPath.Text = "" Then MsgBox("Please enter the original number of current paths!")
        If TextBoxOldSpeed.Text = "" Then MsgBox("Please enter the original motor speed!")
        If TextBoxOldDia.Text = "" Then MsgBox("Please enter the original wire diameter!")
        If RadioButton1.Checked = False And RadioButton2.Checked = False And RadioButton3.Checked = False And RadioButton4.Checked = False Then MsgBox("Click Star or Delta")

        'NEW DATA MESSAGE BOXES

        If TextBoxNewVolt.Text = "" Then MsgBox("Please enter the new voltage!")
        If TextBoxNewFreq.Text = "" Then MsgBox("Please enter the new frequency!")
        If TextBoxNewPath.Text = "" Then MsgBox("Please enter the new number of current paths!")

        'NEW CURRENT CALC

        TextBoxNewAmp.Text = FormatNumber((TextBoxOldAmp.Text * (TextBoxNewFreq.Text / TextBoxOldFreq.Text) * (TextBoxOldVolt.Text / TextBoxNewVolt.Text)), 2)

        'RADIO BUTTONS

        If RadioButton1.Checked = True Then TextBoxCon.Text = "Star"
        If RadioButton1.Checked = True Then TextBoxCon1.Text = "Star"
        If RadioButton2.Checked = True Then TextBoxCon.Text = "Star"
        If RadioButton2.Checked = True Then TextBoxCon1.Text = "Delta"
        If RadioButton3.Checked = True Then TextBoxCon.Text = "Delta"
        If RadioButton3.Checked = True Then TextBoxCon1.Text = "Star"
        If RadioButton4.Checked = True Then TextBoxCon.Text = "Delta"
        If RadioButton4.Checked = True Then TextBoxCon1.Text = "Delta"

        'LABELS

        TextBoxOldVolt.Text = Val(TextBoxOldVolt.Text)
        TextBoxOldAmp.Text = Val(TextBoxOldAmp.Text)
        TextBoxOldFreq.Text = Val(TextBoxOldFreq.Text)
        TextBoxOldKw.Text = Val(TextBoxOldKw.Text)
        TextBoxOldTurn.Text = Val(TextBoxOldTurn.Text)
        TextBoxOldPath.Text = Val(TextBoxOldPath.Text)
        TextBoxOldSpeed.Text = Val(TextBoxOldSpeed.Text)

        'OLD STAR DELTA CONNECTIONS

        If RadioButton1.Checked = True Then TextBoxOldVolt1.Text = FormatNumber(Val(TextBoxOldVolt.Text) / (3 ^ 0.5), 0)
        If RadioButton1.Checked = True Then TextBoxOldAmp1.Text = FormatNumber(Val(TextBoxOldAmp.Text), 1)

        If RadioButton2.Checked = True Then TextBoxOldVolt1.Text = FormatNumber(Val(TextBoxOldVolt.Text) / (3 ^ 0.5), 0)
        If RadioButton2.Checked = True Then TextBoxOldAmp1.Text = FormatNumber(Val(TextBoxOldAmp.Text), 1)

        If RadioButton3.Checked = True Then TextBoxOldVolt1.Text = FormatNumber(Val(TextBoxOldVolt.Text), 0)
        If RadioButton3.Checked = True Then TextBoxOldAmp1.Text = FormatNumber(Val(TextBoxOldAmp.Text) / (3 ^ 0.5), 1)

        If RadioButton4.Checked = True Then TextBoxOldVolt1.Text = FormatNumber(Val(TextBoxOldVolt.Text), 0)
        If RadioButton4.Checked = True Then TextBoxOldAmp1.Text = FormatNumber(Val(TextBoxOldAmp.Text) / (3 ^ 0.5), 1)

        'NEW STAR DELTA CALCULATIONS

        If RadioButton1.Checked = True Then TextBoxNewVolt1.Text = FormatNumber(Val(TextBoxNewVolt.Text) / (3 ^ 0.5), 0)
        If RadioButton1.Checked = True Then TextBoxNewAmp1.Text = FormatNumber(Val(TextBoxNewAmp.Text), 1)

        If RadioButton2.Checked = True Then TextBoxNewVolt1.Text = FormatNumber(Val(TextBoxNewVolt.Text), 0)
        If RadioButton2.Checked = True Then TextBoxNewAmp1.Text = FormatNumber(Val(TextBoxNewAmp.Text) / (3 ^ 0.5), 1)

        If RadioButton3.Checked = True Then TextBoxNewVolt1.Text = FormatNumber(Val(TextBoxNewVolt.Text) / (3 ^ 0.5), 0)
        If RadioButton3.Checked = True Then TextBoxNewAmp1.Text = FormatNumber(Val(TextBoxNewAmp.Text), 1)

        If RadioButton4.Checked = True Then TextBoxNewVolt1.Text = FormatNumber(Val(TextBoxNewVolt.Text), 0)
        If RadioButton4.Checked = True Then TextBoxNewAmp1.Text = FormatNumber(Val(TextBoxNewAmp.Text) / (3 ^ 0.5), 1)

        'OUTPUT CALCULATIONS

        TextBoxNewKw.Text = Val(TextBoxOldKw.Text) * Val(TextBoxNewFreq.Text) / Val(TextBoxOldFreq.Text)
        TextBoxNewSpeed.Text = FormatNumber(Val(TextBoxOldSpeed.Text) * Val(TextBoxNewFreq.Text) / Val(TextBoxOldFreq.Text), 0)


        TextBoxNewCool.Text = FormatNumber((Val(TextBoxNewFreq.Text) * 100 / Val(TextBoxOldFreq.Text)), 0)
        TextBoxNewCool.Text = Val(TextBoxNewCool.Text / 100)





        TextBoxNewTurn.Text = FormatNumber((TextBoxOldTurn.Text * (TextBoxNewVolt.Text / TextBoxOldVolt.Text) * (TextBoxNewPath.Text / TextBoxOldPath.Text) * ((TextBoxOldFreq.Text / TextBoxNewFreq.Text) ^ 0.5)), 2)

        '''''''''''''''done till here


        If RadioButton2.Checked = True Then TextBoxNewTurn.Text = FormatNumber(Val(TextBoxNewTurn.Text) * (3 ^ 0.5), 2)
        If RadioButton3.Checked = True Then TextBoxNewTurn.Text = FormatNumber(Val(TextBoxNewTurn.Text) / (3 ^ 0.5), 2)



        TextBoxArea1.Text = (Val(TextBoxOldDia.Text / 2) ^ 2) * Math.PI
        TextBoxArea2.Text = TextBoxArea1.Text * (TextBoxNewFreq.Text / TextBoxOldFreq.Text) * (TextBoxOldVolt.Text / TextBoxNewVolt.Text) * (TextBoxNewPath.Text / TextBoxOldPath.Text)

        If RadioButton2.Checked = True Then TextBoxArea2.Text = Val(TextBoxArea2.Text) / (3 ^ 0.5)
        If RadioButton3.Checked = True Then TextBoxArea2.Text = Val(TextBoxArea2.Text) * (3 ^ 0.5)

        TextBoxAns1.Text = (Val((TextBoxArea2.Text) / Math.PI) ^ 0.5) * 2
        TextBoxNewDia.Text = FormatNumber(Val(TextBoxAns1.Text), 3)

        Form1.TextBoxNum2.Text = TextBoxNewDia.Text

        Label19.Text = "Your new data is " & TextBoxNewTurn.Text & " turns of " & TextBoxNewDia.Text & "mm. dia (see WireCalc)."
        If TextBoxNewCool.Text > 1 Then Label20.Text = "Your air flow can increase by up to " & Val(TextBoxNewCool.Text * 100) - 100 & "%, depending on the load rating of the cooling fan."

        If TextBoxNewCool.Text < 1 Then Label20.Text = "Your air flow will decrease by " & (Val(TextBoxNewCool.Text * 100) - 100) * -1 & "%."
        If TextBoxNewCool.Text = 1 Then Label20.Text = "Your air flow is the same."

        'Label35.Text = "If you increase your copper area where possible, this should improve the motor's efficiency."
label1:


    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Dim Ask As MsgBoxResult
        Ask = MsgBox("Are you sure you want to exit!", MsgBoxStyle.YesNo, "Title")
        If Ask = MsgBoxResult.Yes Then MsgBox("Remember, you are responsible to ensure that your winding data is correct. You may have to rewind the motor more than once. The best test is in situ, on load doing the job it is intended for. The Author takes no responsibility for any errors in the calculations in any form. Good luck!")
        Close()

    End Sub

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub SpeedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form4.Show()
        Me.Hide()
    End Sub

    Private Sub OutputPowerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form4.Show()
        Me.Hide()
    End Sub

    Private Sub TextBoxOldAmp1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxOldAmp.TextChanged

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged

    End Sub

    Private Sub Label35_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label35.Click

    End Sub

    Private Sub OutputPowerAndSpeedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OutputPowerAndSpeedToolStripMenuItem.Click
        Form4.Show()
        Me.Hide()
    End Sub
End Class