﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form4
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProgramsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WireCalculatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FrequencyConverterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConvertPitchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnevenGroupsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnevenGroupsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.tbolddia = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.tboldturn = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tboldpath = New System.Windows.Forms.TextBox()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbcoredia = New System.Windows.Forms.TextBox()
        Me.tbcorelen = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbslot = New System.Windows.Forms.TextBox()
        Me.tboldpole = New System.Windows.Forms.TextBox()
        Me.tboldkw = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tbspp = New System.Windows.Forms.TextBox()
        Me.Span1 = New System.Windows.Forms.TextBox()
        Me.Span2 = New System.Windows.Forms.TextBox()
        Me.Span3 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbnewcoilgroup = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbnewavspan = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbnewdia = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tbnewturn = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tbnewpath = New System.Windows.Forms.TextBox()
        Me.tbnewpole = New System.Windows.Forms.TextBox()
        Me.tbnewkw = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tboldcf = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.tbcorepower = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.TextBox21 = New System.Windows.Forms.TextBox()
        Me.TextBox22 = New System.Windows.Forms.TextBox()
        Me.TextBox23 = New System.Windows.Forms.TextBox()
        Me.TextBox24 = New System.Windows.Forms.TextBox()
        Me.TextBox25 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.tbnewspp = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.TextBoxArea1 = New System.Windows.Forms.TextBox()
        Me.TextBoxArea2 = New System.Windows.Forms.TextBox()
        Me.TextBoxAns1 = New System.Windows.Forms.TextBox()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(707, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "© Tony Ball April 2012"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(-1, 27)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(835, 51)
        Me.Button1.TabIndex = 26
        Me.Button1.UseVisualStyleBackColor = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightBlue
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ProgramsToolStripMenuItem, Me.UnevenGroupsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(834, 27)
        Me.MenuStrip1.TabIndex = 28
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(41, 23)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(99, 24)
        Me.ExitToolStripMenuItem.Text = "&Exit"
        '
        'ProgramsToolStripMenuItem
        '
        Me.ProgramsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.WireCalculatorToolStripMenuItem, Me.FrequencyConverterToolStripMenuItem, Me.ConvertPitchToolStripMenuItem})
        Me.ProgramsToolStripMenuItem.Name = "ProgramsToolStripMenuItem"
        Me.ProgramsToolStripMenuItem.Size = New System.Drawing.Size(80, 23)
        Me.ProgramsToolStripMenuItem.Text = "Programs"
        '
        'WireCalculatorToolStripMenuItem
        '
        Me.WireCalculatorToolStripMenuItem.Name = "WireCalculatorToolStripMenuItem"
        Me.WireCalculatorToolStripMenuItem.Size = New System.Drawing.Size(206, 24)
        Me.WireCalculatorToolStripMenuItem.Text = "Wire Calculator"
        '
        'FrequencyConverterToolStripMenuItem
        '
        Me.FrequencyConverterToolStripMenuItem.Name = "FrequencyConverterToolStripMenuItem"
        Me.FrequencyConverterToolStripMenuItem.Size = New System.Drawing.Size(206, 24)
        Me.FrequencyConverterToolStripMenuItem.Text = "Frequency Converter"
        '
        'ConvertPitchToolStripMenuItem
        '
        Me.ConvertPitchToolStripMenuItem.Name = "ConvertPitchToolStripMenuItem"
        Me.ConvertPitchToolStripMenuItem.Size = New System.Drawing.Size(206, 24)
        Me.ConvertPitchToolStripMenuItem.Text = "Convert Pitch"
        '
        'UnevenGroupsToolStripMenuItem
        '
        Me.UnevenGroupsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UnevenGroupsToolStripMenuItem1})
        Me.UnevenGroupsToolStripMenuItem.Name = "UnevenGroupsToolStripMenuItem"
        Me.UnevenGroupsToolStripMenuItem.Size = New System.Drawing.Size(61, 23)
        Me.UnevenGroupsToolStripMenuItem.Text = "Charts"
        '
        'UnevenGroupsToolStripMenuItem1
        '
        Me.UnevenGroupsToolStripMenuItem1.Name = "UnevenGroupsToolStripMenuItem1"
        Me.UnevenGroupsToolStripMenuItem1.Size = New System.Drawing.Size(174, 24)
        Me.UnevenGroupsToolStripMenuItem1.Text = "Uneven Groups"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(49, 23)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.White
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(317, 36)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(191, 34)
        Me.Label26.TabIndex = 278
        Me.Label26.Text = "Converting Output Power" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Speed and Current Paths"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(345, 242)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(144, 39)
        Me.Label27.TabIndex = 277
        Me.Label27.Text = "Use the wire calculator to " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "convert your original wires to " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "one wire diameter!"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(107, 303)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(110, 17)
        Me.Label24.TabIndex = 273
        Me.Label24.Text = "Wire Diameter ="
        '
        'tbolddia
        '
        Me.tbolddia.Location = New System.Drawing.Point(220, 301)
        Me.tbolddia.Name = "tbolddia"
        Me.tbolddia.Size = New System.Drawing.Size(86, 20)
        Me.tbolddia.TabIndex = 7
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(132, 282)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(85, 17)
        Me.Label22.TabIndex = 271
        Me.Label22.Text = "Turns/Slot ="
        '
        'tboldturn
        '
        Me.tboldturn.Location = New System.Drawing.Point(220, 279)
        Me.tboldturn.Name = "tboldturn"
        Me.tboldturn.Size = New System.Drawing.Size(86, 20)
        Me.tboldturn.TabIndex = 6
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(72, 259)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(145, 17)
        Me.Label17.TabIndex = 268
        Me.Label17.Text = "No.of Current Paths ="
        '
        'tboldpath
        '
        Me.tboldpath.Location = New System.Drawing.Point(220, 256)
        Me.tboldpath.Name = "tboldpath"
        Me.tboldpath.Size = New System.Drawing.Size(86, 20)
        Me.tboldpath.TabIndex = 5
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button12.ForeColor = System.Drawing.Color.White
        Me.Button12.Location = New System.Drawing.Point(424, 367)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(62, 34)
        Me.Button12.TabIndex = 250
        Me.Button12.Text = "Clear"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.Green
        Me.Button13.ForeColor = System.Drawing.Color.White
        Me.Button13.Location = New System.Drawing.Point(347, 367)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(62, 34)
        Me.Button13.TabIndex = 249
        Me.Button13.Text = "Calc"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(622, 110)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(89, 17)
        Me.Label14.TabIndex = 257
        Me.Label14.Text = "NEW DATA"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button2.Location = New System.Drawing.Point(502, 97)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(294, 305)
        Me.Button2.TabIndex = 246
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(70, 235)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(147, 17)
        Me.Label8.TabIndex = 256
        Me.Label8.Text = "Core Diameter (mm) ="
        '
        'tbcoredia
        '
        Me.tbcoredia.Location = New System.Drawing.Point(220, 233)
        Me.tbcoredia.Name = "tbcoredia"
        Me.tbcoredia.Size = New System.Drawing.Size(86, 20)
        Me.tbcoredia.TabIndex = 4
        '
        'tbcorelen
        '
        Me.tbcorelen.Location = New System.Drawing.Point(220, 211)
        Me.tbcorelen.Name = "tbcorelen"
        Me.tbcorelen.Size = New System.Drawing.Size(86, 20)
        Me.tbcorelen.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(83, 213)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(134, 17)
        Me.Label7.TabIndex = 255
        Me.Label7.Text = "Core Length (mm) ="
        '
        'tbslot
        '
        Me.tbslot.Location = New System.Drawing.Point(220, 188)
        Me.tbslot.Name = "tbslot"
        Me.tbslot.Size = New System.Drawing.Size(86, 20)
        Me.tbslot.TabIndex = 2
        '
        'tboldpole
        '
        Me.tboldpole.Location = New System.Drawing.Point(220, 165)
        Me.tboldpole.Name = "tboldpole"
        Me.tboldpole.Size = New System.Drawing.Size(86, 20)
        Me.tboldpole.TabIndex = 1
        '
        'tboldkw
        '
        Me.tboldkw.Location = New System.Drawing.Point(220, 142)
        Me.tboldkw.Name = "tboldkw"
        Me.tboldkw.Size = New System.Drawing.Size(86, 20)
        Me.tboldkw.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(124, 190)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 17)
        Me.Label3.TabIndex = 254
        Me.Label3.Text = "No. of Slots ="
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(124, 167)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 17)
        Me.Label2.TabIndex = 253
        Me.Label2.Text = "No of Poles ="
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(62, 144)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(155, 17)
        Me.Label1.TabIndex = 252
        Me.Label1.Text = "Output Power in kW's ="
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(114, 108)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(127, 17)
        Me.Label15.TabIndex = 251
        Me.Label15.Text = "ORIGINAL DATA"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button5.Location = New System.Drawing.Point(35, 97)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(294, 305)
        Me.Button5.TabIndex = 248
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(90, 326)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(127, 17)
        Me.Label19.TabIndex = 282
        Me.Label19.Text = "Slots/Pole/Phase ="
        '
        'tbspp
        '
        Me.tbspp.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbspp.Location = New System.Drawing.Point(220, 323)
        Me.tbspp.Name = "tbspp"
        Me.tbspp.ReadOnly = True
        Me.tbspp.Size = New System.Drawing.Size(86, 20)
        Me.tbspp.TabIndex = 360
        '
        'Span1
        '
        Me.Span1.BackColor = System.Drawing.Color.White
        Me.Span1.Location = New System.Drawing.Point(70, 479)
        Me.Span1.Name = "Span1"
        Me.Span1.Size = New System.Drawing.Size(23, 20)
        Me.Span1.TabIndex = 19
        '
        'Span2
        '
        Me.Span2.BackColor = System.Drawing.Color.White
        Me.Span2.Location = New System.Drawing.Point(97, 479)
        Me.Span2.Name = "Span2"
        Me.Span2.Size = New System.Drawing.Size(23, 20)
        Me.Span2.TabIndex = 20
        '
        'Span3
        '
        Me.Span3.BackColor = System.Drawing.Color.White
        Me.Span3.Location = New System.Drawing.Point(123, 479)
        Me.Span3.Name = "Span3"
        Me.Span3.Size = New System.Drawing.Size(23, 20)
        Me.Span3.TabIndex = 21
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.Color.White
        Me.TextBox6.Location = New System.Drawing.Point(148, 479)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(23, 20)
        Me.TextBox6.TabIndex = 22
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.Color.White
        Me.TextBox7.Location = New System.Drawing.Point(175, 479)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(23, 20)
        Me.TextBox7.TabIndex = 23
        '
        'TextBox8
        '
        Me.TextBox8.BackColor = System.Drawing.Color.White
        Me.TextBox8.Location = New System.Drawing.Point(201, 479)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(23, 20)
        Me.TextBox8.TabIndex = 24
        '
        'TextBox9
        '
        Me.TextBox9.BackColor = System.Drawing.Color.White
        Me.TextBox9.Location = New System.Drawing.Point(228, 480)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(23, 20)
        Me.TextBox9.TabIndex = 25
        '
        'TextBox10
        '
        Me.TextBox10.BackColor = System.Drawing.Color.White
        Me.TextBox10.Location = New System.Drawing.Point(254, 480)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(23, 20)
        Me.TextBox10.TabIndex = 26
        '
        'TextBox11
        '
        Me.TextBox11.BackColor = System.Drawing.Color.White
        Me.TextBox11.Location = New System.Drawing.Point(280, 480)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(23, 20)
        Me.TextBox11.TabIndex = 27
        '
        'TextBox12
        '
        Me.TextBox12.BackColor = System.Drawing.Color.White
        Me.TextBox12.Location = New System.Drawing.Point(306, 480)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(23, 20)
        Me.TextBox12.TabIndex = 28
        '
        'TextBox13
        '
        Me.TextBox13.BackColor = System.Drawing.Color.White
        Me.TextBox13.Location = New System.Drawing.Point(333, 480)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(23, 20)
        Me.TextBox13.TabIndex = 29
        '
        'TextBox14
        '
        Me.TextBox14.BackColor = System.Drawing.Color.White
        Me.TextBox14.Location = New System.Drawing.Point(358, 480)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(23, 20)
        Me.TextBox14.TabIndex = 30
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(38, 480)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(32, 17)
        Me.Label28.TabIndex = 297
        Me.Label28.Text = "1 to"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(577, 279)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(106, 17)
        Me.Label9.TabIndex = 318
        Me.Label9.Text = "Coil Grouping ="
        '
        'tbnewcoilgroup
        '
        Me.tbnewcoilgroup.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbnewcoilgroup.Location = New System.Drawing.Point(686, 277)
        Me.tbnewcoilgroup.Name = "tbnewcoilgroup"
        Me.tbnewcoilgroup.ReadOnly = True
        Me.tbnewcoilgroup.Size = New System.Drawing.Size(86, 20)
        Me.tbnewcoilgroup.TabIndex = 17
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(603, 213)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 17)
        Me.Label10.TabIndex = 316
        Me.Label10.Text = "Full Span = 1 to"
        '
        'tbnewavspan
        '
        Me.tbnewavspan.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbnewavspan.Location = New System.Drawing.Point(712, 211)
        Me.tbnewavspan.Name = "tbnewavspan"
        Me.tbnewavspan.ReadOnly = True
        Me.tbnewavspan.Size = New System.Drawing.Size(60, 20)
        Me.tbnewavspan.TabIndex = 16
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(573, 257)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(110, 17)
        Me.Label11.TabIndex = 314
        Me.Label11.Text = "Wire Diameter ="
        '
        'tbnewdia
        '
        Me.tbnewdia.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbnewdia.Location = New System.Drawing.Point(686, 255)
        Me.tbnewdia.Name = "tbnewdia"
        Me.tbnewdia.ReadOnly = True
        Me.tbnewdia.Size = New System.Drawing.Size(86, 20)
        Me.tbnewdia.TabIndex = 15
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(626, 235)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(57, 17)
        Me.Label12.TabIndex = 313
        Me.Label12.Text = "Turns ="
        '
        'tbnewturn
        '
        Me.tbnewturn.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbnewturn.Location = New System.Drawing.Point(686, 233)
        Me.tbnewturn.Name = "tbnewturn"
        Me.tbnewturn.ReadOnly = True
        Me.tbnewturn.Size = New System.Drawing.Size(86, 20)
        Me.tbnewturn.TabIndex = 14
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(538, 190)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(145, 17)
        Me.Label13.TabIndex = 312
        Me.Label13.Text = "No.of Current Paths ="
        '
        'tbnewpath
        '
        Me.tbnewpath.Location = New System.Drawing.Point(686, 188)
        Me.tbnewpath.Name = "tbnewpath"
        Me.tbnewpath.Size = New System.Drawing.Size(86, 20)
        Me.tbnewpath.TabIndex = 13
        '
        'tbnewpole
        '
        Me.tbnewpole.Location = New System.Drawing.Point(686, 165)
        Me.tbnewpole.Name = "tbnewpole"
        Me.tbnewpole.Size = New System.Drawing.Size(86, 20)
        Me.tbnewpole.TabIndex = 12
        '
        'tbnewkw
        '
        Me.tbnewkw.Location = New System.Drawing.Point(686, 142)
        Me.tbnewkw.Name = "tbnewkw"
        Me.tbnewkw.Size = New System.Drawing.Size(86, 20)
        Me.tbnewkw.TabIndex = 11
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(590, 167)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(93, 17)
        Me.Label30.TabIndex = 307
        Me.Label30.Text = "No of Poles ="
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(528, 145)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(155, 17)
        Me.Label31.TabIndex = 306
        Me.Label31.Text = "Output Power in kW's ="
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(605, 322)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(88, 17)
        Me.RadioButton1.TabIndex = 332
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Double Layer"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RadioButton2.Location = New System.Drawing.Point(605, 347)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(88, 17)
        Me.RadioButton2.TabIndex = 333
        Me.RadioButton2.Text = "Chain Basket"
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RadioButton3.Location = New System.Drawing.Point(605, 373)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(84, 17)
        Me.RadioButton3.TabIndex = 334
        Me.RadioButton3.Text = "Chain Group"
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(115, 349)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 17)
        Me.Label4.TabIndex = 336
        Me.Label4.Text = "Chord Factor ="
        '
        'tboldcf
        '
        Me.tboldcf.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboldcf.Location = New System.Drawing.Point(220, 345)
        Me.tboldcf.Name = "tboldcf"
        Me.tboldcf.Size = New System.Drawing.Size(86, 20)
        Me.tboldcf.TabIndex = 350
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(171, 457)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(90, 13)
        Me.Label16.TabIndex = 339
        Me.Label16.Text = "Original Coil Span"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(45, 370)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(172, 17)
        Me.Label20.TabIndex = 341
        Me.Label20.Text = "Max. Core Rating (kW's) ="
        '
        'tbcorepower
        '
        Me.tbcorepower.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbcorepower.Location = New System.Drawing.Point(220, 367)
        Me.tbcorepower.Name = "tbcorepower"
        Me.tbcorepower.Size = New System.Drawing.Size(86, 20)
        Me.tbcorepower.TabIndex = 340
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(20, 55)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(33, 13)
        Me.Label23.TabIndex = 364
        Me.Label23.Text = "Date:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(10, 34)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(44, 13)
        Me.Label25.TabIndex = 363
        Me.Label25.Text = "Job No:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(54, 31)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(66, 20)
        Me.TextBox1.TabIndex = 361
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(201, 36)
        Me.Label36.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(0, 13)
        Me.Label36.TabIndex = 369
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(146, 55)
        Me.Label35.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(33, 13)
        Me.Label35.TabIndex = 368
        Me.Label35.Text = "Time:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(201, 36)
        Me.Label29.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(0, 13)
        Me.Label29.TabIndex = 366
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(176, 55)
        Me.Label32.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(34, 13)
        Me.Label32.TabIndex = 365
        Me.Label32.Text = "00:00"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(51, 55)
        Me.Label37.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(34, 13)
        Me.Label37.TabIndex = 371
        Me.Label37.Text = "00:00"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button3.Location = New System.Drawing.Point(35, 448)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(357, 78)
        Me.Button3.TabIndex = 372
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(584, 459)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 13)
        Me.Label6.TabIndex = 386
        Me.Label6.Text = "New Coil Span"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(443, 483)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(32, 17)
        Me.Label18.TabIndex = 385
        Me.Label18.Text = "1 to"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.White
        Me.TextBox2.Location = New System.Drawing.Point(764, 483)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(23, 20)
        Me.TextBox2.TabIndex = 384
        '
        'TextBox15
        '
        Me.TextBox15.BackColor = System.Drawing.Color.White
        Me.TextBox15.Location = New System.Drawing.Point(738, 483)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(23, 20)
        Me.TextBox15.TabIndex = 383
        '
        'TextBox16
        '
        Me.TextBox16.BackColor = System.Drawing.Color.White
        Me.TextBox16.Location = New System.Drawing.Point(711, 483)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(23, 20)
        Me.TextBox16.TabIndex = 382
        '
        'TextBox17
        '
        Me.TextBox17.BackColor = System.Drawing.Color.White
        Me.TextBox17.Location = New System.Drawing.Point(686, 483)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(23, 20)
        Me.TextBox17.TabIndex = 381
        '
        'TextBox18
        '
        Me.TextBox18.BackColor = System.Drawing.Color.White
        Me.TextBox18.Location = New System.Drawing.Point(659, 483)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(23, 20)
        Me.TextBox18.TabIndex = 380
        '
        'TextBox19
        '
        Me.TextBox19.BackColor = System.Drawing.Color.White
        Me.TextBox19.Location = New System.Drawing.Point(633, 483)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(23, 20)
        Me.TextBox19.TabIndex = 379
        '
        'TextBox20
        '
        Me.TextBox20.BackColor = System.Drawing.Color.White
        Me.TextBox20.Location = New System.Drawing.Point(606, 482)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New System.Drawing.Size(23, 20)
        Me.TextBox20.TabIndex = 378
        '
        'TextBox21
        '
        Me.TextBox21.BackColor = System.Drawing.Color.White
        Me.TextBox21.Location = New System.Drawing.Point(580, 482)
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Size = New System.Drawing.Size(23, 20)
        Me.TextBox21.TabIndex = 377
        '
        'TextBox22
        '
        Me.TextBox22.BackColor = System.Drawing.Color.White
        Me.TextBox22.Location = New System.Drawing.Point(554, 482)
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(23, 20)
        Me.TextBox22.TabIndex = 376
        '
        'TextBox23
        '
        Me.TextBox23.BackColor = System.Drawing.Color.White
        Me.TextBox23.Location = New System.Drawing.Point(528, 482)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New System.Drawing.Size(23, 20)
        Me.TextBox23.TabIndex = 375
        '
        'TextBox24
        '
        Me.TextBox24.BackColor = System.Drawing.Color.White
        Me.TextBox24.Location = New System.Drawing.Point(502, 482)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New System.Drawing.Size(23, 20)
        Me.TextBox24.TabIndex = 374
        '
        'TextBox25
        '
        Me.TextBox25.BackColor = System.Drawing.Color.White
        Me.TextBox25.Location = New System.Drawing.Point(476, 482)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(23, 20)
        Me.TextBox25.TabIndex = 373
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button4.Location = New System.Drawing.Point(440, 450)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(357, 78)
        Me.Button4.TabIndex = 387
        Me.Button4.UseVisualStyleBackColor = False
        '
        'tbnewspp
        '
        Me.tbnewspp.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tbnewspp.Location = New System.Drawing.Point(686, 299)
        Me.tbnewspp.Name = "tbnewspp"
        Me.tbnewspp.ReadOnly = True
        Me.tbnewspp.Size = New System.Drawing.Size(86, 20)
        Me.tbnewspp.TabIndex = 388
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(556, 300)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(127, 17)
        Me.Label39.TabIndex = 390
        Me.Label39.Text = "Slots/Pole/Phase ="
        '
        'TextBoxArea1
        '
        Me.TextBoxArea1.Location = New System.Drawing.Point(380, 97)
        Me.TextBoxArea1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TextBoxArea1.Name = "TextBoxArea1"
        Me.TextBoxArea1.Size = New System.Drawing.Size(76, 20)
        Me.TextBoxArea1.TabIndex = 391
        '
        'TextBoxArea2
        '
        Me.TextBoxArea2.Location = New System.Drawing.Point(380, 117)
        Me.TextBoxArea2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TextBoxArea2.Name = "TextBoxArea2"
        Me.TextBoxArea2.Size = New System.Drawing.Size(76, 20)
        Me.TextBoxArea2.TabIndex = 392
        '
        'TextBoxAns1
        '
        Me.TextBoxAns1.Location = New System.Drawing.Point(380, 140)
        Me.TextBoxAns1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TextBoxAns1.Name = "TextBoxAns1"
        Me.TextBoxAns1.Size = New System.Drawing.Size(76, 20)
        Me.TextBoxAns1.TabIndex = 393
        '
        'Form4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(834, 549)
        Me.Controls.Add(Me.TextBoxAns1)
        Me.Controls.Add(Me.TextBoxArea2)
        Me.Controls.Add(Me.TextBoxArea1)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.tbnewspp)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox15)
        Me.Controls.Add(Me.TextBox16)
        Me.Controls.Add(Me.TextBox17)
        Me.Controls.Add(Me.TextBox18)
        Me.Controls.Add(Me.TextBox19)
        Me.Controls.Add(Me.TextBox20)
        Me.Controls.Add(Me.TextBox21)
        Me.Controls.Add(Me.TextBox22)
        Me.Controls.Add(Me.TextBox23)
        Me.Controls.Add(Me.TextBox24)
        Me.Controls.Add(Me.TextBox25)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.tbnewcoilgroup)
        Me.Controls.Add(Me.tbnewavspan)
        Me.Controls.Add(Me.tbnewdia)
        Me.Controls.Add(Me.tbnewturn)
        Me.Controls.Add(Me.tbnewpath)
        Me.Controls.Add(Me.tbnewpole)
        Me.Controls.Add(Me.tbnewkw)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.tbcorepower)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tboldcf)
        Me.Controls.Add(Me.RadioButton3)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.TextBox14)
        Me.Controls.Add(Me.TextBox13)
        Me.Controls.Add(Me.TextBox12)
        Me.Controls.Add(Me.TextBox11)
        Me.Controls.Add(Me.TextBox10)
        Me.Controls.Add(Me.TextBox9)
        Me.Controls.Add(Me.TextBox8)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.Span3)
        Me.Controls.Add(Me.Span2)
        Me.Controls.Add(Me.Span1)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.tbspp)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.tbolddia)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.tboldturn)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.tboldpath)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tbcoredia)
        Me.Controls.Add(Me.tbcorelen)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tbslot)
        Me.Controls.Add(Me.tboldpole)
        Me.Controls.Add(Me.tboldkw)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form4"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form4"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents tbolddia As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents tboldturn As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents tboldpath As System.Windows.Forms.TextBox
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbcoredia As System.Windows.Forms.TextBox
    Friend WithEvents tbcorelen As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbslot As System.Windows.Forms.TextBox
    Friend WithEvents tboldpole As System.Windows.Forms.TextBox
    Friend WithEvents tboldkw As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents tbspp As System.Windows.Forms.TextBox
    Friend WithEvents Span1 As System.Windows.Forms.TextBox
    Friend WithEvents Span2 As System.Windows.Forms.TextBox
    Friend WithEvents Span3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbnewcoilgroup As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbnewavspan As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbnewdia As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbnewturn As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tbnewpath As System.Windows.Forms.TextBox
    Friend WithEvents tbnewpole As System.Windows.Forms.TextBox
    Friend WithEvents tbnewkw As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents ProgramsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WireCalculatorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FrequencyConverterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboldcf As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents tbcorepower As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox21 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tbnewspp As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents TextBoxArea1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxArea2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxAns1 As System.Windows.Forms.TextBox
    Friend WithEvents UnevenGroupsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnevenGroupsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConvertPitchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
