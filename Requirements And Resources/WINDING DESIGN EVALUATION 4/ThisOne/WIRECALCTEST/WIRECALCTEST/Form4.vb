﻿Public Class Form4

    Dim dlvalue As Double
    Dim span As Integer
    Dim spanB As Double

    Private Sub WireCalcToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form1.Show()
        Me.Hide()
    End Sub

    Private Sub FrequencyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form2.Show()
        Me.Hide()
    End Sub

    Private Sub VoltageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form2.Show()
        Me.Hide()
    End Sub

    Private Sub ParallelPathsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Form2.Show()
        Me.Hide()
    End Sub

    Private Sub Form4_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click



        Dim Ask As MsgBoxResult
        Ask = MsgBox("Are you sure you want to exit!", MsgBoxStyle.YesNo, "Title")
        If Ask = MsgBoxResult.Yes Then MsgBox("Remember, you are responsible to ensure that your winding data is correct. You may have to rewind the motor more than once. The best test is in situ, on load doing the job it is intended for. The Author of this software takes no responsibility for any errors in the calculations of the new winding in any form.")
        Close()

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub tbolddia_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbolddia.TextChanged

    End Sub

    Private Sub WireCalculatorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WireCalculatorToolStripMenuItem.Click
        Form1.Show()
        Me.Hide()
    End Sub

    Private Sub FrequencyConverterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FrequencyConverterToolStripMenuItem.Click
        Form2.Show()
        Me.Hide()
    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click

        tbnewturn.Text = FormatNumber((tboldturn.Text * ((tboldkw.Text / tbnewkw.Text) ^ 0.5) * (tbnewpole.Text / tboldpole.Text) ^ 0.5) * (tbnewpath.Text / tboldpath.Text), 2)

        If RadioButton1.Checked = True Then tbnewturn.Text = FormatNumber(Val(tbnewturn.Text) / 2, 2)

        If tboldpole.Text = 2 Then dlvalue = 12.9
        If tboldpole.Text = 4 Then dlvalue = 15.6
        If tboldpole.Text = 6 Then dlvalue = 27.5
        If tboldpole.Text = 8 Then dlvalue = 38.4

        tbcorepower.Text = FormatNumber(((tbcoredia.Text / 25.4) ^ 2) * (tbcorelen.Text / 25.4) / dlvalue, 2)

        'CHECK DATA

        If tboldpole.Text <> 2 And tboldpole.Text <> 4 And tboldpole.Text <> 6 And tboldpole.Text <> 8 Then MsgBox("Original poles (left) must be 2, 4, 6 or 8!")
        If tbnewpole.Text <> 2 And tbnewpole.Text <> 4 And tbnewpole.Text <> 6 And tbnewpole.Text <> 8 Then MsgBox("New poles (right) must be 2, 4, 6 or 8!")
        If tbslot.Text <> 12 And tbslot.Text <> 18 And tbslot.Text <> 24 And tbslot.Text <> 30 And tbslot.Text <> 36 And tbslot.Text <> 42 And tbslot.Text <> 48 And tbslot.Text <> 54 And tbslot.Text <> 60 And tbslot.Text <> 72 Then MsgBox("No. of slots must be 12, 18, 24, 30, 36, 42, 48, 54, 60 or 72!")
        If tbslot.Text <> 12 And tbslot.Text <> 18 And tbslot.Text <> 24 And tbslot.Text <> 30 And tbslot.Text <> 36 And tbslot.Text <> 42 And tbslot.Text <> 48 And tbslot.Text <> 54 And tbslot.Text <> 60 And tbslot.Text <> 72 Then tbslot.Text = ""

        tbspp.Text = FormatNumber(tbslot.Text / (tboldpole.Text * 3), 2)

        tbnewspp.Text = FormatNumber(Val(tbslot.Text) / Val((tbnewpole.Text) * 3), 2)



        If RadioButton1.Checked = True Then tbnewcoilgroup.Text = Val(tbslot.Text) / Val(tbnewspp.Text) & " Groups of " & Val(tbnewspp.Text)
        If RadioButton1.Checked = True And tbnewspp.Text = 1 Then tbnewcoilgroup.Text = Val(tbslot.Text) / Val(tbnewspp.Text) & " Single Coils"


        If RadioButton2.Checked = True Then tbnewcoilgroup.Text = Val(tbslot.Text) / Val(tbnewspp.Text) / 2 & " Groups of " & Val(tbnewspp.Text)
        If RadioButton2.Checked = True And tbnewspp.Text = 1 Then tbnewcoilgroup.Text = Val(tbslot.Text) / Val(tbnewspp.Text) / 2 & " Single Coils"

        If RadioButton3.Checked = True Then tbnewcoilgroup.Text = Val(tbslot.Text) / Val(tbnewspp.Text) / 2 & " Groups of " & Val(tbnewspp.Text)
        If RadioButton3.Checked = True And tbnewspp.Text = 1 Then tbnewcoilgroup.Text = Val(tbslot.Text) / Val(tbnewspp.Text) / 2 & " Single Coils"




        If tbnewspp.Text <> 1 And tbnewspp.Text <> 2 And tbnewspp.Text <> 3 And tbnewspp.Text <> 4 And tbnewspp.Text <> 5 And tbnewspp.Text <> 6 And tbnewspp.Text <> 7 And tbnewspp.Text <> 8 And tbnewspp.Text <> 9 And tbnewspp.Text <> 10 And tbnewspp.Text <> 11 And tbnewspp.Text <> 12 Then tbnewcoilgroup.Text = "Odd Groups"

        span = (Val(tbslot.Text) / Val(tbnewpole.Text)) + 1
        tbnewavspan.Text = (Val(tbslot.Text) / Val(tbnewpole.Text)) + 1


        If Val(tbnewspp.Text) <> 1 And Val(tbnewspp.Text) <> 2 And Val(tbnewspp.Text) <> 3 And Val(tbnewspp.Text) <> 4 And Val(tbnewspp.Text) <> 5 And Val(tbnewspp.Text) <> 6 And Val(tbnewspp.Text) <> 7 And Val(tbnewspp.Text) <> 8 And Val(tbnewspp.Text) <> 9 And Val(tbnewspp.Text) <> 10 And Val(tbnewspp.Text) <> 11 And Val(tbnewspp.Text) <> 12 Then tbnewavspan.Text = span - 1

        TextBoxArea1.Text = (Val(tbolddia.Text / 2) ^ 2) * Math.PI
        TextBoxArea2.Text = Val(TextBoxArea1.Text) * Val(tboldpath.Text) / Val(tbnewpath.Text) * Val(tbnewkw.Text) / Val(tboldkw.Text)
        TextBoxAns1.Text = (Val((TextBoxArea2.Text) / Math.PI) ^ 0.5) * 2
        tbnewdia.Text = FormatNumber(Val(TextBoxAns1.Text), 3)

        Form6.slot.Text = tbslot.Text
        Form6.pole.Text = tboldpole.Text

    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        tboldkw.Text = ""
        tboldpole.Text = ""
        tbcorelen.Text = ""
        tbslot.Text = ""
        tbcoredia.Text = ""
        tboldpath.Text = ""
        tboldturn.Text = ""
        tbolddia.Text = ""
        tbspp.Text = ""
        tbcorepower.Text = ""
        tboldcf.Text = ""
        tbnewspp.Text = ""

        tbnewkw.Text = ""
        tbnewpole.Text = ""
        tbnewpath.Text = ""
        tbnewturn.Text = ""
        tbnewdia.Text = ""
        tbnewavspan.Text = ""
        tbnewcoilgroup.Text = ""


    End Sub

    Private Sub tbcorepower_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbcorepower.TextChanged

        

    End Sub

    Private Sub tboldcf_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tboldcf.TextChanged

    End Sub

    
    Private Sub Label27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label27.Click

    End Sub

    Private Sub UnevenGroupsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UnevenGroupsToolStripMenuItem1.Click

        Form5.Show()

    End Sub

    Private Sub ProgramsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgramsToolStripMenuItem.Click

        

    End Sub

    Private Sub ConvertPitchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConvertPitchToolStripMenuItem.Click

        Form6.Show()
        Me.Hide()

    End Sub
End Class