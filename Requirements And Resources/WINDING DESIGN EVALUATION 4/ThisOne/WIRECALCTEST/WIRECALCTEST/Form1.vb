﻿Public Class Form1



    Dim Estimate As Integer
    Dim AnswerOld As Double
    Dim AnswerNew As Double

    Dim metric As String
    Dim imperial As String

    Private Sub CalcBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcBtn.Click

        If RadioButton2.Checked = True Then Label16.Text = "and their diameters in inches"
        If RadioButton2.Checked = True Then Label14.Text = "and their diameters in inches"
        If RadioButton1.Checked = True Then Label16.Text = "and their diameters in mm's"
        If RadioButton1.Checked = True Then Label14.Text = "and their diameters in mm's"

        TextBoxNum3.Text = FormatNumber(Val(TextBoxNum1.Text) * ((Val(TextBoxNum2.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum3.Text = 0 Then TextBoxNum3.Text = ""

        TextBoxNum6.Text = FormatNumber(Val(TextBoxNum4.Text) * ((Val(TextBoxNum5.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum6.Text = 0 Then TextBoxNum6.Text = ""

        TextBoxNum9.Text = FormatNumber(Val(TextBoxNum7.Text) * ((Val(TextBoxNum8.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum9.Text = 0 Then TextBoxNum9.Text = ""

        TextBoxNum12.Text = FormatNumber(Val(TextBoxNum10.Text) * ((Val(TextBoxNum11.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum12.Text = 0 Then TextBoxNum12.Text = ""

        TextBoxNum15.Text = FormatNumber(Val(TextBoxNum13.Text) * ((Val(TextBoxNum14.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum15.Text = 0 Then TextBoxNum15.Text = ""

        TextBoxNum18.Text = FormatNumber(Val(TextBoxNum16.Text) * ((Val(TextBoxNum17.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum18.Text = 0 Then TextBoxNum18.Text = ""

        TextBoxNum21.Text = FormatNumber(Val(TextBoxNum19.Text) * ((Val(TextBoxNum20.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum21.Text = 0 Then TextBoxNum21.Text = ""

        TextBoxNum24.Text = FormatNumber(Val(TextBoxNum22.Text) * ((Val(TextBoxNum23.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum24.Text = 0 Then TextBoxNum24.Text = ""

        TextBoxNum27.Text = FormatNumber(Val(TextBoxNum25.Text) * ((Val(TextBoxNum26.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum27.Text = 0 Then TextBoxNum27.Text = ""

        TextBoxNum30.Text = FormatNumber(Val(TextBoxNum28.Text) * ((Val(TextBoxNum29.Text) / 2) ^ 2) * Math.PI, 10)

        If TextBoxNum30.Text = 0 Then TextBoxNum30.Text = ""

        If Val(TextBoxSq1.Text) > 0 And Val(TextBoxSq2.Text) > 0 Then TextBoxNum3.Text = FormatNumber(Val(TextBoxSq1.Text) * Val(TextBoxSq2.Text), 10)

        TextBoxOld.Text = FormatNumber(Val(TextBoxNum3.Text) + Val(TextBoxNum6.Text) + Val(TextBoxNum9.Text) + Val(TextBoxNum12.Text) + Val(TextBoxNum15.Text) + Val(TextBoxNum18.Text) + Val(TextBoxNum21.Text) + Val(TextBoxNum24.Text) + Val(TextBoxNum27.Text) + Val(TextBoxNum30.Text), 10)

        If Val(TextBoxOld.Text) = 0 Then TextBoxOld.Text = ""
        If Val(TextBoxOld.Text) = 0 Then MsgBox("Enter the data in the boxes (Left) and click calc!")
        If Val(TextBoxOld.Text) = 0 Then GoTo Label1

        TextBoxNum33.Text = FormatNumber(Val(TextBoxNum31.Text) * ((Val(TextBoxNum32.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum33.Text = 0 Then TextBoxNum33.Text = ""

        TextBoxNum36.Text = FormatNumber(Val(TextBoxNum34.Text) * ((Val(TextBoxNum35.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum36.Text = 0 Then TextBoxNum36.Text = ""

        TextBoxNum39.Text = FormatNumber(Val(TextBoxNum37.Text) * ((Val(TextBoxNum38.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum39.Text = 0 Then TextBoxNum39.Text = ""

        TextBoxNum42.Text = FormatNumber(Val(TextBoxNum40.Text) * ((Val(TextBoxNum41.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum42.Text = 0 Then TextBoxNum42.Text = ""

        TextBoxNum45.Text = FormatNumber(Val(TextBoxNum43.Text) * ((Val(TextBoxNum44.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum45.Text = 0 Then TextBoxNum45.Text = ""

        TextBoxNum48.Text = FormatNumber(Val(TextBoxNum46.Text) * ((Val(TextBoxNum47.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum48.Text = 0 Then TextBoxNum48.Text = ""

        TextBoxNum51.Text = FormatNumber(Val(TextBoxNum49.Text) * ((Val(TextBoxNum50.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum51.Text = 0 Then TextBoxNum51.Text = ""

        TextBoxNum54.Text = FormatNumber(Val(TextBoxNum52.Text) * ((Val(TextBoxNum53.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum54.Text = 0 Then TextBoxNum54.Text = ""

        TextBoxNum57.Text = FormatNumber(Val(TextBoxNum55.Text) * ((Val(TextBoxNum56.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum57.Text = 0 Then TextBoxNum57.Text = ""

        TextBoxNum60.Text = FormatNumber(Val(TextBoxNum58.Text) * ((Val(TextBoxNum59.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum60.Text = 0 Then TextBoxNum60.Text = ""

        TextBoxNew.Text = FormatNumber(Val(TextBoxNum33.Text) + Val(TextBoxNum36.Text) + Val(TextBoxNum39.Text) + Val(TextBoxNum42.Text) + Val(TextBoxNum45.Text) + Val(TextBoxNum48.Text) + Val(TextBoxNum51.Text) + Val(TextBoxNum54.Text) + Val(TextBoxNum57.Text) + Val(TextBoxNum60.Text), 10)

        If Val(TextBoxNew.Text) = 0 Then TextBoxNew.Text = ""
        If Val(TextBoxNew.Text) = 0 Then MsgBox("Enter the data in the boxes (Right) and click calc!")
        If Val(TextBoxNew.Text) = 0 Then GoTo Label1

        Label1.Text = ""
        Label2.Text = ""
        Label3.Text = ""

        If TextBoxOld.Text = TextBoxNew.Text Then
            Label2.Text = "Your new area is the same as your old area"

            If Val(TextBoxOld.Text) = Val(TextBoxNew.Text) Then GoTo label1

        ElseIf TextBoxOld.Text > Val(TextBoxNew.Text) Then
            Label1.Text = "Your new area is " & FormatNumber((((TextBoxOld.Text - TextBoxNew.Text) / TextBoxOld.Text) * 100), 2) & "% smaller than your old area"
        ElseIf TextBoxOld.Text < Val(TextBoxNew.Text) Then
            Label1.Text = "Your new area is " & FormatNumber((((TextBoxNew.Text - TextBoxOld.Text) / TextBoxOld.Text) * 100), 2) & "% bigger than your old area"
        End If

        If RadioButton1.Checked = False Then metric = " " & imperial
        If RadioButton2.Checked = False Then metric = " mm's"

        If TextBoxOld.Text > Val(TextBoxNew.Text) Then
            Label2.Text = "The area difference is " & FormatNumber(TextBoxOld.Text - TextBoxNew.Text, 10) & metric
        ElseIf TextBoxOld.Text < Val(TextBoxNew.Text) Then
            Label2.Text = "The area difference is " & FormatNumber(TextBoxNew.Text - TextBoxOld.Text, 10) & metric
        ElseIf TextBoxOld.Text = Val(TextBoxNew.Text) Then
            Label2.Text = "The area difference is " & FormatNumber(TextBoxNew.Text - TextBoxOld.Text, 10) & metric
        End If

        If TextBoxOld.Text > Val(TextBoxNew.Text) Then
            Label3.Text = "This is equal to a diameter of " & FormatNumber((((TextBoxOld.Text - TextBoxNew.Text) / Math.PI) ^ 0.5) * 2, 10) & metric
        ElseIf TextBoxNew.Text > Val(TextBoxOld.Text) Then
            Label3.Text = "This is equal to a diameter of " & FormatNumber((((TextBoxNew.Text - TextBoxOld.Text) / Math.PI) ^ 0.5) * 2, 10) & metric

Label1:

        End If

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click

        TextBoxNum1.Text = ""
        TextBoxNum2.Text = ""
        TextBoxNum3.Text = ""
        TextBoxNum4.Text = ""
        TextBoxNum5.Text = ""
        TextBoxNum6.Text = ""
        TextBoxNum7.Text = ""
        TextBoxNum8.Text = ""
        TextBoxNum9.Text = ""
        TextBoxNum10.Text = ""
        TextBoxNum11.Text = ""
        TextBoxNum12.Text = ""
        TextBoxNum13.Text = ""
        TextBoxNum14.Text = ""
        TextBoxNum15.Text = ""
        TextBoxNum16.Text = ""
        TextBoxNum17.Text = ""
        TextBoxNum18.Text = ""
        TextBoxNum19.Text = ""
        TextBoxNum20.Text = ""
        TextBoxNum21.Text = ""
        TextBoxNum22.Text = ""
        TextBoxNum23.Text = ""
        TextBoxNum24.Text = ""
        TextBoxNum25.Text = ""
        TextBoxNum26.Text = ""
        TextBoxNum27.Text = ""
        TextBoxNum28.Text = ""
        TextBoxNum29.Text = ""
        TextBoxNum30.Text = ""

        TextBoxOld.Text = ""

        TextBoxNum31.Text = ""
        TextBoxNum32.Text = ""
        TextBoxNum33.Text = ""
        TextBoxNum34.Text = ""
        TextBoxNum35.Text = ""
        TextBoxNum36.Text = ""
        TextBoxNum37.Text = ""
        TextBoxNum38.Text = ""
        TextBoxNum39.Text = ""
        TextBoxNum40.Text = ""
        TextBoxNum41.Text = ""
        TextBoxNum42.Text = ""
        TextBoxNum43.Text = ""
        TextBoxNum44.Text = ""
        TextBoxNum45.Text = ""
        TextBoxNum46.Text = ""
        TextBoxNum47.Text = ""
        TextBoxNum48.Text = ""
        TextBoxNum49.Text = ""
        TextBoxNum50.Text = ""
        TextBoxNum51.Text = ""
        TextBoxNum52.Text = ""
        TextBoxNum53.Text = ""
        TextBoxNum54.Text = ""
        TextBoxNum55.Text = ""
        TextBoxNum56.Text = ""
        TextBoxNum57.Text = ""
        TextBoxNum58.Text = ""
        TextBoxNum59.Text = ""
        TextBoxNum60.Text = ""

        TextBoxNew.Text = ""

        Label1.Text = ""
        Label2.Text = ""
        Label3.Text = ""



        If Val(TextBoxSq1.Text) = 0 Or Val(TextBoxSq2.Text) = 0 Then MsgBox("Enter both values in the boxes (Left) and click calc!")
        If Val(TextBoxSq1.Text) = 0 Then GoTo label20
        If Val(TextBoxSq2.Text) = 0 Then GoTo label20

        TextBoxNum1.Text = 1
        TextBoxSq3.Text = FormatNumber(Val(TextBoxSq1.Text) * Val(TextBoxSq2.Text), 10)
        TextBoxNum2.Text = FormatNumber((((Val(TextBoxSq3.Text) / Math.PI) ^ 0.5) * 2), 5)
        TextBoxNum3.Text = FormatNumber(Val(TextBoxSq1.Text) * Val(TextBoxSq2.Text), 10)
        TextBoxOld.Text = FormatNumber(Val(TextBoxNum3.Text + TextBoxNum6.Text + TextBoxNum9.Text + TextBoxNum12.Text + TextBoxNum15.Text + TextBoxNum18.Text + TextBoxNum21.Text + TextBoxNum24.Text + TextBoxNum27.Text + TextBoxNum30.Text), 10)

label20:

        
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        metric = " mm's"
        imperial = " inches"

        If RadioButton1.Checked = False Then metric = " inches"

        If TextBoxEst.Text = "" Then
            MsgBox("Enter the no. of wires in the Estimator box (Below)!")

            GoTo label5
        Else

            Estimate = Val(TextBoxEst.Text)
            If TextBoxOld.Text = "" Then MsgBox("Enter the data in the boxes (Left) and click calc!")
            If TextBoxOld.Text = "" Then GoTo label5
            TextBoxEst.Text = Val(FormatNumber(((TextBoxOld.Text / Estimate / Math.PI) ^ 0.5) * 2, 4))
            Label2.Text = "Try " & Estimate & " wires around " & TextBoxEst.Text & metric & " diameter."

            TextBoxNum31.Text = ""
            TextBoxNum32.Text = ""
            TextBoxNum33.Text = ""
            TextBoxNum34.Text = ""
            TextBoxNum35.Text = ""
            TextBoxNum36.Text = ""
            TextBoxNum37.Text = ""
            TextBoxNum38.Text = ""
            TextBoxNum39.Text = ""
            TextBoxNum40.Text = ""
            TextBoxNum41.Text = ""
            TextBoxNum42.Text = ""
            TextBoxNum43.Text = ""
            TextBoxNum44.Text = ""
            TextBoxNum45.Text = ""
            TextBoxNum46.Text = ""
            TextBoxNum47.Text = ""
            TextBoxNum48.Text = ""
            TextBoxNum49.Text = ""
            TextBoxNum50.Text = ""
            TextBoxNum51.Text = ""
            TextBoxNum52.Text = ""
            TextBoxNum53.Text = ""
            TextBoxNum54.Text = ""
            TextBoxNum55.Text = ""
            TextBoxNum56.Text = ""
            TextBoxNum57.Text = ""
            TextBoxNum58.Text = ""
            TextBoxNum59.Text = ""
            TextBoxNum60.Text = ""
            TextBoxNew.Text = ""

        End If

label5:

        TextBoxEst.Text = ""
        Label1.Text = ""
        Label3.Text = ""

    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click


        TextBoxNum3.Text = FormatNumber(Val(TextBoxNum1.Text) * ((Val(TextBoxNum2.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum3.Text = 0 Then TextBoxNum3.Text = ""

        TextBoxNum6.Text = FormatNumber(Val(TextBoxNum4.Text) * ((Val(TextBoxNum5.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum6.Text = 0 Then TextBoxNum6.Text = ""

        TextBoxNum9.Text = FormatNumber(Val(TextBoxNum7.Text) * ((Val(TextBoxNum8.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum9.Text = 0 Then TextBoxNum9.Text = ""

        TextBoxNum12.Text = FormatNumber(Val(TextBoxNum10.Text) * ((Val(TextBoxNum11.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum12.Text = 0 Then TextBoxNum12.Text = ""

        TextBoxNum15.Text = FormatNumber(Val(TextBoxNum13.Text) * ((Val(TextBoxNum14.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum15.Text = 0 Then TextBoxNum15.Text = ""

        TextBoxNum18.Text = FormatNumber(Val(TextBoxNum16.Text) * ((Val(TextBoxNum17.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum18.Text = 0 Then TextBoxNum18.Text = ""

        TextBoxNum21.Text = FormatNumber(Val(TextBoxNum19.Text) * ((Val(TextBoxNum20.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum21.Text = 0 Then TextBoxNum21.Text = ""

        TextBoxNum24.Text = FormatNumber(Val(TextBoxNum22.Text) * ((Val(TextBoxNum23.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum24.Text = 0 Then TextBoxNum24.Text = ""

        TextBoxNum27.Text = FormatNumber(Val(TextBoxNum25.Text) * ((Val(TextBoxNum26.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum27.Text = 0 Then TextBoxNum27.Text = ""

        TextBoxNum30.Text = FormatNumber(Val(TextBoxNum28.Text) * ((Val(TextBoxNum29.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum30.Text = 0 Then TextBoxNum30.Text = ""

label4:

        AnswerOld = Val(TextBoxNum3.Text) + Val(TextBoxNum6.Text) + Val(TextBoxNum9.Text) + Val(TextBoxNum12.Text) + Val(TextBoxNum15.Text) + Val(TextBoxNum18.Text) + Val(TextBoxNum21.Text) + Val(TextBoxNum24.Text) + Val(TextBoxNum27.Text) + Val(TextBoxNum30.Text)
        TextBoxOld.Text = AnswerOld
        If TextBoxOld.Text = 0 Then MsgBox("Enter the data in the boxes (Left) and click calc!")
        If TextBoxOld.Text = 0 Then TextBoxOld.Text = ""

    End Sub


    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        TextBoxNum33.Text = FormatNumber(Val(TextBoxNum31.Text) * ((Val(TextBoxNum32.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum33.Text = 0 Then TextBoxNum33.Text = ""

        TextBoxNum36.Text = FormatNumber(Val(TextBoxNum34.Text) * ((Val(TextBoxNum35.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum36.Text = 0 Then TextBoxNum36.Text = ""

        TextBoxNum39.Text = FormatNumber(Val(TextBoxNum37.Text) * ((Val(TextBoxNum38.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum39.Text = 0 Then TextBoxNum39.Text = ""

        TextBoxNum42.Text = FormatNumber(Val(TextBoxNum40.Text) * ((Val(TextBoxNum41.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum42.Text = 0 Then TextBoxNum42.Text = ""

        TextBoxNum45.Text = FormatNumber(Val(TextBoxNum43.Text) * ((Val(TextBoxNum44.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum45.Text = 0 Then TextBoxNum45.Text = ""

        TextBoxNum48.Text = FormatNumber(Val(TextBoxNum46.Text) * ((Val(TextBoxNum47.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum48.Text = 0 Then TextBoxNum48.Text = ""

        TextBoxNum51.Text = FormatNumber(Val(TextBoxNum49.Text) * ((Val(TextBoxNum50.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum51.Text = 0 Then TextBoxNum51.Text = ""

        TextBoxNum54.Text = FormatNumber(Val(TextBoxNum52.Text) * ((Val(TextBoxNum53.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum54.Text = 0 Then TextBoxNum54.Text = ""

        TextBoxNum57.Text = FormatNumber(Val(TextBoxNum55.Text) * ((Val(TextBoxNum56.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum57.Text = 0 Then TextBoxNum57.Text = ""

        TextBoxNum60.Text = FormatNumber(Val(TextBoxNum58.Text) * ((Val(TextBoxNum59.Text) / 2) ^ 2) * Math.PI, 10)
        If TextBoxNum60.Text = 0 Then TextBoxNum60.Text = ""

label7:

        AnswerNew = Val(TextBoxNum33.Text) + Val(TextBoxNum36.Text) + Val(TextBoxNum39.Text) + Val(TextBoxNum42.Text) + Val(TextBoxNum45.Text) + Val(TextBoxNum48.Text) + Val(TextBoxNum51.Text) + Val(TextBoxNum54.Text) + Val(TextBoxNum57.Text) + Val(TextBoxNum60.Text)
        TextBoxNew.Text = AnswerNew
        If TextBoxNew.Text = 0 Then MsgBox("Enter the data in the boxes (Right)!")
        If TextBoxNew.Text = 0 Then TextBoxNew.Text = ""
    End Sub

End Class
